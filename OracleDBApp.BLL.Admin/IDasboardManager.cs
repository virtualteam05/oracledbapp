﻿using OracleDBApp.Common.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OracleDBApp.BLL.Admin
{
   public interface IDasboardManager
    {
        Task<List<Menu>> GetMenus(decimal userNo,decimal companyNo);
    }
}
