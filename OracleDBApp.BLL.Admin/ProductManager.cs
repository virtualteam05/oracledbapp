﻿using Dapper;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace OracleDBApp.BLL.Admin
{
   public class ProductManager
    {
        IOptions _ConnectionString;
        public ProductConcrete(IOptions ConnectionString)
        {
            _ConnectionString = ConnectionString;
        }

        public int DeleteProduct(int ProductID)
        {
            using (OracleConnection con = new OracleConnection(_ConnectionString.Value.ConnectionString))
            {
                con.Open();
                OracleTransaction sqltrans = con.BeginTransaction();
                var param = new DynamicParameters();
                param.Add("@ProductID", ProductID);
                var result = con.Execute("sprocProductTBDeleteSingleItem", param, sqltrans, 0, System.Data.CommandType.StoredProcedure);

                if (result > 0)
                {
                    sqltrans.Commit();
                }
                else
                {
                    sqltrans.Rollback();
                }
                return result;
            }
        }

        public List GetProductList()
        {
            using (OracleConnection con = new OracleConnection(_ConnectionString.Value.ConnectionString))
            {
                return con.Query("sprocProductTBSelectList", null, null, true, 0, System.Data.CommandType.StoredProcedure).ToList();
            }
        }

        public ProductModel GetSingleProduct(int ProductID)
        {
            using (OracleConnection con = new OracleConnection(_ConnectionString.Value.ConnectionString))
            {
                var param = new DynamicParameters();
                param.Add("@ProductID", ProductID);
                return con.Query("sprocProductTBSelectSingleItem", param, null, true, 0, System.Data.CommandType.StoredProcedure).SingleOrDefault();
            }
        }

        public int InsertProduct(ProductModel productmodel)
        {
            using (OracleConnection con = new OracleConnection(_ConnectionString.Value.ConnectionString))
            {
                con.Open();
                OracleTransaction sqltrans = con.BeginTransaction();
                var param = new DynamicParameters();
                param.Add("@ProductID", productmodel.ProductID);
                param.Add("@ProductName", productmodel.ProductName);
                param.Add("@Price", productmodel.Price);
                param.Add("@QuantityperUnit", productmodel.QuantityperUnit);
                param.Add("@ProductDesc", productmodel.ProductDesc);
                param.Add("@Status", "A");
                var result = con.Execute("sprocProductTBInsertUpdateSingleItem", param, sqltrans, 0, System.Data.CommandType.StoredProcedure);

                if (result > 0)
                {
                    sqltrans.Commit();
                }
                else
                {
                    sqltrans.Rollback();
                }
                return result;
            }
        }

        public int UpdateProduct(ProductModel productmodel)
        {
            using (OracleConnection con = new OracleConnection(_ConnectionString.Value.ConnectionString))
            {
                con.Open();
                OracleTransaction sqltrans = con.BeginTransaction();
                var param = new DynamicParameters();
                param.Add("@ProductID", productmodel.ProductID);
                param.Add("@ProductName", productmodel.ProductName);
                param.Add("@Price", productmodel.Price);
                param.Add("@QuantityperUnit", productmodel.QuantityperUnit);
                param.Add("@ProductDesc", productmodel.ProductDesc);
                param.Add("@Status", productmodel.Status);
                var result = con.Execute("sprocProductTBInsertUpdateSingleItem", param, sqltrans, 0, System.Data.CommandType.StoredProcedure);

                if (result > 0)
                {
                    sqltrans.Commit();
                }
                else
                {
                    sqltrans.Rollback();
                }
                return result;
            }
        }
    }
}
