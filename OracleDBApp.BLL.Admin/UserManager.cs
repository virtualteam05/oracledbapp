﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using OracleDBApp.Common;
using OracleDBApp.Common.Model;
using OracleDBApp.Common.ViewModel;
using OracleDBApp.DAL;

namespace OracleDBApp.BLL.Admin
{
    public class UserManager : IUserManager
    {
        private IDataAccessManager dal;
        private IToken tokenManager;

        public UserManager(IDataAccessManager dataAccessManager,IToken tokenManager)
        {
            this.dal = dataAccessManager;
            this.tokenManager = tokenManager;
        }
        public Task DeleteUser(Users users, ResponseMessage response, int userID)
        {
            throw new NotImplementedException();
        }

        public async Task GetAllUser(ResponseMessage response)
        {

            response.ResponseObj =await dal.GetAll<hr_appskill>();
        }

        public async Task<USER_DETAILS> GetUserByEmailAndPassword(string userName, string password)
        {
            try
            {
                 

                var dyParam = new OracleDynamicParameters();
                dyParam.Add("p_user_name", OracleDbType.Varchar2, ParameterDirection.Input, userName);
                dyParam.Add("p_password", OracleDbType.Varchar2, ParameterDirection.Input, password);
                dyParam.Add("result", OracleDbType.RefCursor, ParameterDirection.ReturnValue);
                var lst = await dal.ExecuteFunction<USER_DETAILS>("f_user_data", dyParam);
                if (lst!=null && lst.ToList().Count>0 )
                {
                    return lst.ToList().FirstOrDefault();
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return null;
        }

        public Task GetUserById(int userId, ResponseMessage response)
        {
            throw new NotImplementedException();
        }

        public Task InsertUser(Users users, ResponseMessage response, int userID)
        {
            throw new NotImplementedException();
        }

        public Task UpdateUser(Users users, ResponseMessage response, int userID)
        {
            throw new NotImplementedException();
        }
    }
}
