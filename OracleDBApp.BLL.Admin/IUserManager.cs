﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OracleDBApp.Common.Model;
using OracleDBApp.Common.ViewModel;

namespace OracleDBApp.BLL.Admin
{
   public interface IUserManager
    {
         Task InsertUser(Users users,ResponseMessage response,int userID);
        Task UpdateUser(Users users, ResponseMessage response, int userID);
        Task DeleteUser(Users users, ResponseMessage response, int userID);
        Task GetAllUser(ResponseMessage response);
        Task GetUserById(int userId, ResponseMessage response);
        Task<USER_DETAILS> GetUserByEmailAndPassword(string email, string password);
    }
}
