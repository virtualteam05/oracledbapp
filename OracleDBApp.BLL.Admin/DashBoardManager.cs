﻿using Oracle.ManagedDataAccess.Client;
using OracleDBApp.Common.Model;
using OracleDBApp.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace OracleDBApp.BLL.Admin
{
   public class DashBoardManager : IDasboardManager
    {
        private IDataAccessManager dal;

        public DashBoardManager(IDataAccessManager dataAccessManager)
        {
            this.dal = dataAccessManager;
        }
        public async Task<List<Menu>> GetMenus(decimal userNo, decimal companyNo)
        {
            List<Menu> lstMenu = new List<Menu>();
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("p_user_no", OracleDbType.Decimal, ParameterDirection.Input, userNo);
                dyParam.Add("p_company_no", OracleDbType.Decimal, ParameterDirection.Input, companyNo);
                dyParam.Add("result", OracleDbType.RefCursor, ParameterDirection.ReturnValue);
                lstMenu = await dal.ExecuteFunction<Menu>("f_menu_list", dyParam) as List<Menu>;
                // var conn = this.GetConnection();
                //return user;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstMenu;
        }
    }
}
