﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OracleDBApp.Common
{
   public class Constants
    {
        public const string Authorization = "Authorization";
        public const string AuthenticationProblem = "Authentication Problem.";
        public const string Authentication_Schema = "Bearer ";
    }

}
