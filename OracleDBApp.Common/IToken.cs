﻿using OracleDBApp.Common.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OracleDBApp.Common
{
   public interface IToken
    {
        Payload Token { get; set; }
        Payload GetToken(); 
    }
}
