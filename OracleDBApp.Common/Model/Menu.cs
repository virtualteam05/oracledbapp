﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OracleDBApp.Common.Model
{
   public class Menu
    {
        public int LVL { get; set; }
        public string OBJ_NAME { get; set; }
        public string OBJ_NO { get; set; }
        public string PARENT_OBJ_NO { get; set; }
        public List<Menu> Child { get; set; }
    }
}
