﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OracleDBApp.Common.Model
{
   public class USER_DETAILS
    {
        public decimal EMP_NO { get; set; }
        public decimal USER_NO { get; set; }
        public string USER_PWD { get; set; }

        public decimal COMPANY_NO { get; set; }
        public int ACTIVE_STAT { get; set; }
        public string AUTO_REFRESH_PENDING_JOB { get; set; }
    }
}
