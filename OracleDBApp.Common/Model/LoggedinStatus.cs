﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Common.Model
{
    public partial class LoggedinStatus
    {
        public int Id { get; set; }
        public string StatusName { get; set; }
    }
}
