﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OracleDBApp.Common.ViewModel
{
   public class Payload
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime IssuedTime { get; set; }
        public DateTime ExpiredTime { get; set; }
        public decimal EMP_NO { get; set; }
        public decimal USER_NO { get; set; }
        public decimal COMPANY_NO { get; set; }

    }
}
