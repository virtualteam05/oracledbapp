﻿using OracleDBApp.Common.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OracleDBApp.Common
{
    public class TokenManager : IToken
    {
        public Payload Token { get; set; } = new Payload();

        public Payload GetToken()
        {
            return this.Token;
        }
    }
}
