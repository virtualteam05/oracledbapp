import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpResponse } from '@angular/common/http';
import { finalize, tap } from 'rxjs/operators';

import { UrlConstants } from '../helpers/UrlConstants';
import { isNullOrUndefined } from 'util';

@Injectable()
export class HttpInterceptorService {

    

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        let authToken = localStorage.getItem('token');
        var customReq;
if (isNullOrUndefined(authToken)) {
    customReq = req.clone({
        body: req.body,
        url: UrlConstants.baseUrl + req.url
    });
} else {
    customReq = req.clone({
        body: req.body,
        headers: req.headers.set('Authorization', authToken),
        url: UrlConstants.baseUrl + req.url
    });
}


   

        return next.handle(customReq).pipe(
            tap(event => {
            
                if (event instanceof HttpResponse) {
                    console.log(event);
                }
            }, error => {
                
            }),
            finalize(() => {
                
            }));
    }
}
