﻿export const UrlConstants = {

    // BaseUrl

    baseUrl: 'http://localhost:54744/api/',



    // user
    addUser: 'Users/register',
    updateUser: 'Users/update',
    searchAllUser: 'Users/all',
    changeUserPassword: 'user/changePassword',
    deleteUser: 'Users/delete',
    getUserById:"Users/id",

    //security
    login:"security/login",
    logout:"security/logout",
    //Task
    insertTask:"Tasks/save",
    searchTask:"Tasks/all"








};
