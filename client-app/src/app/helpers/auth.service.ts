import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { UrlConstants } from './UrlConstants';

@Injectable()
export class AuthService {

    constructor(private http: HttpClient) {
    }


    

    public changePassword(obj:any){
        return this.http.post(UrlConstants.changeUserPassword, obj);
    }

    public logout(): void {
        localStorage.removeItem('token');
    }
    
    public isAuthenticated(){
        debugger;
        let token = localStorage.getItem('token');
        if(token != null){
            return true;
        }
        return false;
    }
}