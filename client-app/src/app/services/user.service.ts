import { Injectable } from '@angular/core';
import { RequestMessage } from '../helpers/request-message';
import { HttpClient } from '@angular/common/http';
import { UrlConstants } from '../helpers/UrlConstants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  getUserById(userId: any) {
    this.request.RequestObj=JSON.stringify(userId);
    return this.http.post(UrlConstants.getUserById,this.request);
  }
  deleteUser(obj: any) {
    this.request.RequestObj=JSON.stringify(obj);
   return this.http.post(UrlConstants.deleteUser,this.request);
  }
  updateUser(obj: any) {
    this.request.RequestObj=JSON.stringify(obj);
   return this.http.post(UrlConstants.updateUser,this.request);
  }
  loadAllUsers() {
   return this.http.get(UrlConstants.searchAllUser);
  }
  request:RequestMessage=new RequestMessage();
  registerUser(newUserRegister: any):Observable<any> {
    this.request.RequestObj=JSON.stringify(newUserRegister);
    return this.http.post(UrlConstants.addUser,this.request);
  }

  constructor(private http:HttpClient) { }
}
