import { Injectable } from '@angular/core';
import { RequestMessage } from '../helpers/request-message';
import { Observable } from 'rxjs';
import { UrlConstants } from '../helpers/UrlConstants';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  logout() {
    return this.http.get(UrlConstants.logout);
  }
  login(user: any) {
    this.request.RequestObj=JSON.stringify(user);
    return this.http.post(UrlConstants.login,this.request);
  }

  request:RequestMessage=new RequestMessage();
  registerUser(newUserRegister: any):Observable<any> {
    this.request.RequestObj=JSON.stringify(newUserRegister);
    return this.http.post(UrlConstants.addUser,this.request);
  }

  constructor(private http:HttpClient) { }
}
