import { Component, OnInit } from '@angular/core';
import { SecurityService } from 'src/app/services/security.service';
import { ResponseMessage } from 'src/app/helpers/response-message';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private securityService:SecurityService) { }

  ngOnInit(): void {
  }
  logout(){
    this.securityService.logout().subscribe((res:ResponseMessage)=>{
      localStorage.removeItem('token');
      location.replace('login')
    })
  }
}
