import { Component, OnInit } from '@angular/core';
import { SecurityService } from 'src/app/services/security.service';
import { ResponseMessage } from 'src/app/helpers/response-message';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 user:any;
  constructor(private securityService:SecurityService) { }

  ngOnInit(): void {
    this.user=new Object();
  }
  signIn(){
    this.securityService.login(this.user).subscribe((res:ResponseMessage)=>{
      if (res.statusCode==1) {
        localStorage.setItem("token",res.token);
        location.replace('view-task');
      }
    })
  }
}
