import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ResponseMessage } from 'src/app/helpers/response-message';
import  {Observable } from 'rxjs'
import { ActivatedRoute } from '@angular/router';
import { isNullOrUndefined } from 'util';
@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  userId: any;
  urlroute: any;

  constructor(private userService:UserService,private route:ActivatedRoute) { }
  newUserRegister:any;

  ngOnInit(): void {
    this.urlroute=location.pathname;
    console.log(this.urlroute)
    this.userId=this.route.snapshot.queryParams.id;
    this.newUserRegister=new Object();
    if (!isNullOrUndefined(this.userId)&&this.userId>0) {
      this.userService.getUserById(this.userId).subscribe((res:ResponseMessage)=>{
       debugger;
          this.newUserRegister=res.responseObj;
 
      })
    }
  }
 registerUser(){

   if (this.newUserRegister.id>0) {
    this.userService.updateUser(this.newUserRegister).subscribe((res:ResponseMessage)=>{
      alert(res.message);
      location.replace('view-user');
    })
   } else {
    this.userService.registerUser(this.newUserRegister).subscribe((res:ResponseMessage)=>{
      alert(res.message);
      location.replace('view-user');
    })
   }

 }
}
