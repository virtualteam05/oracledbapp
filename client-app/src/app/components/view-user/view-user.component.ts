import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ResponseMessage } from 'src/app/helpers/response-message';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {

  constructor(private userService:UserService) { }
  lstUsers:Array<any>;
  ngOnInit(): void {
    this.lstUsers=[];
    this.loadAllUsers();
  }
  loadAllUsers(){
    this.userService.loadAllUsers().subscribe((res:ResponseMessage)=>{
      this.lstUsers=res.responseObj;
    })
  }
  updateUser(obj){
    location.replace('create-user?id='+obj.id)
  }
  deleteUser(obj){
    this.userService.deleteUser(obj).subscribe((res:ResponseMessage)=>{
      alert(res.message);
      this.loadAllUsers();
    })
  }
}
