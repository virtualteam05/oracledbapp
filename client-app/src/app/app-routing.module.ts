import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { LoginComponent } from './components/login/login.component';
import { ViewUserComponent } from './components/view-user/view-user.component';
import { AuthGuard } from './helpers/auth.guard';
import { HomeComponent } from './components/home/home.component';


const routes: Routes = [
  { path: '', redirectTo: 'view-task', pathMatch: 'full' },
  { path: 'new-user-register', component: RegisterUserComponent},
  { path: 'login', component: LoginComponent},
  { path: '', component: HomeComponent, children: [
      { path: 'view-user', component: ViewUserComponent, canActivate: [AuthGuard] },
      { path: 'create-user', component: RegisterUserComponent,canActivate:[AuthGuard]},
      { path: '**', redirectTo: 'view-task' ,pathMatch: 'full' }
  ] ,canActivate:[AuthGuard]},

  { path: '**', redirectTo: 'new-user-register' ,pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
