﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrEmpSalArear
    {
        public decimal? ArearNo { get; set; }
        public string ArearId { get; set; }
        public DateTime? ArearDatetime { get; set; }
        public decimal? ArearElement { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? EmppayrollNo { get; set; }
        public decimal? ArearAmt { get; set; }
        public DateTime? ArearStartDatetime { get; set; }
        public DateTime? ArearEndDatetime { get; set; }
        public decimal? IsInstallment { get; set; }
        public decimal? IsAutoCalculate { get; set; }
        public decimal? NoOfInstallment { get; set; }
        public decimal? PaymentInterval { get; set; }
        public decimal? PerInstallmentAmt { get; set; }
        public decimal? EffectWithsal { get; set; }
        public decimal? PaidInSeperateElement { get; set; }
        public decimal? PaidFlag { get; set; }
        public string Descr { get; set; }
        public decimal? ApprovedBy { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
