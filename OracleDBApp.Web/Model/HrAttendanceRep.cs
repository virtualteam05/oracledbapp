﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrAttendanceRep
    {
        public decimal? EmpNo { get; set; }
        public string EmpNameId { get; set; }
        public string Jobtitle { get; set; }
        public string BuName { get; set; }
        public string AttnDatetime { get; set; }
        public string Shift { get; set; }
        public string ShiftInTime { get; set; }
        public string ShiftOutTime { get; set; }
        public string Duration { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string Status { get; set; }
        public decimal? Ot { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public string SsCreatedOn { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsCreator { get; set; }
        public decimal? SsModifier { get; set; }
        public decimal? SsOgNo { get; set; }
    }
}
