﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrCarddata
    {
        public string Id { get; set; }
        public string CardNo { get; set; }
        public string AttnDatetime { get; set; }
        public string QueryFailed { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public string SsCreatedOn { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsCreator { get; set; }
        public decimal? SsModifier { get; set; }
        public decimal? SsOgNo { get; set; }
    }
}
