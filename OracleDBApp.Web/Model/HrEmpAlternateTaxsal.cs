﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrEmpAlternateTaxsal
    {
        public decimal? AlternateTaxsalNo { get; set; }
        public string AlternateTaxsalId { get; set; }
        public decimal? EmpNo { get; set; }
        public string Descr { get; set; }
        public decimal? ActiveStat { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public decimal? MasterBaFlag { get; set; }
    }
}
