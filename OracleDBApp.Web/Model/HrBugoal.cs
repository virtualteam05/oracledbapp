﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrBugoal
    {
        public decimal? BugoalNo { get; set; }
        public string BugoalId { get; set; }
        public decimal? BuNo { get; set; }
        public DateTime? GoalFromDt { get; set; }
        public DateTime? GoalToDt { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public string MeasureUnit { get; set; }
        public string Target { get; set; }
        public string InteractWith { get; set; }
        public decimal? ImpactAchive1 { get; set; }
        public decimal? ImpactAchive2 { get; set; }
        public string Outcome { get; set; }
    }
}
