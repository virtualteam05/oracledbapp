﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrBu
    {
        public decimal? BuNo { get; set; }
        public decimal? BuNoParent { get; set; }
        public string BuName { get; set; }
        public string Descr { get; set; }
        public string BuAlias { get; set; }
        public string Loc { get; set; }
        public decimal? BuTypeNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? ActiveStat { get; set; }
        public decimal? LookupdtlNo { get; set; }
        public DateTime? BuOpeningDatetime { get; set; }
        public string BuAddress { get; set; }
        public string BuPhone { get; set; }
        public string BuCategory { get; set; }
        public decimal? ContractBy { get; set; }
        public decimal? OperationBy { get; set; }
        public string BuLocationType { get; set; }
        public string BuGrade { get; set; }
        public decimal? ReportSlNo { get; set; }
        public string BuCategoryFlag { get; set; }
        public decimal? BaNo { get; set; }
        public string BuNoTree { get; set; }
        public string BuNameTree { get; set; }
        public decimal? IsLeaf { get; set; }
        public string BuNameLevel { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public string BuCode { get; set; }
        public decimal? RootBuNo { get; set; }
        public decimal? RefCompanyNo { get; set; }
        public decimal? TreeLevel { get; set; }
        public string BuAddress2 { get; set; }
        public string Fax { get; set; }
        public string EMail { get; set; }
        public string Web { get; set; }
        public decimal? CashAccNo { get; set; }
        public decimal? ChequeAccNo { get; set; }
        public decimal? LcAccNo { get; set; }
        public string NlsBuName { get; set; }
        public string CreditFlag { get; set; }
        public decimal? BuActivityTypeNo { get; set; }
        public decimal? DefaltFlag { get; set; }
        public decimal? ProductionActivityNo { get; set; }
    }
}
