﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrElement
    {
        public decimal? ElementNo { get; set; }
        public string ElementName { get; set; }
        public decimal? Gross { get; set; }
        public string Earning { get; set; }
        public decimal? Recurring { get; set; }
        public decimal? Active { get; set; }
        public string Descr { get; set; }
        public decimal? SlNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public string EffectOnSal { get; set; }
        public decimal? EffectType { get; set; }
        public string ElementId { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? IsBasic { get; set; }
        public decimal? IsTaxable { get; set; }
        public decimal? ExtraPageInSalBill { get; set; }
        public decimal? OtherType { get; set; }
        public decimal? HideFromRep { get; set; }
        public string ElementNameBangla { get; set; }
        public decimal? IsCompensation { get; set; }
        public decimal? CashRatio { get; set; }
        public decimal? ChequeRatio { get; set; }
        public decimal? GroupNo { get; set; }
        public decimal? SkipCalculation { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public decimal? AccNo { get; set; }
        public string AmountType { get; set; }
    }
}
