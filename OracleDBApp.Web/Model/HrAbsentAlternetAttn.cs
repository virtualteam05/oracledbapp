﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrAbsentAlternetAttn
    {
        public decimal? AtndNo { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? ShiftNo { get; set; }
        public DateTime? InTime { get; set; }
        public DateTime? OutTime { get; set; }
        public DateTime? AttnDatetime { get; set; }
        public decimal? LateStat { get; set; }
        public string DayStat { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public DateTime? ShiftInTime { get; set; }
        public DateTime? ShiftOutTime { get; set; }
        public DateTime? BeforeGraceIntime { get; set; }
        public DateTime? AfterGraceIntime { get; set; }
        public decimal? LateentryApproved { get; set; }
        public decimal? EarlyexitApproved { get; set; }
        public decimal? ApprovedBy { get; set; }
        public string LateentryReason { get; set; }
        public string EarlyexitReason { get; set; }
        public decimal? SsOgNo { get; set; }
        public string SsCheckedOn { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public decimal? Ot { get; set; }
        public decimal? OtRatePerHr { get; set; }
        public decimal? OtAmt { get; set; }
        public decimal? HrData { get; set; }
        public decimal? HrDataPhr { get; set; }
        public decimal? HrDataAmt { get; set; }
        public decimal? PaidFlg { get; set; }
        public DateTime? PaidDatetime { get; set; }
        public decimal? ElAvail { get; set; }
        public string RemarksManualAtten { get; set; }
    }
}
