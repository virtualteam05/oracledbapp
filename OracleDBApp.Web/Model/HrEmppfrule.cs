﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrEmppfrule
    {
        public decimal? ElementNo { get; set; }
        public string Peron { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? PfruleNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public DateTime? RuleDatetime { get; set; }
        public decimal? OwnPer { get; set; }
        public decimal? Amt { get; set; }
        public decimal? MaxAmt { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? ProbationMonth { get; set; }
        public string PfType { get; set; }
        public decimal? CashRatio { get; set; }
        public decimal? ChequeRatio { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
