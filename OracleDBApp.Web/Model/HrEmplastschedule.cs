﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrEmplastschedule
    {
        public decimal? LastScheduleNo { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? LastRuleDuration { get; set; }
        public decimal? LastScheduleDuration { get; set; }
        public decimal? DurationPolNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
