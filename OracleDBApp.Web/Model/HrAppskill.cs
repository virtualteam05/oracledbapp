﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrAppskill
    {
        public decimal? AppskillNo { get; set; }
        public string SkillName { get; set; }
        public decimal? AppNo { get; set; }
        public decimal? EmpNo { get; set; }
        public string Descr { get; set; }
        public string SkillLevel { get; set; }
        public string ExpYears { get; set; }
        public string LastUsed { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
