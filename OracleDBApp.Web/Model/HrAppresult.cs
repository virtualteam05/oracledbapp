﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrAppresult
    {
        public string RollNo { get; set; }
        public decimal? AppNo { get; set; }
        public decimal? Secton1 { get; set; }
        public decimal? Secton2 { get; set; }
        public decimal? Secton3 { get; set; }
        public decimal? Secton4 { get; set; }
        public decimal? Secton5 { get; set; }
        public decimal? Selected { get; set; }
        public decimal? ResultRank { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
