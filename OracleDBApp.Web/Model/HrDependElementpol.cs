﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrDependElementpol
    {
        public decimal? DependNo { get; set; }
        public decimal? JobtitleNo { get; set; }
        public decimal? ElementNo { get; set; }
        public string Peron { get; set; }
        public decimal? Per { get; set; }
        public decimal? ProcessDurationMonth { get; set; }
        public decimal? ProvisionMonth { get; set; }
        public decimal? FixedAmt { get; set; }
        public decimal? MaxAmt { get; set; }
        public decimal? CashRatio { get; set; }
        public decimal? ChequeRatio { get; set; }
        public DateTime? EffectiveDatetime { get; set; }
        public decimal? NoOfOccurance { get; set; }
        public decimal? Conjucative { get; set; }
        public decimal? ActiveStat { get; set; }
        public decimal? CompanyNo { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public decimal? ProcessDependOn { get; set; }
        public decimal? DeductElementNo { get; set; }
        public decimal? AmtDay { get; set; }
        public string AmtDayOn { get; set; }
        public decimal? SameAsElementAmt { get; set; }
        public decimal? MultiplyBy { get; set; }
    }
}
