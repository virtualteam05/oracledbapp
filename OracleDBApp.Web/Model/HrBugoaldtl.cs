﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrBugoaldtl
    {
        public decimal? BugoaldtlNo { get; set; }
        public decimal? BugoalNo { get; set; }
        public decimal? ObjectiveSerial { get; set; }
        public string Objectives { get; set; }
        public decimal? ActiveStat { get; set; }
        public DateTime? EffectiveToDatetime { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public DateTime? EffectiveFromDatetime { get; set; }
    }
}
