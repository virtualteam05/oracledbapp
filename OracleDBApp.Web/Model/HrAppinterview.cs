﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrAppinterview
    {
        public decimal? AppinterviewNo { get; set; }
        public decimal? InterviewNo { get; set; }
        public decimal? AppNo { get; set; }
        public decimal? GetMark { get; set; }
        public string Comments { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? InterviewLetterNo { get; set; }
        public decimal? NextLevel { get; set; }
        public string RollNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
