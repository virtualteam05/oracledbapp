﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrBill
    {
        public decimal? BillNo { get; set; }
        public DateTime? BillDatetime { get; set; }
        public decimal? PreparedBy { get; set; }
        public decimal? CheckedBy { get; set; }
        public decimal? ApprovedBy { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public string BillId { get; set; }
        public decimal? CheckedFlg { get; set; }
        public string ApproveFlg { get; set; }
        public decimal? AstCrAccNo { get; set; }
        public decimal? VNo { get; set; }
        public DateTime? CheckDatetime { get; set; }
        public DateTime? ApproveDatetime { get; set; }
        public decimal? LblCrAccNo { get; set; }
        public decimal? PayVNo { get; set; }
        public DateTime? PayDatetime { get; set; }
        public decimal? PaidBy { get; set; }
        public string ReceivedBy { get; set; }
        public string Pay { get; set; }
        public decimal? BilltypedtlNo { get; set; }
        public string FileExt { get; set; }
        public decimal? FileSize { get; set; }
        public string AttachFileName { get; set; }
        public decimal? AttachFileNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
