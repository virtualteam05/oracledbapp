﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrDynamicRepCol
    {
        public decimal? Col1 { get; set; }
        public string Col1Value { get; set; }
        public string Col1Name { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public string SsCreatedOn { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsCreator { get; set; }
        public decimal? SsModifier { get; set; }
        public decimal? SsOgNo { get; set; }
    }
}
