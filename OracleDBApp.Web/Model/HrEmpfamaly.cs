﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrEmpfamaly
    {
        public decimal? SlNo { get; set; }
        public decimal? EmpNo { get; set; }
        public string Fname { get; set; }
        public string Relation { get; set; }
        public DateTime? Dob { get; set; }
        public string CurrentStatus { get; set; }
        public string Description { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
