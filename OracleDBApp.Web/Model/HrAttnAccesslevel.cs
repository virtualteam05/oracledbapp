﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrAttnAccesslevel
    {
        public decimal? AccesslevelNo { get; set; }
        public string AccesslevelName { get; set; }
    }
}
