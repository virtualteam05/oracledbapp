﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrEmpDoc
    {
        public decimal? DocNo { get; set; }
        public decimal? DoctypeNo { get; set; }
        public string RefNo { get; set; }
        public DateTime? PrintDatetime { get; set; }
        public string DocHeader { get; set; }
        public string DocBody { get; set; }
        public string DocFooter { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? AppNo { get; set; }
        public string FileExt { get; set; }
        public decimal? FileSize { get; set; }
        public string AttachFileName { get; set; }
        public decimal? AttachFileNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public DateTime? LastInterviewDatetime { get; set; }
        public DateTime? LastApplicationDatetime { get; set; }
        public DateTime? JoinDatetime { get; set; }
        public decimal? TraineeAmount { get; set; }
        public string Test { get; set; }
        public string Test2 { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
