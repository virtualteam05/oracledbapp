﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrAttnCorrectionTemp
    {
        public string EmpId { get; set; }
        public string EmpNameId { get; set; }
        public DateTime? CalDt { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? BuNo { get; set; }
        public string BuName { get; set; }
        public decimal? JobtitleNo { get; set; }
        public string Jobtitle { get; set; }
        public DateTime? SchDatetime { get; set; }
        public decimal? SchShift { get; set; }
        public DateTime? SchInTime { get; set; }
        public DateTime? InTime { get; set; }
        public DateTime? SchOutTime { get; set; }
        public DateTime? OutTime { get; set; }
        public string Status { get; set; }
        public decimal? ActiveStat { get; set; }
    }
}
