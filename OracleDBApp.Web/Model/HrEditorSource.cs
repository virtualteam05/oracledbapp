﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrEditorSource
    {
        public decimal? SourceNo { get; set; }
        public string SourceId { get; set; }
        public string SourceName { get; set; }
        public string SourceType { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public string SqlTxt { get; set; }
        public string ShareType { get; set; }
        public string SelectedUsers { get; set; }
    }
}
