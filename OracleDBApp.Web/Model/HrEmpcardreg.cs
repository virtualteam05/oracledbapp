﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrEmpcardreg
    {
        public decimal? CardRegNo { get; set; }
        public decimal? EmpNo { get; set; }
        public string CardNo { get; set; }
        public DateTime? ActivationDatetime { get; set; }
        public decimal? AccesslevelNo { get; set; }
        public decimal? Activeuser { get; set; }
        public string Antipassbackind { get; set; }
        public string Alarmuser { get; set; }
        public decimal? ActiveStat { get; set; }
        public DateTime? Expirydatetime { get; set; }
        public decimal? Cardtypeid { get; set; }
        public decimal? CardRegBy { get; set; }
        public DateTime? CardRegDatetime { get; set; }
        public decimal? RegCompleteFlag { get; set; }
        public decimal? RegCompleteBy { get; set; }
        public DateTime? RegCompleteDatetime { get; set; }
        public decimal? ServerSubmitFlag { get; set; }
        public DateTime? ServerSubmitDatetime { get; set; }
        public decimal? CompanyNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
