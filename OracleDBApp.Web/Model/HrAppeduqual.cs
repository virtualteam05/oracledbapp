﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrAppeduqual
    {
        public decimal? AppeduqualNo { get; set; }
        public decimal? EdulevelNo { get; set; }
        public decimal? EduareaNo { get; set; }
        public decimal? EdumajorNo { get; set; }
        public decimal? InstNo { get; set; }
        public decimal? Cgpa { get; set; }
        public decimal? OutOf { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? YearOfPass { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? AppNo { get; set; }
        public decimal? SsOgNo { get; set; }
        public string ResultType { get; set; }
        public string Grade { get; set; }
        public decimal? LastQualFlag { get; set; }
        public string Remarks { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public decimal? EduboardNo { get; set; }
    }
}
