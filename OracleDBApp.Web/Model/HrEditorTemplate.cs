﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrEditorTemplate
    {
        public decimal? EditorTemplateNo { get; set; }
        public string EditorTemplateId { get; set; }
        public string TemplateName { get; set; }
        public decimal? SourceNo { get; set; }
        public string Descr { get; set; }
        public string ShareType { get; set; }
        public string SelectedUsers { get; set; }
        public string TemplateBody { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
