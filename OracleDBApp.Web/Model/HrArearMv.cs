﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrArearMv
    {
        public decimal? ArrElementNo { get; set; }
        public string ArrElementName { get; set; }
        public decimal? Amount { get; set; }
        public decimal? ArrAmount { get; set; }
        public decimal? ActAmount { get; set; }
        public decimal? SalAmount { get; set; }
        public DateTime? SsCreatedOn { get; set; }
        public DateTime? SsModifiedOn { get; set; }
    }
}
