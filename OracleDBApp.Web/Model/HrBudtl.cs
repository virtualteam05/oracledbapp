﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrBudtl
    {
        public decimal? BudtlNo { get; set; }
        public decimal? BuNo { get; set; }
        public string Descr { get; set; }
        public decimal? InChargeType { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? JobtitleNo { get; set; }
        public DateTime? StartDatetime { get; set; }
        public DateTime? EndDatetime { get; set; }
        public decimal? ActiveStat { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
