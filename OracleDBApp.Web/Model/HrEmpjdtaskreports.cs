﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrEmpjdtaskreports
    {
        public decimal? EmpjdtaskreportsNo { get; set; }
        public decimal? EmpJdDtlNo { get; set; }
        public string Type { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? BuNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
