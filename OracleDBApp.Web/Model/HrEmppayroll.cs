﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Web.Model
{
    public partial class HrEmppayroll
    {
        public decimal? EmppayrollNo { get; set; }
        public DateTime? EffectiveDatetime { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? JobtitleNo { get; set; }
        public decimal? IncrementNo { get; set; }
        public decimal? TransferNo { get; set; }
        public decimal? GradeNo { get; set; }
        public decimal? BuNo { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? StepNo { get; set; }
        public decimal? CurrNo { get; set; }
        public decimal? ExchangeRate { get; set; }
        public decimal? CashAmt { get; set; }
        public decimal? ChequeAmt { get; set; }
        public decimal? CashRatio { get; set; }
        public decimal? ChequeRatio { get; set; }
        public decimal? CashPayment { get; set; }
        public decimal? ChequePayment { get; set; }
        public decimal? Gross { get; set; }
        public string CalBasicFromGrossFormula { get; set; }
        public decimal? PolNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
