﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace OracleDBApp.Web.Models
{
    public class CommonFunction
    {
        public static T ConvertToObject<T>(FormCollection form)
        {
            var model = Activator.CreateInstance(typeof(T));
            Type modelType = model.GetType();

            foreach (PropertyInfo propertyInfo in modelType.GetProperties())
            {
                var mykey = propertyInfo.Name;
                if (propertyInfo.CanRead && form.AllKeys.Contains(mykey))
                {
                    try
                    {
                        var value = form[mykey];
                        propertyInfo.SetValue(model, value);
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
            return (T)model;
        }
    }
}