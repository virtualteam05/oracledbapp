﻿using OracleDBApp.Web.Model;
using OracleDBApp.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OracleDBApp.Web.Controllers
{
    public class HrEmpController : Controller
    {
        // GET: HrEmp
        public ActionResult Index()
        {
            return View();
        }

        // GET: HrEmp/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: HrEmp/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HrEmp/Create
        [HttpPost]
        public ActionResult Create(HrEmp collection)
        {
            try
            {
                // TODO: Add insert logic here
                //HrEmp empNo = CommonFunction.ConvertToObject<HrEmp>(collection);
                return RedirectToAction("Create");
            }
            catch
            {
                return View();
            }
        }

        // GET: HrEmp/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: HrEmp/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: HrEmp/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HrEmp/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
