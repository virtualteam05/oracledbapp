﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using OracleDBApp.BLL.Admin;
using OracleDBApp.Common;
using OracleDBApp.Common.Model;
using OracleDBApp.Common.ViewModel;
using OracleDBApp.Service.Helper;

namespace OracleDBApp.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : ControllerBase
    {
        public IConfiguration _configuration;
        private IToken _tokenManager;
        private IUserManager userManager;
        private IDasboardManager dashboardManager;
        private string user_name;
        private decimal user_no;
        private decimal company_no;
        public SecurityController(IUserManager _userManager, IConfiguration configuration,IDasboardManager _dashboadManager,IToken tokenManager)
        {
            this._tokenManager = tokenManager;
            this.userManager = _userManager;
            this.dashboardManager = _dashboadManager;
            _configuration = configuration;
        }

        [HttpPost("login")]
        public async Task<ResponseMessage> Login(RequestMessage message)
        {
            ResponseMessage response = new ResponseMessage();
            try
            {
                Users _userData = JsonConvert.DeserializeObject<Users>(message.RequestObj.ToString());
                if (_userData != null  && _userData.Password != null)
                {
                    USER_DETAILS _DETAILS = await userManager.GetUserByEmailAndPassword(_userData.UserName, _userData.Password);
                    if (_DETAILS != null)
                    {


                       GenerateToken(_DETAILS,_userData, response);
                    }
                    else
                    {
                        response.Message = "Invalid credentials";
                        response.StatusCode = (int)Common.Enums.StatusCode.Failed;
                    }
                }
                else
                {
                    response.Message = "Invalid request";
                    response.StatusCode = (int)Common.Enums.StatusCode.Failed;
                }
            }
            catch (Exception ex)
            {
                response.Message = "Invalid credentials";
                response.StatusCode = (int)Common.Enums.StatusCode.Failed;
            }


            return response;
        }

        [HttpGet("dashboard")]
        public async Task<ResponseMessage> GetDashboard()
        {
            ResponseMessage response = new ResponseMessage();
            try
            {
                GetUserIdFromToken();
                //TO DO: Using userId logout functionality;
                List<Menu> lst = await dashboardManager.GetMenus(this.user_no, this.company_no);
                var mainMenus = lst.BuildTree();
                response.ResponseObj = mainMenus;
               // response.ResponseObj = await dashboardManager.GetMenus(this.user_no,this.company_no);
                response.Message = "Logout successfully";
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)Common.Enums.StatusCode.Exception;
                response.Message = "An error has occurred";
            }
            return response;
        }

        [HttpGet("logout")]
        public async Task<ResponseMessage> Logout()
        {
            ResponseMessage response = new ResponseMessage();
            try
            {
                //GetUserIdFromToken();
                //TO DO: Using userId logout functionality;
                response.ResponseObj= await userManager.GetUserByEmailAndPassword("data","123");
                response.Message = "Logout successfully";
            }
            catch (Exception ex)
            {
                response.StatusCode = (int)Common.Enums.StatusCode.Exception;
                response.Message = "An error has occurred";
            }
            return response;
        }
        private void GetUserIdFromToken()
        {
            string tokenStr = Request.Headers[OracleDBApp.Common.Constants.Authorization].ToString();
            var token = new JwtSecurityTokenHandler().ReadJwtToken(tokenStr).Payload;
            this.user_name = token["UserName"].ToString();
            this.user_no = Convert.ToDecimal(token["USER_NO"].ToString());
            this.company_no= Convert.ToDecimal(token["COMPANY_NO"].ToString());
        }
        private void GenerateToken(USER_DETAILS details,Users user, ResponseMessage response)
        {
            var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                    new Claim("UserName", user.UserName),
                    new Claim("COMPANY_NO", details.COMPANY_NO.ToString()),
                    new Claim("EMP_NO", details.EMP_NO.ToString()),
                    new Claim("USER_NO", details.USER_NO.ToString())
                   };
            var payLoad = new { UserName = user.UserName,COMPANY_NO=details.COMPANY_NO,EMP_NO=details.EMP_NO,USER_NO=details.USER_NO,IssuedTime=DateTime.Now,ExpiredTime=DateTime.Now.AddMinutes(20) ,Email=""};
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

            var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Audience"], claims, expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);
            response.Token = new JwtSecurityTokenHandler().WriteToken(token);
        }
        //public void GenerateCustomJwtToken(Users user, ResponseMessage response)
        //{
        //    var mySecret = "asdv234234^&%&^%&^hjsdfb2%%%";
        //    var mySecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(mySecret));

        //    var myIssuer = "http://mysite.com";
        //    var myAudience = "http://myaudience.com";

        //    var tokenHandler = new JwtSecurityTokenHandler();
        //    var tokenDescriptor = new SecurityTokenDescriptor
        //    {
        //        Subject = new ClaimsIdentity(new Claim[]
        //        {
        //    new Claim(ClaimTypes.NameIdentifier, userId.ToString()),
        //        }),
        //        Expires = DateTime.UtcNow.AddDays(7),
        //        Issuer = myIssuer,
        //        Audience = myAudience,
        //        SigningCredentials = new SigningCredentials(mySecurityKey, SecurityAlgorithms.HmacSha256Signature)
        //    };

        //    var token = tokenHandler.CreateToken(tokenDescriptor);
        //    response.ResponseObj = tokenHandler.WriteToken(token);
        //}
       public void newmethod()
        {
            //test to do;
        }
    }
}