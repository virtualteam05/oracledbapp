using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using OracleDBApp.BLL.Admin;
using OracleDBApp.Common;
using OracleDBApp.DAL;

namespace OracleDBApp.Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddScoped<IUserManager, UserManager>();
            services.AddScoped<IDasboardManager, DashBoardManager>();
            services.AddScoped<IToken, TokenManager>();
            services.AddScoped<IDataAccessManager, DataAccessManager>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = Configuration["Jwt:Audience"],
                    ValidIssuer = Configuration["Jwt:Issuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
              builder.AllowAnyOrigin()
                     .AllowAnyHeader()
                     .AllowAnyMethod());

            app.UseHttpsRedirection();
            app.UseMiddleware<ServerMessageHandler>();


            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
           
            //app.Run(async (context) =>
            //{
            //    //Demo: Basic ODP.NET Core application for ASP.NET Core
            //    // to connect, query, and return results to a web page

            //    //Create a connection to Oracle			
            //    string conString = Configuration["ConnectionString"].ToString();

            //    //How to connect to an Oracle DB without SQL*Net configuration file
            //    //  also known as tnsnames.ora.
               

            //    //How to connect to an Oracle DB with a DB alias.
            //    //Uncomment below and comment above.
            //    //"Data Source=<service name alias>;";

            //    using (OracleConnection con = new OracleConnection(conString))
            //    {
            //        using (OracleCommand cmd = con.CreateCommand())
            //        {
            //            try
            //            {
            //                Dictionary<string, object> config = Configuration.GetSection("OracleConfiguration").Get<Dictionary<string, object>>();
            //                // This sample demonstrates how to use ODP.NET Core Configuration API

            //                // Add connect descriptors and net service names entries.
            //                //OracleConfiguration.OracleDataSources.Add("orclpdb", "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=<hostname or IP>)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=<service name>)(SERVER=dedicated)))");
            //                OracleConfiguration.OracleDataSources.Add("orcl", Configuration["ORCL"].ToString());

            //                // Set default statement cache size to be used by all connections.
            //                OracleConfiguration.StatementCacheSize = Convert.ToInt32(config["StatementCacheSize"]);

            //                // Disable self tuning by default.
            //                OracleConfiguration.SelfTuning = Convert.ToBoolean(config["SelfTuning"]);

            //                // Bind all parameters by name.
            //                OracleConfiguration.BindByName = Convert.ToBoolean(config["BindByName"]);

            //                // Set default timeout to 60 seconds.
            //                OracleConfiguration.CommandTimeout = Convert.ToInt32(config["CommandTimeout"]);

            //                // Set default fetch size as 1 MB.
            //                OracleConfiguration.FetchSize = Convert.ToInt32(config["FetchSize"]);

            //                // Set tracing options
            //                OracleConfiguration.TraceOption = Convert.ToInt32(config["TraceOption"]);
            //                OracleConfiguration.TraceFileLocation = Convert.ToString(config["TraceFileLocation"]);
            //                // Uncomment below to generate trace files
            //                //OracleConfiguration.TraceLevel = 7;

            //                // Set network properties
            //                OracleConfiguration.SendBufferSize = Convert.ToInt32(config["SendBufferSize"]);
            //                OracleConfiguration.ReceiveBufferSize = Convert.ToInt32(config["ReceiveBufferSize"]);
            //                OracleConfiguration.DisableOOB = Convert.ToBoolean(config["DisableOOB"]);
            //                con.Open();
            //                cmd.BindByName = true;
            //                cmd.Parameters.Add("pin_deptno", 20);
            //                //Use the command to display employee names from 
            //                // the EMPLOYEES table
            //                cmd.CommandText = "select *from hr_appskill";
                            
            //                // Assign id to the department number 50 
            //                OracleParameter id = new OracleParameter("id", OracleDbType.Int32,7,ParameterDirection.Input);
            //                //cmd.Parameters.Add(id);

            //                //Execute the command and use DataReader to display the data
            //                OracleDataReader reader = cmd.ExecuteReader();
            //                while (reader.Read())
            //                {
            //                    await context.Response.WriteAsync("Employee First Name: " + reader.GetInt16(0) + "\n");
            //                }

            //                reader.Dispose();
            //            }
            //            catch (Exception ex)
            //            {
            //                await context.Response.WriteAsync(ex.Message);
            //            }
            //        }
            //    }

            //  });
            }
    }
}
