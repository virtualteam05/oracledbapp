﻿using OracleDBApp.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OracleDBApp.Service.Helper
{
    public static class GroupEnumerable
    {
        public static IList<Menu> BuildTree(this IEnumerable<Menu> source)
        {
            var groups = source.GroupBy(i => i.PARENT_OBJ_NO);

            var roots = groups.FirstOrDefault(g => g.Key == "M").ToList();

            if (roots.Count > 0)
            {
                var dict = groups.Where(g => g.Key!="M").ToDictionary(g => g.Key, g => g.ToList());
                for (int i = 0; i < roots.Count; i++)
                    AddChildren(roots[i], dict);
            }

            return roots;
        }

        private static void AddChildren(Menu node, IDictionary<string, List<Menu>> source)
        {
            if (source.ContainsKey(node.OBJ_NO))
            {
                node.Child = source[node.OBJ_NO];
                for (int i = 0; i < node.Child.Count; i++)
                    AddChildren(node.Child[i], source);
            }
            else
            {
                node.Child = new List<Menu>();
            }
        }
    }
}
