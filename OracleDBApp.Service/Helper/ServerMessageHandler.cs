﻿
using Microsoft.AspNetCore.Http;
using OracleDBApp.Common;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace OracleDBApp.Service
{
    public class ServerMessageHandler
    {
        private readonly RequestDelegate _next;
        private IToken _token;

        public ServerMessageHandler(RequestDelegate next)
        {
            _next = next;
           
        }
        public async Task Invoke(HttpContext httpContext, IToken itoken)
        {
            this._token = itoken;
            if (httpContext.Request.Path.ToString().Contains("login")|| httpContext.Request.Path.ToString().Contains("weatherforecast"))
            {
                await _next.Invoke(httpContext);
            }
            else
            {
                if (httpContext.Request.Headers.ContainsKey(OracleDBApp.Common.Constants.Authorization))
                {
                    if (!string.IsNullOrEmpty(httpContext.Request.Headers[OracleDBApp.Common.Constants.Authorization].ToString()))
                    {
                        string token = httpContext.Request.Headers[OracleDBApp.Common.Constants.Authorization].ToString().Replace(Common.Constants.Authentication_Schema, "");

                        try
                        {
                           var tokenObj = new JwtSecurityTokenHandler().ReadJwtToken(token).Payload;
                            this._token.Token.UserName = tokenObj["UserName"].ToString();
                            this._token.Token.USER_NO = Convert.ToDecimal(tokenObj["USER_NO"].ToString());
                            this._token.Token.EMP_NO = Convert.ToDecimal(tokenObj["EMP_NO"].ToString());
                        }
                        catch (Exception ex)
                        {
                            httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            httpContext.Response.ContentType = "application/json";
                            await httpContext.Response.WriteAsync(ex.Message);
                            return;
                        }
                        if (!string.IsNullOrEmpty(token))
                        {
                            await _next.Invoke(httpContext);
                        }
                        else
                        {
                            httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            httpContext.Response.ContentType = "application/json";
                            await httpContext.Response.WriteAsync(Common.Constants.AuthenticationProblem);
                            return;
                        }
                    }
                    else
                    {
                        httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        httpContext.Response.ContentType = "application/json";
                        await httpContext.Response.WriteAsync(Common.Constants.AuthenticationProblem);
                        return;
                    }
                }
                else
                {
                    httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    httpContext.Response.ContentType = "application/json";
                    await httpContext.Response.WriteAsync(Common.Constants.AuthenticationProblem);
                    return;
                }

                return;
            }
        }



        private void DecryptRequest(HttpRequestMessage request)
        {
            byte[] requestByte = request.Content.ReadAsByteArrayAsync().Result;
            //byte[] decryptedRequestByte = EncryptionHelper.RijndaelEncryption().Decrypt(requestByte, decryptionKey);

            HttpContent decryptedContent = new StreamContent(new System.IO.MemoryStream(requestByte));
            decryptedContent.Headers.ContentType = request.Content.Headers.ContentType;

            request.Content = decryptedContent;

        }
        private void EncryptResponse(HttpResponseMessage response)
        {
            if (response.Content != null)
            {
                byte[] responseByte = response.Content.ReadAsByteArrayAsync().Result;
                HttpContent encryptedContent = new StreamContent(new System.IO.MemoryStream(responseByte));
                encryptedContent.Headers.ContentType = response.Content.Headers.ContentType;
                response.Content = encryptedContent;
            }
        }



    }
}