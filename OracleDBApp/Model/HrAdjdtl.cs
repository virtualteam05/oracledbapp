﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrAdjdtl
    {
        public decimal? AdjdtlNo { get; set; }
        public decimal? AdjNo { get; set; }
        public decimal? AdjAmt { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public string Descr { get; set; }
        public decimal? ElementNo { get; set; }
        public decimal? SalaryNo { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
