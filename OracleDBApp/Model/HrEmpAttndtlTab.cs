﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrEmpAttndtlTab
    {
        public DateTime? CalDt { get; set; }
        public string DdStatus { get; set; }
        public decimal? EmpNo { get; set; }
        public DateTime? SchDatetime { get; set; }
        public decimal? SchShift { get; set; }
        public DateTime? SchInTime { get; set; }
        public DateTime? SchOutTime { get; set; }
        public decimal? AtndNo { get; set; }
        public string ShiftId { get; set; }
        public decimal? AtnShift { get; set; }
        public DateTime? InTime { get; set; }
        public string InTimeTfmt { get; set; }
        public DateTime? GenInTime { get; set; }
        public string GenInTimeTmft { get; set; }
        public DateTime? OutTime { get; set; }
        public string OutTimeTfmt { get; set; }
        public DateTime? GenOutTime { get; set; }
        public string GenOutTimeTfmt { get; set; }
        public decimal? ShiftWorkHour { get; set; }
        public decimal? ActWorkHour { get; set; }
        public decimal? LateStat { get; set; }
        public string Ot { get; set; }
        public string OtTfmt { get; set; }
        public string OthOt { get; set; }
        public string OthOtTfmt { get; set; }
        public decimal? ActOthOt { get; set; }
        public string ActOthOtTfmt { get; set; }
        public decimal? LateentryApproved { get; set; }
        public decimal? EarlyExitStat { get; set; }
        public decimal? EarlyexitApproved { get; set; }
        public string Status { get; set; }
        public string Prefix { get; set; }
        public decimal? BuNo { get; set; }
        public string BuName { get; set; }
        public string EmpId { get; set; }
        public string EmpNameId { get; set; }
        public string EmpName { get; set; }
        public decimal? JobtitleNo { get; set; }
        public string Jobtitle { get; set; }
        public decimal? ActiveStat { get; set; }
        public decimal? ReportingTo { get; set; }
        public decimal? PeriodNo { get; set; }
        public decimal? OtStat { get; set; }
        public decimal? AttnSchedulePol { get; set; }
        public DateTime? JoinDatetime { get; set; }
        public decimal? EmpType { get; set; }
        public string EmpTypeName { get; set; }
        public string HrType { get; set; }
        public string JobType { get; set; }
        public decimal? SalReportSlNo { get; set; }
        public string EarlyexitReason { get; set; }
        public string LateentryReason { get; set; }
        public decimal? DepartmentNo { get; set; }
        public string Department { get; set; }
        public decimal? SectionNo { get; set; }
        public string Section { get; set; }
        public string Gender { get; set; }
        public decimal? OtMultiply { get; set; }
        public decimal? ExtraOtMultiply { get; set; }
        public string TotOtTfmt { get; set; }
        public decimal? TotOt { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public string BuNoAll { get; set; }
        public string SsCreatedOn { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsCreator { get; set; }
        public decimal? SsModifier { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? UnadjustedOthOt { get; set; }
        public decimal? UnadjustedTotOt { get; set; }
        public string RemarksManualAtten { get; set; }
    }
}
