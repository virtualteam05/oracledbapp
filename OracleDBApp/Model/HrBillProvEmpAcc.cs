﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrBillProvEmpAcc
    {
        public decimal? ProaccNo { get; set; }
        public decimal? AccNo { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? BilltypedtlNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
