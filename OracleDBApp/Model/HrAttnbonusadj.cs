﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrAttnbonusadj
    {
        public decimal? HrAttnbonusadjNo { get; set; }
        public decimal? SalaryNo { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? AdjAmount { get; set; }
        public DateTime? AdjDatetime { get; set; }
        public decimal? ApproveBy { get; set; }
        public DateTime? ApproveDatetime { get; set; }
        public decimal? ElementNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public decimal? AdjsalaryNo { get; set; }
    }
}
