﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrAppinfo
    {
        public decimal? NoticedtlNo { get; set; }
        public decimal? AppNo { get; set; }
        public DateTime? AppDatetime { get; set; }
        public decimal? CvAttchFileNo { get; set; }
        public string CvAttchFileName { get; set; }
        public decimal? FileSize { get; set; }
        public string FileExt { get; set; }
        public string TrackNo { get; set; }
        public decimal? ShortListed { get; set; }
        public string Salutation { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string HusbandName { get; set; }
        public string PrAddr1 { get; set; }
        public string PrAddr2 { get; set; }
        public decimal? PrAddrDist { get; set; }
        public decimal? PrAddrCountry { get; set; }
        public string PreferedAddr { get; set; }
        public string Nationality { get; set; }
        public string EmailPersonal { get; set; }
        public string EmailOfficial { get; set; }
        public string PhoneHome { get; set; }
        public string PhoneOffice { get; set; }
        public string PhoneMobile { get; set; }
        public string PreferedPhone { get; set; }
        public string ExtraCurAtc { get; set; }
        public string FaxNo { get; set; }
        public string MStatus { get; set; }
        public string Religion { get; set; }
        public string ProMembership { get; set; }
        public string PreferedLvl { get; set; }
        public string CareerObjective { get; set; }
        public string AvailableFor { get; set; }
        public decimal? PresentSalary { get; set; }
        public decimal? ExpectedSalary { get; set; }
        public DateTime? JoinTime { get; set; }
        public string AppBefore { get; set; }
        public string AppReason { get; set; }
        public string Interest { get; set; }
        public string PeAddr1 { get; set; }
        public string PeAddr2 { get; set; }
        public decimal? PeAddrDist { get; set; }
        public decimal? PeAddrCountry { get; set; }
        public string Gender { get; set; }
        public DateTime? Dob { get; set; }
        public string PrAddrPost { get; set; }
        public string PeAddrPost { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? Selected { get; set; }
        public decimal? AppointmentLetterNo { get; set; }
        public string FathersOccupation { get; set; }
        public string MothersOccupation { get; set; }
        public string HusbandOccupaton { get; set; }
        public string PassportNo { get; set; }
        public string DrivinglcNo { get; set; }
        public string TinNo { get; set; }
        public string SsNo { get; set; }
        public string ImgNo { get; set; }
        public string ImgExt { get; set; }
        public decimal? YearOfExp { get; set; }
        public string PayorderNo { get; set; }
        public string SerialNo { get; set; }
        public DateTime? PayorderDatetime { get; set; }
        public decimal? BankNo { get; set; }
        public string RollNo { get; set; }
        public decimal? SlNo { get; set; }
        public string Mimetype { get; set; }
        public byte[] AppImage { get; set; }
        public string Filename { get; set; }
        public DateTime? ImageLastUpdatetime { get; set; }
        public DateTime? AppxJoinDatetime { get; set; }
        public decimal? JoinConfirm { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
