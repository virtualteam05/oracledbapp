﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrDependBillMonthdtl
    {
        public decimal? DependBillMonthdtlNo { get; set; }
        public decimal? DependBilldtlNo { get; set; }
        public decimal? SalarydtlNo { get; set; }
        public DateTime? SalstartDatetime { get; set; }
        public decimal? ElementNo { get; set; }
        public decimal? Amount { get; set; }
        public decimal? CompanyNo { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public DateTime? ProcessEndDatetime { get; set; }
    }
}
