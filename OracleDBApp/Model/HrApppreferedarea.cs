﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrApppreferedarea
    {
        public decimal? PreferedareaNo { get; set; }
        public decimal? AppNo { get; set; }
        public decimal? PreferenceSl { get; set; }
        public decimal? ApppreferedareaNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
