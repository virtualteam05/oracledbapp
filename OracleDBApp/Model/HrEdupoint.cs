﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrEdupoint
    {
        public decimal? SlNo { get; set; }
        public string ResultType { get; set; }
        public string Grade { get; set; }
        public decimal? CgpaFrom { get; set; }
        public decimal? CgpaTo { get; set; }
        public decimal? ResultPoint { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
