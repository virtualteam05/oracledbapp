﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrEmppayrolldtl
    {
        public decimal? EmppayrolldtlNo { get; set; }
        public decimal? EmppayrollNo { get; set; }
        public decimal? ElementNo { get; set; }
        public decimal? Amt { get; set; }
        public decimal? Per { get; set; }
        public string Peron { get; set; }
        public decimal? ProvisionMonth { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? Active { get; set; }
        public decimal? MaxAmt { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CashAmt { get; set; }
        public decimal? ChequeAmt { get; set; }
        public decimal? CashRatio { get; set; }
        public decimal? ChequeRatio { get; set; }
        public decimal? IsGross { get; set; }
        public decimal? JtpayrollNo { get; set; }
        public DateTime? EffectiveDatetime { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public decimal? FlagForArear { get; set; }
        public decimal? Percents { get; set; }
    }
}
