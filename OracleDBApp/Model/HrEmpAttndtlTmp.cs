﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrEmpAttndtlTmp
    {
        public DateTime? CalDt { get; set; }
        public string DdStatus { get; set; }
        public decimal? EmpNo { get; set; }
        public DateTime? SchDatetime { get; set; }
        public decimal? SchShift { get; set; }
        public DateTime? SchInTime { get; set; }
        public DateTime? SchOutTime { get; set; }
        public decimal? AtndNo { get; set; }
        public decimal? AtnShift { get; set; }
        public DateTime? InTime { get; set; }
        public DateTime? OutTime { get; set; }
        public decimal? LateStat { get; set; }
        public decimal? LateentryApproved { get; set; }
        public decimal? EarlyExitStat { get; set; }
        public decimal? EarlyexitApproved { get; set; }
        public string Status { get; set; }
        public string Prefix { get; set; }
        public decimal? BuNo { get; set; }
        public string BuName { get; set; }
        public string EmpNameId { get; set; }
        public decimal? JobtitleNo { get; set; }
        public string Jobtitle { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? SalReportSlNo { get; set; }
        public string HrType { get; set; }
        public string JobType { get; set; }
        public decimal? DepartmentNo { get; set; }
        public string Department { get; set; }
        public decimal? SectionNo { get; set; }
        public string Section { get; set; }
        public decimal? ActWorkHour { get; set; }
        public string ActWorkHourTfmt { get; set; }
        public decimal? MovHr { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
