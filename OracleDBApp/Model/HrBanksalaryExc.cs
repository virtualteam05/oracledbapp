﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrBanksalaryExc
    {
        public decimal? BanksalaryExcNo { get; set; }
        public string BanksalaryExcId { get; set; }
        public decimal? CompanyNo { get; set; }
        public decimal? BuNo { get; set; }
        public decimal? JobtitleNo { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? MaxBankAmt { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
