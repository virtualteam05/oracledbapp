﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrBilldtl
    {
        public decimal? BilldtlNo { get; set; }
        public decimal? BillNo { get; set; }
        public decimal? ElementNo { get; set; }
        public decimal? DedElementNo { get; set; }
        public DateTime? ExpenseDatetime { get; set; }
        public string Purpose { get; set; }
        public string Peron { get; set; }
        public decimal? Per { get; set; }
        public decimal? Amount { get; set; }
        public decimal? MaxAmt { get; set; }
        public decimal? DedAmount { get; set; }
        public decimal? AppAmount { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CostNo { get; set; }
        public decimal? DrAccNo { get; set; }
        public decimal? CheckedAmount { get; set; }
        public string Remarks { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
