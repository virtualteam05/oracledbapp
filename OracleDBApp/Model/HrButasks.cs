﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrButasks
    {
        public decimal? ButaskNo { get; set; }
        public string Butaskid { get; set; }
        public decimal? BuNo { get; set; }
        public decimal? TaskNo { get; set; }
        public string Descr { get; set; }
        public DateTime? EffectiveFromDatetime { get; set; }
        public DateTime? EffectiveToDatetime { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public decimal? TypeNo { get; set; }
        public decimal? NatureNo { get; set; }
        public decimal? LevelNo { get; set; }
    }
}
