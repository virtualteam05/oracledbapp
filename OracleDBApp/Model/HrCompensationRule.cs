﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrCompensationRule
    {
        public decimal? CompruleNo { get; set; }
        public decimal? ElementNo { get; set; }
        public string Peron { get; set; }
        public decimal? JobtitleNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public DateTime? RuleDatetime { get; set; }
        public decimal? MaxAmt { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? ProbationMonth { get; set; }
        public decimal? CashRatio { get; set; }
        public decimal? ChequeRatio { get; set; }
        public decimal? Per { get; set; }
        public decimal? MinPieceEarning { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public decimal? EffectType { get; set; }
        public string CompareWith { get; set; }
    }
}
