﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrEmpevakra
    {
        public decimal? EmpevakraNo { get; set; }
        public decimal? AssignforNo { get; set; }
        public decimal? SlNo { get; set; }
        public decimal? EvaFor { get; set; }
        public decimal? TaskNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public decimal? Weightage { get; set; }
    }
}
