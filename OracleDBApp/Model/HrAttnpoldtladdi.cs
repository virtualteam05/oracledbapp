﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrAttnpoldtladdi
    {
        public decimal? Attnpoldtladdi { get; set; }
        public decimal? AttnpoldtlNo { get; set; }
        public decimal? AttnpolNo { get; set; }
        public string Gender { get; set; }
        public decimal? ServiceMonthFrom { get; set; }
        public decimal? ServiceMonthTo { get; set; }
        public decimal? EarnAmt { get; set; }
        public decimal? EarnPer { get; set; }
        public string Peron { get; set; }
        public decimal? CashRatio { get; set; }
        public decimal? ChequeRatio { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? MaxAmt { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
