﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrConfig
    {
        public decimal? HrConfigNo { get; set; }
        public decimal? OgNo { get; set; }
        public decimal? TrDr { get; set; }
        public decimal? DrTr { get; set; }
        public decimal? SalaryHour { get; set; }
        public string SalReportTitle { get; set; }
        public decimal? Basic { get; set; }
        public decimal? HouseRent { get; set; }
        public decimal? MedAllow { get; set; }
        public decimal? OtRate { get; set; }
        public decimal? RoundAmt { get; set; }
        public decimal? Conv { get; set; }
        public string SsUserDisplayFmt { get; set; }
        public decimal? HrNofWeekend { get; set; }
        public decimal? HrWeekend1 { get; set; }
        public decimal? HrWeekend2 { get; set; }
        public decimal? HrWeekStartDay { get; set; }
        public decimal? HrSysGenEmpId { get; set; }
        public string HrSysGenEmpIdPfix { get; set; }
        public decimal? HrOfficeHour { get; set; }
        public decimal? HrLeaveHour { get; set; }
        public decimal? HrSysGenEmpIdBuPfix { get; set; }
        public string HrEmpIdNameStyle { get; set; }
        public decimal? HrEmpIdSuffixLen { get; set; }
        public string EmpPhotoReadBy { get; set; }
        public string SsPathEmpImg { get; set; }
        public string SsEmpImgType { get; set; }
        public string SsCandidatetimeImgFilePath { get; set; }
        public decimal? SsCandidatetimeImgMaxSizeKb { get; set; }
        public decimal? SsCandidatetimeImgExt { get; set; }
        public decimal? LeaveRound { get; set; }
        public string IdCardImagePath { get; set; }
        public string IdCardImagePathBack { get; set; }
        public decimal? BuDisplay { get; set; }
        public decimal? ServiceEndAge { get; set; }
        public decimal? MaxServiceYear { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? AttndCardEnterDelay { get; set; }
        public decimal? DefaultSalarySheet { get; set; }
        public decimal? ExcludeAttendanceInProcess { get; set; }
        public string SalaryElementShowBy { get; set; }
        public string SalaryJobtitleShowBy { get; set; }
        public decimal? CompanyNo { get; set; }
        public string BuTitleBy { get; set; }
        public decimal? EmpIdIsCharCheck { get; set; }
        public decimal? ProcessingDayStart { get; set; }
        public decimal? ProcessingDayEnd { get; set; }
        public decimal? IsFromPreviousMon { get; set; }
        public decimal? UseAttendanceProcess { get; set; }
        public decimal? GenInTimeFlag { get; set; }
        public decimal? PrvsIncrmntElement1 { get; set; }
        public decimal? PrvsIncrmntElement2 { get; set; }
        public string CardNoGenType { get; set; }
        public decimal? OtRatePerHrRound { get; set; }
        public decimal? NoOfDayLessProcessForOt { get; set; }
        public decimal? SalDivFacProcessStartDatetime { get; set; }
        public decimal? AutoIdYearCountFlag { get; set; }
        public decimal? OtForOnlyEmpPayrollFlg { get; set; }
        public decimal? FoodAllow { get; set; }
        public byte[] NoImage { get; set; }
        public byte[] ErrorImage { get; set; }
        public decimal? OtherAllow { get; set; }
        public string BasicSalaryFormat { get; set; }
        public decimal? ProvisionVtypeNo { get; set; }
        public decimal? PaymentVtypeNo { get; set; }
        public decimal? PieceRateSalary { get; set; }
        public decimal? LeaveApplicationToFlg { get; set; }
        public decimal? CompanyWiseEmpIdSeq { get; set; }
        public decimal? CompanyWiseIdLength { get; set; }
        public decimal? JobLocationFlg { get; set; }
        public decimal? MovementAsOutTime { get; set; }
        public decimal? PresentAtFutureDatetime { get; set; }
        public decimal? GroupFlag { get; set; }
        public decimal? DisplaySalaryPeriod { get; set; }
        public decimal? CalcCountHrAsOtSkip { get; set; }
        public decimal? ReportDescDisplayFlag { get; set; }
        public decimal? HidePayrollInEmpForm { get; set; }
        public decimal? HideTransferInEmpForm { get; set; }
        public decimal? HideIncrementInEmpForm { get; set; }
        public decimal? HideSalaryInEmpForm { get; set; }
        public decimal? AttnBonus { get; set; }
    }
}
