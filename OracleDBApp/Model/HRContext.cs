﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace OracleDBApp.Model
{
    public partial class HRContext : DbContext
    {
        public HRContext()
        {
        }

        public HRContext(DbContextOptions<HRContext> options)
            : base(options)
        {
        }

        public virtual DbSet<HrAbsentAlternetAttn> HrAbsentAlternetAttn { get; set; }
        public virtual DbSet<HrAdj> HrAdj { get; set; }
        public virtual DbSet<HrAdjdtl> HrAdjdtl { get; set; }
        public virtual DbSet<HrAppeduqual> HrAppeduqual { get; set; }
        public virtual DbSet<HrAppinfo> HrAppinfo { get; set; }
        public virtual DbSet<HrAppinterview> HrAppinterview { get; set; }
        public virtual DbSet<HrAppinterviewExt> HrAppinterviewExt { get; set; }
        public virtual DbSet<HrApplang> HrApplang { get; set; }
        public virtual DbSet<HrApppreferedarea> HrApppreferedarea { get; set; }
        public virtual DbSet<HrAppresult> HrAppresult { get; set; }
        public virtual DbSet<HrAppskill> HrAppskill { get; set; }
        public virtual DbSet<HrAppuserdefineinfo> HrAppuserdefineinfo { get; set; }
        public virtual DbSet<HrArearMv> HrArearMv { get; set; }
        public virtual DbSet<HrAttendance> HrAttendance { get; set; }
        public virtual DbSet<HrAttendance04112018> HrAttendance04112018 { get; set; }
        public virtual DbSet<HrAttendanceBack> HrAttendanceBack { get; set; }
        public virtual DbSet<HrAttendanceBk> HrAttendanceBk { get; set; }
        public virtual DbSet<HrAttendanceLog> HrAttendanceLog { get; set; }
        public virtual DbSet<HrAttendanceRep> HrAttendanceRep { get; set; }
        public virtual DbSet<HrAttendanceTmp> HrAttendanceTmp { get; set; }
        public virtual DbSet<HrAttendanceWhouse> HrAttendanceWhouse { get; set; }
        public virtual DbSet<HrAttnAccesslevel> HrAttnAccesslevel { get; set; }
        public virtual DbSet<HrAttnCorrectionTemp> HrAttnCorrectionTemp { get; set; }
        public virtual DbSet<HrAttnDynamicVTemp> HrAttnDynamicVTemp { get; set; }
        public virtual DbSet<HrAttnProcess> HrAttnProcess { get; set; }
        public virtual DbSet<HrAttnProcess27102017> HrAttnProcess27102017 { get; set; }
        public virtual DbSet<HrAttnProcessTo300916> HrAttnProcessTo300916 { get; set; }
        public virtual DbSet<HrAttnbonusadj> HrAttnbonusadj { get; set; }
        public virtual DbSet<HrAttnbonusdtl> HrAttnbonusdtl { get; set; }
        public virtual DbSet<HrAttnopbal> HrAttnopbal { get; set; }
        public virtual DbSet<HrAttnpol> HrAttnpol { get; set; }
        public virtual DbSet<HrAttnpoldtl> HrAttnpoldtl { get; set; }
        public virtual DbSet<HrAttnpoldtladdi> HrAttnpoldtladdi { get; set; }
        public virtual DbSet<HrBankinfo> HrBankinfo { get; set; }
        public virtual DbSet<HrBanksalaryExc> HrBanksalaryExc { get; set; }
        public virtual DbSet<HrBill> HrBill { get; set; }
        public virtual DbSet<HrBillEmpAcc> HrBillEmpAcc { get; set; }
        public virtual DbSet<HrBillProvEmpAcc> HrBillProvEmpAcc { get; set; }
        public virtual DbSet<HrBilldtl> HrBilldtl { get; set; }
        public virtual DbSet<HrBu> HrBu { get; set; }
        public virtual DbSet<HrBuGroup> HrBuGroup { get; set; }
        public virtual DbSet<HrBudtl> HrBudtl { get; set; }
        public virtual DbSet<HrBugoal> HrBugoal { get; set; }
        public virtual DbSet<HrBugoaldtl> HrBugoaldtl { get; set; }
        public virtual DbSet<HrButasks> HrButasks { get; set; }
        public virtual DbSet<HrCarddata> HrCarddata { get; set; }
        public virtual DbSet<HrCarddataExt> HrCarddataExt { get; set; }
        public virtual DbSet<HrCarddatasize> HrCarddatasize { get; set; }
        public virtual DbSet<HrCompensationCalcElement> HrCompensationCalcElement { get; set; }
        public virtual DbSet<HrCompensationRule> HrCompensationRule { get; set; }
        public virtual DbSet<HrConfig> HrConfig { get; set; }
        public virtual DbSet<HrDaypol> HrDaypol { get; set; }
        public virtual DbSet<HrDaypolMaster> HrDaypolMaster { get; set; }
        public virtual DbSet<HrDaypoldtl> HrDaypoldtl { get; set; }
        public virtual DbSet<HrDependBill> HrDependBill { get; set; }
        public virtual DbSet<HrDependBillMonthdtl> HrDependBillMonthdtl { get; set; }
        public virtual DbSet<HrDependBilldtl> HrDependBilldtl { get; set; }
        public virtual DbSet<HrDependElementpol> HrDependElementpol { get; set; }
        public virtual DbSet<HrDurationpol> HrDurationpol { get; set; }
        public virtual DbSet<HrDurationpolMaster> HrDurationpolMaster { get; set; }
        public virtual DbSet<HrDurationpoldtl> HrDurationpoldtl { get; set; }
        public virtual DbSet<HrDynamicRep> HrDynamicRep { get; set; }
        public virtual DbSet<HrDynamicRepCol> HrDynamicRepCol { get; set; }
        public virtual DbSet<HrEditorSource> HrEditorSource { get; set; }
        public virtual DbSet<HrEditorTemplate> HrEditorTemplate { get; set; }
        public virtual DbSet<HrEduarea> HrEduarea { get; set; }
        public virtual DbSet<HrEduboard> HrEduboard { get; set; }
        public virtual DbSet<HrEduinstitute> HrEduinstitute { get; set; }
        public virtual DbSet<HrEdulevel> HrEdulevel { get; set; }
        public virtual DbSet<HrEdumajor> HrEdumajor { get; set; }
        public virtual DbSet<HrEdupoint> HrEdupoint { get; set; }
        public virtual DbSet<HrElement> HrElement { get; set; }
        public virtual DbSet<HrElementReportGroup> HrElementReportGroup { get; set; }
        public virtual DbSet<HrEmp> HrEmp { get; set; }
        public virtual DbSet<HrEmpAdvanceSearch> HrEmpAdvanceSearch { get; set; }
        public virtual DbSet<HrEmpAlternateTaxsal> HrEmpAlternateTaxsal { get; set; }
        public virtual DbSet<HrEmpAttndtlTab> HrEmpAttndtlTab { get; set; }
        public virtual DbSet<HrEmpAttndtlTmp> HrEmpAttndtlTmp { get; set; }
        public virtual DbSet<HrEmpAttndtlTmpRep> HrEmpAttndtlTmpRep { get; set; }
        public virtual DbSet<HrEmpAttndtlVTmp> HrEmpAttndtlVTmp { get; set; }
        public virtual DbSet<HrEmpAutoinactive> HrEmpAutoinactive { get; set; }
        public virtual DbSet<HrEmpDoc> HrEmpDoc { get; set; }
        public virtual DbSet<HrEmpDoctype> HrEmpDoctype { get; set; }
        public virtual DbSet<HrEmpJd> HrEmpJd { get; set; }
        public virtual DbSet<HrEmpJdDtl> HrEmpJdDtl { get; set; }
        public virtual DbSet<HrEmpLog> HrEmpLog { get; set; }
        public virtual DbSet<HrEmpSalArear> HrEmpSalArear { get; set; }
        public virtual DbSet<HrEmpSalAreardtl> HrEmpSalAreardtl { get; set; }
        public virtual DbSet<HrEmpSearchEdumajor> HrEmpSearchEdumajor { get; set; }
        public virtual DbSet<HrEmpSearchTemp> HrEmpSearchTemp { get; set; }
        public virtual DbSet<HrEmpcardreg> HrEmpcardreg { get; set; }
        public virtual DbSet<HrEmpevakra> HrEmpevakra { get; set; }
        public virtual DbSet<HrEmpfamaly> HrEmpfamaly { get; set; }
        public virtual DbSet<HrEmpfixedelement> HrEmpfixedelement { get; set; }
        public virtual DbSet<HrEmpinfo> HrEmpinfo { get; set; }
        public virtual DbSet<HrEmpjdreports> HrEmpjdreports { get; set; }
        public virtual DbSet<HrEmpjdtaskreports> HrEmpjdtaskreports { get; set; }
        public virtual DbSet<HrEmplastschedule> HrEmplastschedule { get; set; }
        public virtual DbSet<HrEmpp> HrEmpp { get; set; }
        public virtual DbSet<HrEmppayroll> HrEmppayroll { get; set; }
        public virtual DbSet<HrEmppayrolldtl> HrEmppayrolldtl { get; set; }
        public virtual DbSet<HrEmppfrule> HrEmppfrule { get; set; }
        public virtual DbSet<HrEmprosterhistory> HrEmprosterhistory { get; set; }
        public virtual DbSet<HrEmptraining> HrEmptraining { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=192.168.1.100;Database=HR;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<HrAbsentAlternetAttn>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ABSENT_ALTERNET_ATTN");

                entity.Property(e => e.AfterGraceIntime)
                    .HasColumnName("AFTER_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnDatetime)
                    .HasColumnName("ATTN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.BeforeGraceIntime)
                    .HasColumnName("BEFORE_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DayStat)
                    .HasColumnName("DAY_STAT")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EarlyexitReason)
                    .HasColumnName("EARLYEXIT_REASON")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ElAvail)
                    .HasColumnName("EL_AVAIL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.HrData)
                    .HasColumnName("HR_DATA")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataAmt)
                    .HasColumnName("HR_DATA_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataPhr)
                    .HasColumnName("HR_DATA_PHR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryReason)
                    .HasColumnName("LATEENTRY_REASON")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtAmt)
                    .HasColumnName("OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtRatePerHr)
                    .HasColumnName("OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidDatetime)
                    .HasColumnName("PAID_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidFlg)
                    .HasColumnName("PAID_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.RemarksManualAtten)
                    .HasColumnName("REMARKS_MANUAL_ATTEN")
                    .HasMaxLength(500);

                entity.Property(e => e.ShiftInTime)
                    .HasColumnName("SHIFT_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ShiftNo)
                    .HasColumnName("SHIFT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ShiftOutTime)
                    .HasColumnName("SHIFT_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCheckedOn)
                    .HasColumnName("SS_CHECKED_ON")
                    .HasMaxLength(200);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAdj>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ADJ");

                entity.Property(e => e.AdjDatetime)
                    .HasColumnName("ADJ_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.AdjNo)
                    .HasColumnName("ADJ_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.TranId)
                    .HasColumnName("TRAN_ID")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<HrAdjdtl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ADJDTL");

                entity.Property(e => e.AdjAmt)
                    .HasColumnName("ADJ_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.AdjNo)
                    .HasColumnName("ADJ_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AdjdtlNo)
                    .HasColumnName("ADJDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(500);

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalaryNo)
                    .HasColumnName("SALARY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAppeduqual>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_APPEDUQUAL");

                entity.Property(e => e.AppNo)
                    .HasColumnName("APP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AppeduqualNo)
                    .HasColumnName("APPEDUQUAL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Cgpa)
                    .HasColumnName("CGPA")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EduareaNo)
                    .HasColumnName("EDUAREA_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EduboardNo)
                    .HasColumnName("EDUBOARD_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EdulevelNo)
                    .HasColumnName("EDULEVEL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EdumajorNo)
                    .HasColumnName("EDUMAJOR_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Grade)
                    .HasColumnName("GRADE")
                    .HasMaxLength(10);

                entity.Property(e => e.InstNo)
                    .HasColumnName("INST_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.LastQualFlag)
                    .HasColumnName("LAST_QUAL_FLAG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.OutOf)
                    .HasColumnName("OUT_OF")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.Remarks)
                    .HasColumnName("REMARKS")
                    .HasMaxLength(500);

                entity.Property(e => e.ResultType)
                    .HasColumnName("RESULT_TYPE")
                    .HasMaxLength(1);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.YearOfPass)
                    .HasColumnName("YEAR_OF_PASS")
                    .HasColumnType("decimal(4, 0)");
            });

            modelBuilder.Entity<HrAppinfo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_APPINFO");

                entity.Property(e => e.AppBefore)
                    .HasColumnName("APP_BEFORE")
                    .HasMaxLength(1);

                entity.Property(e => e.AppDatetime)
                    .HasColumnName("APP_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.AppImage)
                    .HasColumnName("APP_IMAGE")
                    .HasColumnType("image");

                entity.Property(e => e.AppNo)
                    .HasColumnName("APP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AppReason)
                    .HasColumnName("APP_REASON")
                    .HasMaxLength(200);

                entity.Property(e => e.AppointmentLetterNo)
                    .HasColumnName("APPOINTMENT_LETTER_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AppxJoinDatetime)
                    .HasColumnName("APPX_JOIN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.AvailableFor)
                    .HasColumnName("AVAILABLE_FOR")
                    .HasMaxLength(1);

                entity.Property(e => e.BankNo)
                    .HasColumnName("BANK_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CareerObjective)
                    .HasColumnName("CAREER_OBJECTIVE")
                    .HasMaxLength(500);

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CvAttchFileName)
                    .HasColumnName("CV_ATTCH_FILE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.CvAttchFileNo)
                    .HasColumnName("CV_ATTCH_FILE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.DrivinglcNo)
                    .HasColumnName("DRIVINGLC_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.EmailOfficial)
                    .HasColumnName("EMAIL_OFFICIAL")
                    .HasMaxLength(100);

                entity.Property(e => e.EmailPersonal)
                    .HasColumnName("EMAIL_PERSONAL")
                    .HasMaxLength(100);

                entity.Property(e => e.ExpectedSalary)
                    .HasColumnName("EXPECTED_SALARY")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ExtraCurAtc)
                    .HasColumnName("EXTRA_CUR_ATC")
                    .HasMaxLength(200);

                entity.Property(e => e.FatherName)
                    .HasColumnName("FATHER_NAME")
                    .HasMaxLength(90);

                entity.Property(e => e.FathersOccupation)
                    .HasColumnName("FATHERS_OCCUPATION")
                    .HasMaxLength(50);

                entity.Property(e => e.FaxNo)
                    .HasColumnName("FAX_NO")
                    .HasMaxLength(30);

                entity.Property(e => e.FileExt)
                    .HasColumnName("FILE_EXT")
                    .HasMaxLength(10);

                entity.Property(e => e.FileSize)
                    .HasColumnName("FILE_SIZE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Filename)
                    .HasColumnName("FILENAME")
                    .HasMaxLength(400);

                entity.Property(e => e.Fname)
                    .HasColumnName("FNAME")
                    .HasMaxLength(50);

                entity.Property(e => e.Gender)
                    .HasColumnName("GENDER")
                    .HasMaxLength(1);

                entity.Property(e => e.HusbandName)
                    .HasColumnName("HUSBAND_NAME")
                    .HasMaxLength(90);

                entity.Property(e => e.HusbandOccupaton)
                    .HasColumnName("HUSBAND_OCCUPATON")
                    .HasMaxLength(50);

                entity.Property(e => e.ImageLastUpdatetime)
                    .HasColumnName("IMAGE_LAST_UPdatetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ImgExt)
                    .HasColumnName("IMG_EXT")
                    .HasMaxLength(5);

                entity.Property(e => e.ImgNo)
                    .HasColumnName("IMG_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.Interest)
                    .HasColumnName("INTEREST")
                    .HasMaxLength(200);

                entity.Property(e => e.JoinConfirm)
                    .HasColumnName("JOIN_CONFIRM")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.JoinTime)
                    .HasColumnName("JOIN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.Lname)
                    .HasColumnName("LNAME")
                    .HasMaxLength(20);

                entity.Property(e => e.MStatus)
                    .HasColumnName("M_STATUS")
                    .HasMaxLength(1);

                entity.Property(e => e.Mimetype)
                    .HasColumnName("MIMETYPE")
                    .HasMaxLength(200);

                entity.Property(e => e.MotherName)
                    .HasColumnName("MOTHER_NAME")
                    .HasMaxLength(90);

                entity.Property(e => e.MothersOccupation)
                    .HasColumnName("MOTHERS_OCCUPATION")
                    .HasMaxLength(50);

                entity.Property(e => e.Nationality)
                    .HasColumnName("NATIONALITY")
                    .HasMaxLength(50);

                entity.Property(e => e.NoticedtlNo)
                    .HasColumnName("NOTICEDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PassportNo)
                    .HasColumnName("PASSPORT_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.PayorderDatetime)
                    .HasColumnName("PAYORDER_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PayorderNo)
                    .HasColumnName("PAYORDER_NO")
                    .HasMaxLength(200);

                entity.Property(e => e.PeAddr1)
                    .HasColumnName("PE_ADDR1")
                    .HasMaxLength(100);

                entity.Property(e => e.PeAddr2)
                    .HasColumnName("PE_ADDR2")
                    .HasMaxLength(50);

                entity.Property(e => e.PeAddrCountry)
                    .HasColumnName("PE_ADDR_COUNTRY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PeAddrDist)
                    .HasColumnName("PE_ADDR_DIST")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PeAddrPost)
                    .HasColumnName("PE_ADDR_POST")
                    .HasMaxLength(20);

                entity.Property(e => e.PhoneHome)
                    .HasColumnName("PHONE_HOME")
                    .HasMaxLength(30);

                entity.Property(e => e.PhoneMobile)
                    .HasColumnName("PHONE_MOBILE")
                    .HasMaxLength(30);

                entity.Property(e => e.PhoneOffice)
                    .HasColumnName("PHONE_OFFICE")
                    .HasMaxLength(30);

                entity.Property(e => e.PrAddr1)
                    .HasColumnName("PR_ADDR1")
                    .HasMaxLength(100);

                entity.Property(e => e.PrAddr2)
                    .HasColumnName("PR_ADDR2")
                    .HasMaxLength(50);

                entity.Property(e => e.PrAddrCountry)
                    .HasColumnName("PR_ADDR_COUNTRY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PrAddrDist)
                    .HasColumnName("PR_ADDR_DIST")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PrAddrPost)
                    .HasColumnName("PR_ADDR_POST")
                    .HasMaxLength(20);

                entity.Property(e => e.PreferedAddr)
                    .HasColumnName("PREFERED_ADDR")
                    .HasMaxLength(2);

                entity.Property(e => e.PreferedLvl)
                    .HasColumnName("PREFERED_LVL")
                    .HasMaxLength(1);

                entity.Property(e => e.PreferedPhone)
                    .HasColumnName("PREFERED_PHONE")
                    .HasMaxLength(1);

                entity.Property(e => e.PresentSalary)
                    .HasColumnName("PRESENT_SALARY")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ProMembership)
                    .HasColumnName("PRO_MEMBERSHIP")
                    .HasMaxLength(200);

                entity.Property(e => e.Religion)
                    .HasColumnName("RELIGION")
                    .HasMaxLength(25);

                entity.Property(e => e.RollNo)
                    .HasColumnName("ROLL_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.Salutation)
                    .HasColumnName("SALUTATION")
                    .HasMaxLength(20);

                entity.Property(e => e.Selected)
                    .HasColumnName("SELECTED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SerialNo)
                    .HasColumnName("SERIAL_NO")
                    .HasMaxLength(200);

                entity.Property(e => e.ShortListed)
                    .HasColumnName("SHORT_LISTED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SlNo)
                    .HasColumnName("SL_NO")
                    .HasColumnType("decimal(15, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsNo)
                    .HasColumnName("SS_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.TinNo)
                    .HasColumnName("TIN_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.TrackNo)
                    .HasColumnName("TRACK_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.YearOfExp)
                    .HasColumnName("YEAR_OF_EXP")
                    .HasColumnType("decimal(4, 2)");
            });

            modelBuilder.Entity<HrAppinterview>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_APPINTERVIEW");

                entity.Property(e => e.AppNo)
                    .HasColumnName("APP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AppinterviewNo)
                    .HasColumnName("APPINTERVIEW_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Comments)
                    .HasColumnName("COMMENTS")
                    .HasMaxLength(250);

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.GetMark)
                    .HasColumnName("GET_MARK")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.InterviewLetterNo)
                    .HasColumnName("INTERVIEW_LETTER_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.InterviewNo)
                    .HasColumnName("INTERVIEW_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.NextLevel)
                    .HasColumnName("NEXT_LEVEL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RollNo)
                    .HasColumnName("ROLL_NO")
                    .HasMaxLength(100);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAppinterviewExt>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_APPINTERVIEW_EXT");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.GetMark)
                    .HasColumnName("GET_MARK")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RollNo)
                    .HasColumnName("ROLL_NO")
                    .HasMaxLength(100);

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrApplang>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_APPLANG");

                entity.Property(e => e.AppNo)
                    .HasColumnName("APP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ApplangNo)
                    .HasColumnName("APPLANG_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.LangName)
                    .HasColumnName("LANG_NAME")
                    .HasMaxLength(50);

                entity.Property(e => e.Reading)
                    .HasColumnName("READING")
                    .HasMaxLength(1);

                entity.Property(e => e.Speaking)
                    .HasColumnName("SPEAKING")
                    .HasMaxLength(1);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.Writing)
                    .HasColumnName("WRITING")
                    .HasMaxLength(1);
            });

            modelBuilder.Entity<HrApppreferedarea>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_APPPREFEREDAREA");

                entity.Property(e => e.AppNo)
                    .HasColumnName("APP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ApppreferedareaNo)
                    .HasColumnName("APPPREFEREDAREA_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PreferedareaNo)
                    .HasColumnName("PREFEREDAREA_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PreferenceSl)
                    .HasColumnName("PREFERENCE_SL")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAppresult>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_APPRESULT");

                entity.Property(e => e.AppNo)
                    .HasColumnName("APP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ResultRank)
                    .HasColumnName("RESULT_RANK")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RollNo)
                    .HasColumnName("ROLL_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.Secton1)
                    .HasColumnName("SECTON1")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Secton2)
                    .HasColumnName("SECTON2")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Secton3)
                    .HasColumnName("SECTON3")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Secton4)
                    .HasColumnName("SECTON4")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Secton5)
                    .HasColumnName("SECTON5")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Selected)
                    .HasColumnName("SELECTED")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAppskill>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_APPSKILL");

                entity.Property(e => e.AppNo)
                    .HasColumnName("APP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AppskillNo)
                    .HasColumnName("APPSKILL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(200);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ExpYears)
                    .HasColumnName("EXP_YEARS")
                    .HasMaxLength(50);

                entity.Property(e => e.LastUsed)
                    .HasColumnName("LAST_USED")
                    .HasMaxLength(50);

                entity.Property(e => e.SkillLevel)
                    .HasColumnName("SKILL_LEVEL")
                    .HasMaxLength(25);

                entity.Property(e => e.SkillName)
                    .HasColumnName("SKILL_NAME")
                    .HasMaxLength(150);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAppuserdefineinfo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_APPUSERDEFINEINFO");

                entity.Property(e => e.AppNo)
                    .HasColumnName("APP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AppuserdefineinfoNo)
                    .HasColumnName("APPUSERDEFINEINFO_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(1000);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.UserLbl)
                    .HasColumnName("USER_LBL")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<HrArearMv>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_AREAR_MV");

                entity.Property(e => e.ActAmount)
                    .HasColumnName("ACT_AMOUNT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Amount)
                    .HasColumnName("AMOUNT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ArrAmount)
                    .HasColumnName("ARR_AMOUNT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ArrElementName)
                    .HasColumnName("ARR_ELEMENT_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ArrElementNo)
                    .HasColumnName("ARR_ELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalAmount)
                    .HasColumnName("SAL_AMOUNT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrAttendance>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTENDANCE");

                entity.Property(e => e.AfterGraceIntime)
                    .HasColumnName("AFTER_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnDatetime)
                    .HasColumnName("ATTN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.BeforeGraceIntime)
                    .HasColumnName("BEFORE_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DayStat)
                    .HasColumnName("DAY_STAT")
                    .HasMaxLength(10);

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EarlyexitReason)
                    .HasColumnName("EARLYEXIT_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.ElAvail)
                    .HasColumnName("EL_AVAIL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.HrData)
                    .HasColumnName("HR_DATA")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataAmt)
                    .HasColumnName("HR_DATA_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataPhr)
                    .HasColumnName("HR_DATA_PHR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryReason)
                    .HasColumnName("LATEENTRY_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtAmt)
                    .HasColumnName("OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtRatePerHr)
                    .HasColumnName("OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidDatetime)
                    .HasColumnName("PAID_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidFlg)
                    .HasColumnName("PAID_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.RemarksManualAtten)
                    .HasColumnName("REMARKS_MANUAL_ATTEN")
                    .HasMaxLength(500);

                entity.Property(e => e.ShiftInTime)
                    .HasColumnName("SHIFT_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ShiftNo)
                    .HasColumnName("SHIFT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ShiftOutTime)
                    .HasColumnName("SHIFT_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCheckedOn)
                    .HasColumnName("SS_CHECKED_ON")
                    .HasMaxLength(200);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAttendance04112018>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTENDANCE_04112018");

                entity.Property(e => e.AfterGraceIntime)
                    .HasColumnName("AFTER_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnDatetime)
                    .HasColumnName("ATTN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.BeforeGraceIntime)
                    .HasColumnName("BEFORE_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DayStat)
                    .HasColumnName("DAY_STAT")
                    .HasMaxLength(10);

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EarlyexitReason)
                    .HasColumnName("EARLYEXIT_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.ElAvail)
                    .HasColumnName("EL_AVAIL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.HrData)
                    .HasColumnName("HR_DATA")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataAmt)
                    .HasColumnName("HR_DATA_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataPhr)
                    .HasColumnName("HR_DATA_PHR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryReason)
                    .HasColumnName("LATEENTRY_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtAmt)
                    .HasColumnName("OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtRatePerHr)
                    .HasColumnName("OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidDatetime)
                    .HasColumnName("PAID_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidFlg)
                    .HasColumnName("PAID_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.RemarksManualAtten)
                    .HasColumnName("REMARKS_MANUAL_ATTEN")
                    .HasMaxLength(500);

                entity.Property(e => e.ShiftInTime)
                    .HasColumnName("SHIFT_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ShiftNo)
                    .HasColumnName("SHIFT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ShiftOutTime)
                    .HasColumnName("SHIFT_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCheckedOn)
                    .HasColumnName("SS_CHECKED_ON")
                    .HasMaxLength(200);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAttendanceBack>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTENDANCE_BACK");

                entity.Property(e => e.AfterGraceIntime)
                    .HasColumnName("AFTER_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnDatetime)
                    .HasColumnName("ATTN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.BeforeGraceIntime)
                    .HasColumnName("BEFORE_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DayStat)
                    .HasColumnName("DAY_STAT")
                    .HasMaxLength(10);

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EarlyexitReason)
                    .HasColumnName("EARLYEXIT_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.ElAvail)
                    .HasColumnName("EL_AVAIL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.HrData)
                    .HasColumnName("HR_DATA")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataAmt)
                    .HasColumnName("HR_DATA_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataPhr)
                    .HasColumnName("HR_DATA_PHR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryReason)
                    .HasColumnName("LATEENTRY_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtAmt)
                    .HasColumnName("OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtRatePerHr)
                    .HasColumnName("OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidDatetime)
                    .HasColumnName("PAID_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidFlg)
                    .HasColumnName("PAID_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.RemarksManualAtten)
                    .HasColumnName("REMARKS_MANUAL_ATTEN")
                    .HasMaxLength(500);

                entity.Property(e => e.ShiftInTime)
                    .HasColumnName("SHIFT_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ShiftNo)
                    .HasColumnName("SHIFT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ShiftOutTime)
                    .HasColumnName("SHIFT_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCheckedOn)
                    .HasColumnName("SS_CHECKED_ON")
                    .HasMaxLength(200);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAttendanceBk>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTENDANCE_BK");

                entity.Property(e => e.AfterGraceIntime)
                    .HasColumnName("AFTER_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnDatetime)
                    .HasColumnName("ATTN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.BeforeGraceIntime)
                    .HasColumnName("BEFORE_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DayStat)
                    .HasColumnName("DAY_STAT")
                    .HasMaxLength(10);

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EarlyexitReason)
                    .HasColumnName("EARLYEXIT_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.ElAvail)
                    .HasColumnName("EL_AVAIL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.HrData)
                    .HasColumnName("HR_DATA")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataAmt)
                    .HasColumnName("HR_DATA_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataPhr)
                    .HasColumnName("HR_DATA_PHR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryReason)
                    .HasColumnName("LATEENTRY_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtAmt)
                    .HasColumnName("OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtRatePerHr)
                    .HasColumnName("OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidDatetime)
                    .HasColumnName("PAID_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidFlg)
                    .HasColumnName("PAID_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.RemarksManualAtten)
                    .HasColumnName("REMARKS_MANUAL_ATTEN")
                    .HasMaxLength(500);

                entity.Property(e => e.ShiftInTime)
                    .HasColumnName("SHIFT_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ShiftNo)
                    .HasColumnName("SHIFT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ShiftOutTime)
                    .HasColumnName("SHIFT_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCheckedOn)
                    .HasColumnName("SS_CHECKED_ON")
                    .HasMaxLength(200);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAttendanceLog>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTENDANCE_LOG");

                entity.Property(e => e.AfterGraceIntime)
                    .HasColumnName("AFTER_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnDatetime)
                    .HasColumnName("ATTN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.BeforeGraceIntime)
                    .HasColumnName("BEFORE_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DayStat)
                    .HasColumnName("DAY_STAT")
                    .HasMaxLength(10);

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EarlyexitReason)
                    .HasColumnName("EARLYEXIT_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.ElAvail)
                    .HasColumnName("EL_AVAIL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.HrData)
                    .HasColumnName("HR_DATA")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataAmt)
                    .HasColumnName("HR_DATA_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataPhr)
                    .HasColumnName("HR_DATA_PHR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryReason)
                    .HasColumnName("LATEENTRY_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.LogTime)
                    .HasColumnName("LOG_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtAmt)
                    .HasColumnName("OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtRatePerHr)
                    .HasColumnName("OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidDatetime)
                    .HasColumnName("PAID_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidFlg)
                    .HasColumnName("PAID_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.RemarksManualAtten)
                    .HasColumnName("REMARKS_MANUAL_ATTEN")
                    .HasMaxLength(500);

                entity.Property(e => e.ShiftInTime)
                    .HasColumnName("SHIFT_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ShiftNo)
                    .HasColumnName("SHIFT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ShiftOutTime)
                    .HasColumnName("SHIFT_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCheckedOn)
                    .HasColumnName("SS_CHECKED_ON")
                    .HasMaxLength(200);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAttendanceRep>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTENDANCE_REP");

                entity.Property(e => e.AttnDatetime)
                    .HasColumnName("ATTN_datetime")
                    .HasMaxLength(200);

                entity.Property(e => e.BuName)
                    .HasColumnName("BU_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Duration)
                    .HasColumnName("DURATION")
                    .HasMaxLength(30);

                entity.Property(e => e.EmpNameId)
                    .HasColumnName("EMP_NAME_ID")
                    .HasMaxLength(100);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasMaxLength(30);

                entity.Property(e => e.Jobtitle)
                    .HasColumnName("JOBTITLE")
                    .HasMaxLength(200);

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasMaxLength(30);

                entity.Property(e => e.Shift)
                    .HasColumnName("SHIFT")
                    .HasMaxLength(100);

                entity.Property(e => e.ShiftInTime)
                    .HasColumnName("SHIFT_IN_TIME")
                    .HasMaxLength(30);

                entity.Property(e => e.ShiftOutTime)
                    .HasColumnName("SHIFT_OUT_TIME")
                    .HasMaxLength(30);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAttendanceTmp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTENDANCE_TMP");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.AttndDatetime)
                    .HasColumnName("ATTND_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.GenOutTime)
                    .HasColumnName("GEN_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ShiftOutTime)
                    .HasColumnName("SHIFT_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAttendanceWhouse>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTENDANCE_WHOUSE");

                entity.Property(e => e.AfterGraceIntime)
                    .HasColumnName("AFTER_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnDatetime)
                    .HasColumnName("ATTN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.BeforeGraceIntime)
                    .HasColumnName("BEFORE_GRACE_INTIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DayStat)
                    .HasColumnName("DAY_STAT")
                    .HasMaxLength(10);

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EarlyexitReason)
                    .HasColumnName("EARLYEXIT_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.HrData)
                    .HasColumnName("HR_DATA")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataAmt)
                    .HasColumnName("HR_DATA_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrDataPhr)
                    .HasColumnName("HR_DATA_PHR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryReason)
                    .HasColumnName("LATEENTRY_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtAmt)
                    .HasColumnName("OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtRatePerHr)
                    .HasColumnName("OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidDatetime)
                    .HasColumnName("PAID_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidFlg)
                    .HasColumnName("PAID_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.ShiftInTime)
                    .HasColumnName("SHIFT_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.ShiftNo)
                    .HasColumnName("SHIFT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ShiftOutTime)
                    .HasColumnName("SHIFT_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCheckedOn)
                    .HasColumnName("SS_CHECKED_ON")
                    .HasMaxLength(200);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAttnAccesslevel>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTN_ACCESSLEVEL");

                entity.Property(e => e.AccesslevelName)
                    .HasColumnName("ACCESSLEVEL_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.AccesslevelNo)
                    .HasColumnName("ACCESSLEVEL_NO")
                    .HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<HrAttnCorrectionTemp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTN_CORRECTION_TEMP");

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BuName)
                    .HasColumnName("BU_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CalDt)
                    .HasColumnName("CAL_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmpId)
                    .HasColumnName("EMP_ID")
                    .HasMaxLength(80);

                entity.Property(e => e.EmpNameId)
                    .HasColumnName("EMP_NAME_ID")
                    .HasMaxLength(175);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.Jobtitle)
                    .HasColumnName("JOBTITLE")
                    .HasMaxLength(50);

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchDatetime)
                    .HasColumnName("SCH_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchInTime)
                    .HasColumnName("SCH_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchOutTime)
                    .HasColumnName("SCH_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchShift)
                    .HasColumnName("SCH_SHIFT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasMaxLength(53);
            });

            modelBuilder.Entity<HrAttnDynamicVTemp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTN_DYNAMIC_V_TEMP");

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtnShift)
                    .HasColumnName("ATN_SHIFT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnSchedulePol)
                    .HasColumnName("ATTN_SCHEDULE_POL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Basic)
                    .HasColumnName("BASIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BreakTimeEnd)
                    .HasColumnName("BREAK_TIME_END")
                    .HasColumnType("datetime");

                entity.Property(e => e.BreakTimeEnd2)
                    .HasColumnName("BREAK_TIME_END2")
                    .HasColumnType("datetime");

                entity.Property(e => e.BreakTimeStart)
                    .HasColumnName("BREAK_TIME_START")
                    .HasColumnType("datetime");

                entity.Property(e => e.BreakTimeStart2)
                    .HasColumnName("BREAK_TIME_START2")
                    .HasColumnType("datetime");

                entity.Property(e => e.BuName)
                    .HasColumnName("BU_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNameParent)
                    .HasColumnName("BU_NAME_PARENT")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BuNoAll)
                    .HasColumnName("BU_NO_ALL")
                    .HasMaxLength(200);

                entity.Property(e => e.CalDt)
                    .HasColumnName("CAL_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CalcCountHr)
                    .HasColumnName("CALC_COUNT_HR")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DdStatus)
                    .HasColumnName("DD_STATUS")
                    .HasMaxLength(1);

                entity.Property(e => e.DdStatusAlternet)
                    .HasColumnName("DD_STATUS_ALTERNET")
                    .HasMaxLength(1);

                entity.Property(e => e.DeductMin)
                    .HasColumnName("DEDUCT_MIN")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Department)
                    .HasColumnName("DEPARTMENT")
                    .HasMaxLength(100);

                entity.Property(e => e.DepartmentNo)
                    .HasColumnName("DEPARTMENT_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyExitStat)
                    .HasColumnName("EARLY_EXIT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyOt)
                    .HasColumnName("EARLY_OT")
                    .HasMaxLength(4000);

                entity.Property(e => e.EarlyOtStartTime)
                    .HasColumnName("EARLY_OT_START_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EarlyexitReason)
                    .HasColumnName("EARLYEXIT_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.EmpId)
                    .HasColumnName("EMP_ID")
                    .HasMaxLength(80);

                entity.Property(e => e.EmpName)
                    .HasColumnName("EMP_NAME")
                    .HasMaxLength(92);

                entity.Property(e => e.EmpNameId)
                    .HasColumnName("EMP_NAME_ID")
                    .HasMaxLength(175);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpType)
                    .HasColumnName("EMP_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpTypeName)
                    .HasColumnName("EMP_TYPE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ExtraOtMultiply)
                    .HasColumnName("EXTRA_OT_MULTIPLY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.GenInTime)
                    .HasColumnName("GEN_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenInTimeTfmt)
                    .HasColumnName("GEN_IN_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.GenOutTime)
                    .HasColumnName("GEN_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenOutTimeTfmt)
                    .HasColumnName("GEN_OUT_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.Gender)
                    .HasColumnName("GENDER")
                    .HasMaxLength(1);

                entity.Property(e => e.Gross)
                    .HasColumnName("GROSS")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrType)
                    .HasColumnName("HR_TYPE")
                    .HasMaxLength(100);

                entity.Property(e => e.HrTypeNo)
                    .HasColumnName("HR_TYPE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.InTimeTfmt)
                    .HasColumnName("IN_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.IsOtFromFirst)
                    .HasColumnName("IS_OT_FROM_FIRST")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.JobType)
                    .HasColumnName("JOB_TYPE")
                    .HasMaxLength(100);

                entity.Property(e => e.JobTypeNo)
                    .HasColumnName("JOB_TYPE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoblocName)
                    .HasColumnName("JOBLOC_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.JoblocNo)
                    .HasColumnName("JOBLOC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Jobtitle)
                    .HasColumnName("JOBTITLE")
                    .HasMaxLength(50);

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoinDatetime)
                    .HasColumnName("JOIN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryReason)
                    .HasColumnName("LATEENTRY_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.LineName)
                    .HasColumnName("LINE_NAME")
                    .HasMaxLength(50);

                entity.Property(e => e.LineNo)
                    .HasColumnName("LINE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaxExtOtSal)
                    .HasColumnName("MAX_EXT_OT_SAL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaxOtSal)
                    .HasColumnName("MAX_OT_SAL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Maxexotdr)
                    .HasColumnName("MAXEXOTDR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Maxotdr)
                    .HasColumnName("MAXOTDR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MovHr)
                    .HasColumnName("MOV_HR")
                    .HasMaxLength(4000);

                entity.Property(e => e.MovHrTfmt)
                    .HasColumnName("MOV_HR_TFMT")
                    .HasMaxLength(4000);

                entity.Property(e => e.OfficeHour)
                    .HasColumnName("OFFICE_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OrginEmpId)
                    .HasColumnName("ORGIN_EMP_ID")
                    .HasMaxLength(80);

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtMultiply)
                    .HasColumnName("OT_MULTIPLY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtPer)
                    .HasColumnName("OT_PER")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.OtPeron)
                    .HasColumnName("OT_PERON")
                    .HasMaxLength(1);

                entity.Property(e => e.OtRateFlg)
                    .HasColumnName("OT_RATE_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.OtRateFormula)
                    .HasColumnName("OT_RATE_FORMULA")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtStat)
                    .HasColumnName("OT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.OutTimeTfmt)
                    .HasColumnName("OUT_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.PaidDatetime)
                    .HasColumnName("PAID_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidFlg)
                    .HasColumnName("PAID_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PerDayBasic)
                    .HasColumnName("PER_DAY_BASIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PeriodNo)
                    .HasColumnName("PERIOD_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Prefix)
                    .HasColumnName("PREFIX")
                    .HasMaxLength(46);

                entity.Property(e => e.RemarksManualAtten)
                    .HasColumnName("REMARKS_MANUAL_ATTEN")
                    .HasMaxLength(500);

                entity.Property(e => e.ReportingTo)
                    .HasColumnName("REPORTING_TO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.RoundMin)
                    .HasColumnName("ROUND_MIN")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RuleJobtitleNo)
                    .HasColumnName("RULE_JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalReportSlNo)
                    .HasColumnName("SAL_REPORT_SL_NO")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.ScaleNo)
                    .HasColumnName("SCALE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SchDatetime)
                    .HasColumnName("SCH_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchInTime)
                    .HasColumnName("SCH_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchOffHr)
                    .HasColumnName("SCH_OFF_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SchOutTime)
                    .HasColumnName("SCH_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchShift)
                    .HasColumnName("SCH_SHIFT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Section)
                    .HasColumnName("SECTION")
                    .HasMaxLength(100);

                entity.Property(e => e.SectionNo)
                    .HasColumnName("SECTION_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ShiftId)
                    .HasColumnName("SHIFT_ID")
                    .HasMaxLength(169);

                entity.Property(e => e.ShiftName)
                    .HasColumnName("SHIFT_NAME")
                    .HasMaxLength(15);

                entity.Property(e => e.ShiftWorkHour)
                    .HasColumnName("SHIFT_WORK_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasMaxLength(53);

                entity.Property(e => e.TotOt)
                    .HasColumnName("TOT_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotOtTfmt)
                    .HasColumnName("TOT_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.VisibleInReport)
                    .HasColumnName("VISIBLE_IN_REPORT")
                    .HasColumnType("decimal(1, 0)");
            });

            modelBuilder.Entity<HrAttnProcess>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTN_PROCESS");

                entity.Property(e => e.ActWorkHour)
                    .HasColumnName("ACT_WORK_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ActWorkHourTfmt)
                    .HasColumnName("ACT_WORK_HOUR_TFMT")
                    .HasMaxLength(8);

                entity.Property(e => e.ActWorkIncludeMov)
                    .HasColumnName("ACT_WORK_INCLUDE_MOV")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ActWorkIncludeMovTfmt)
                    .HasColumnName("ACT_WORK_INCLUDE_MOV_TFMT")
                    .HasMaxLength(8);

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtnShift)
                    .HasColumnName("ATN_SHIFT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnSchedulePol)
                    .HasColumnName("ATTN_SCHEDULE_POL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Basic)
                    .HasColumnName("BASIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BreakTimeEnd)
                    .HasColumnName("BREAK_TIME_END")
                    .HasColumnType("datetime");

                entity.Property(e => e.BreakTimeEnd2)
                    .HasColumnName("BREAK_TIME_END2")
                    .HasColumnType("datetime");

                entity.Property(e => e.BreakTimeStart)
                    .HasColumnName("BREAK_TIME_START")
                    .HasColumnType("datetime");

                entity.Property(e => e.BreakTimeStart2)
                    .HasColumnName("BREAK_TIME_START2")
                    .HasColumnType("datetime");

                entity.Property(e => e.BuName)
                    .HasColumnName("BU_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNameParent)
                    .HasColumnName("BU_NAME_PARENT")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BuNoAll)
                    .HasColumnName("BU_NO_ALL")
                    .HasMaxLength(200);

                entity.Property(e => e.CalDt)
                    .HasColumnName("CAL_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CalcCountHr)
                    .HasColumnName("CALC_COUNT_HR")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.CashOtAmt)
                    .HasColumnName("CASH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CashOthOtAmt)
                    .HasColumnName("CASH_OTH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeOtAmt)
                    .HasColumnName("CHEQUE_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeOthOtAmt)
                    .HasColumnName("CHEQUE_OTH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DdStatus)
                    .HasColumnName("DD_STATUS")
                    .HasMaxLength(1);

                entity.Property(e => e.DdStatusAlternet)
                    .HasColumnName("DD_STATUS_ALTERNET")
                    .HasMaxLength(1);

                entity.Property(e => e.DeductMin)
                    .HasColumnName("DEDUCT_MIN")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DefaultOperationNo)
                    .HasColumnName("DEFAULT_OPERATION_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DefaultProductType)
                    .HasColumnName("DEFAULT_PRODUCT_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Department)
                    .HasColumnName("DEPARTMENT")
                    .HasMaxLength(100);

                entity.Property(e => e.DepartmentNo)
                    .HasColumnName("DEPARTMENT_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyExitStat)
                    .HasColumnName("EARLY_EXIT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyOt)
                    .HasColumnName("EARLY_OT")
                    .HasMaxLength(4000);

                entity.Property(e => e.EarlyOtFlag)
                    .HasColumnName("EARLY_OT_FLAG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyOtStartTime)
                    .HasColumnName("EARLY_OT_START_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.EarlyOtTfmt)
                    .HasColumnName("EARLY_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EarlyexitReason)
                    .HasColumnName("EARLYEXIT_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.EmpId)
                    .HasColumnName("EMP_ID")
                    .HasMaxLength(80);

                entity.Property(e => e.EmpName)
                    .HasColumnName("EMP_NAME")
                    .HasMaxLength(92);

                entity.Property(e => e.EmpNameId)
                    .HasColumnName("EMP_NAME_ID")
                    .HasMaxLength(175);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpType)
                    .HasColumnName("EMP_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpTypeName)
                    .HasColumnName("EMP_TYPE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ExtraOtMultiply)
                    .HasColumnName("EXTRA_OT_MULTIPLY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.GenInTime)
                    .HasColumnName("GEN_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenInTimeTfmt)
                    .HasColumnName("GEN_IN_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.GenOutTime)
                    .HasColumnName("GEN_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenOutTimeTfmt)
                    .HasColumnName("GEN_OUT_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.Gender)
                    .HasColumnName("GENDER")
                    .HasMaxLength(1);

                entity.Property(e => e.Gross)
                    .HasColumnName("GROSS")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrType)
                    .HasColumnName("HR_TYPE")
                    .HasMaxLength(100);

                entity.Property(e => e.HrTypeNo)
                    .HasColumnName("HR_TYPE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.InTimeTfmt)
                    .HasColumnName("IN_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.IsOtFromFirst)
                    .HasColumnName("IS_OT_FROM_FIRST")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.JobType)
                    .HasColumnName("JOB_TYPE")
                    .HasMaxLength(100);

                entity.Property(e => e.JobTypeNo)
                    .HasColumnName("JOB_TYPE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoblocName)
                    .HasColumnName("JOBLOC_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.JoblocNo)
                    .HasColumnName("JOBLOC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Jobtitle)
                    .HasColumnName("JOBTITLE")
                    .HasMaxLength(50);

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoinDatetime)
                    .HasColumnName("JOIN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryReason)
                    .HasColumnName("LATEENTRY_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.LineName)
                    .HasColumnName("LINE_NAME")
                    .HasMaxLength(50);

                entity.Property(e => e.LineNo)
                    .HasColumnName("LINE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaxExtOtSal)
                    .HasColumnName("MAX_EXT_OT_SAL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaxOtSal)
                    .HasColumnName("MAX_OT_SAL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Maxexotdr)
                    .HasColumnName("MAXEXOTDR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Maxotdr)
                    .HasColumnName("MAXOTDR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MovHr)
                    .HasColumnName("MOV_HR")
                    .HasMaxLength(4000);

                entity.Property(e => e.MovHrTfmt)
                    .HasColumnName("MOV_HR_TFMT")
                    .HasMaxLength(4000);

                entity.Property(e => e.OfficeHour)
                    .HasColumnName("OFFICE_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OperationName)
                    .HasColumnName("OPERATION_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.OrginEmpId)
                    .HasColumnName("ORGIN_EMP_ID")
                    .HasMaxLength(80);

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtAmt)
                    .HasColumnName("OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtMultiply)
                    .HasColumnName("OT_MULTIPLY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtPer)
                    .HasColumnName("OT_PER")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.OtPeron)
                    .HasColumnName("OT_PERON")
                    .HasMaxLength(1);

                entity.Property(e => e.OtRateFlg)
                    .HasColumnName("OT_RATE_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.OtRateFormula)
                    .HasColumnName("OT_RATE_FORMULA")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtRatePerHr)
                    .HasColumnName("OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtStat)
                    .HasColumnName("OT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtTfmt)
                    .HasColumnName("OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.OthOt)
                    .HasColumnName("OTH_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OthOtAmt)
                    .HasColumnName("OTH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OthOtRatePerHr)
                    .HasColumnName("OTH_OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OthOtTfmt)
                    .HasColumnName("OTH_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.OutTimeTfmt)
                    .HasColumnName("OUT_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.PaidDatetime)
                    .HasColumnName("PAID_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidFlg)
                    .HasColumnName("PAID_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PerDayBasic)
                    .HasColumnName("PER_DAY_BASIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PeriodNo)
                    .HasColumnName("PERIOD_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Prefix)
                    .HasColumnName("PREFIX")
                    .HasMaxLength(46);

                entity.Property(e => e.ProductTypeName)
                    .HasColumnName("PRODUCT_TYPE_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.RemarksManualAtten)
                    .HasColumnName("REMARKS_MANUAL_ATTEN")
                    .HasMaxLength(500);

                entity.Property(e => e.ReportingTo)
                    .HasColumnName("REPORTING_TO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.RoundMin)
                    .HasColumnName("ROUND_MIN")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RuleJobtitleNo)
                    .HasColumnName("RULE_JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalReportSlNo)
                    .HasColumnName("SAL_REPORT_SL_NO")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.ScaleNo)
                    .HasColumnName("SCALE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SchDatetime)
                    .HasColumnName("SCH_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchInTime)
                    .HasColumnName("SCH_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchOffHr)
                    .HasColumnName("SCH_OFF_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SchOutTime)
                    .HasColumnName("SCH_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchShift)
                    .HasColumnName("SCH_SHIFT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Section)
                    .HasColumnName("SECTION")
                    .HasMaxLength(100);

                entity.Property(e => e.SectionNo)
                    .HasColumnName("SECTION_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ShiftAmt)
                    .HasColumnName("SHIFT_AMT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ShiftId)
                    .HasColumnName("SHIFT_ID")
                    .HasMaxLength(169);

                entity.Property(e => e.ShiftName)
                    .HasColumnName("SHIFT_NAME")
                    .HasMaxLength(15);

                entity.Property(e => e.ShiftWorkHour)
                    .HasColumnName("SHIFT_WORK_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasMaxLength(53);

                entity.Property(e => e.TotOt)
                    .HasColumnName("TOT_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotOtAmt)
                    .HasColumnName("TOT_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotOtTfmt)
                    .HasColumnName("TOT_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.UnadjustedOthOt)
                    .HasColumnName("UNADJUSTED_OTH_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UnadjustedTotOt)
                    .HasColumnName("UNADJUSTED_TOT_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UnadjustedTotOtTfmt)
                    .HasColumnName("UNADJUSTED_TOT_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.VisibleInReport)
                    .HasColumnName("VISIBLE_IN_REPORT")
                    .HasColumnType("decimal(1, 0)");
            });

            modelBuilder.Entity<HrAttnProcess27102017>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTN_PROCESS_27102017");

                entity.Property(e => e.ActWorkHour)
                    .HasColumnName("ACT_WORK_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ActWorkHourTfmt)
                    .HasColumnName("ACT_WORK_HOUR_TFMT")
                    .HasMaxLength(8);

                entity.Property(e => e.ActWorkIncludeMov)
                    .HasColumnName("ACT_WORK_INCLUDE_MOV")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ActWorkIncludeMovTfmt)
                    .HasColumnName("ACT_WORK_INCLUDE_MOV_TFMT")
                    .HasMaxLength(8);

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtnShift)
                    .HasColumnName("ATN_SHIFT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnSchedulePol)
                    .HasColumnName("ATTN_SCHEDULE_POL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Basic)
                    .HasColumnName("BASIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BreakTimeEnd)
                    .HasColumnName("BREAK_TIME_END")
                    .HasColumnType("datetime");

                entity.Property(e => e.BreakTimeEnd2)
                    .HasColumnName("BREAK_TIME_END2")
                    .HasColumnType("datetime");

                entity.Property(e => e.BreakTimeStart)
                    .HasColumnName("BREAK_TIME_START")
                    .HasColumnType("datetime");

                entity.Property(e => e.BreakTimeStart2)
                    .HasColumnName("BREAK_TIME_START2")
                    .HasColumnType("datetime");

                entity.Property(e => e.BuName)
                    .HasColumnName("BU_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNameParent)
                    .HasColumnName("BU_NAME_PARENT")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BuNoAll)
                    .HasColumnName("BU_NO_ALL")
                    .HasMaxLength(200);

                entity.Property(e => e.CalDt)
                    .HasColumnName("CAL_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CalcCountHr)
                    .HasColumnName("CALC_COUNT_HR")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.CashOtAmt)
                    .HasColumnName("CASH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CashOthOtAmt)
                    .HasColumnName("CASH_OTH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeOtAmt)
                    .HasColumnName("CHEQUE_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeOthOtAmt)
                    .HasColumnName("CHEQUE_OTH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DdStatus)
                    .HasColumnName("DD_STATUS")
                    .HasMaxLength(1);

                entity.Property(e => e.DdStatusAlternet)
                    .HasColumnName("DD_STATUS_ALTERNET")
                    .HasMaxLength(1);

                entity.Property(e => e.DeductMin)
                    .HasColumnName("DEDUCT_MIN")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DefaultOperationNo)
                    .HasColumnName("DEFAULT_OPERATION_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DefaultProductType)
                    .HasColumnName("DEFAULT_PRODUCT_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Department)
                    .HasColumnName("DEPARTMENT")
                    .HasMaxLength(100);

                entity.Property(e => e.DepartmentNo)
                    .HasColumnName("DEPARTMENT_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyExitStat)
                    .HasColumnName("EARLY_EXIT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyOt)
                    .HasColumnName("EARLY_OT")
                    .HasMaxLength(4000);

                entity.Property(e => e.EarlyOtFlag)
                    .HasColumnName("EARLY_OT_FLAG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyOtStartTime)
                    .HasColumnName("EARLY_OT_START_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.EarlyOtTfmt)
                    .HasColumnName("EARLY_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EarlyexitReason)
                    .HasColumnName("EARLYEXIT_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.EmpId)
                    .HasColumnName("EMP_ID")
                    .HasMaxLength(80);

                entity.Property(e => e.EmpName)
                    .HasColumnName("EMP_NAME")
                    .HasMaxLength(92);

                entity.Property(e => e.EmpNameId)
                    .HasColumnName("EMP_NAME_ID")
                    .HasMaxLength(175);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpType)
                    .HasColumnName("EMP_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpTypeName)
                    .HasColumnName("EMP_TYPE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ExtraOtMultiply)
                    .HasColumnName("EXTRA_OT_MULTIPLY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.GenInTime)
                    .HasColumnName("GEN_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenInTimeTfmt)
                    .HasColumnName("GEN_IN_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.GenOutTime)
                    .HasColumnName("GEN_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenOutTimeTfmt)
                    .HasColumnName("GEN_OUT_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.Gender)
                    .HasColumnName("GENDER")
                    .HasMaxLength(1);

                entity.Property(e => e.Gross)
                    .HasColumnName("GROSS")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrType)
                    .HasColumnName("HR_TYPE")
                    .HasMaxLength(100);

                entity.Property(e => e.HrTypeNo)
                    .HasColumnName("HR_TYPE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.InTimeTfmt)
                    .HasColumnName("IN_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.IsOtFromFirst)
                    .HasColumnName("IS_OT_FROM_FIRST")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.JobType)
                    .HasColumnName("JOB_TYPE")
                    .HasMaxLength(100);

                entity.Property(e => e.JobTypeNo)
                    .HasColumnName("JOB_TYPE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoblocName)
                    .HasColumnName("JOBLOC_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.JoblocNo)
                    .HasColumnName("JOBLOC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Jobtitle)
                    .HasColumnName("JOBTITLE")
                    .HasMaxLength(50);

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoinDatetime)
                    .HasColumnName("JOIN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryReason)
                    .HasColumnName("LATEENTRY_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.LineName)
                    .HasColumnName("LINE_NAME")
                    .HasMaxLength(50);

                entity.Property(e => e.LineNo)
                    .HasColumnName("LINE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaxExtOtSal)
                    .HasColumnName("MAX_EXT_OT_SAL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaxOtSal)
                    .HasColumnName("MAX_OT_SAL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Maxexotdr)
                    .HasColumnName("MAXEXOTDR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Maxotdr)
                    .HasColumnName("MAXOTDR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MovHr)
                    .HasColumnName("MOV_HR")
                    .HasMaxLength(4000);

                entity.Property(e => e.MovHrTfmt)
                    .HasColumnName("MOV_HR_TFMT")
                    .HasMaxLength(4000);

                entity.Property(e => e.OfficeHour)
                    .HasColumnName("OFFICE_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OperationName)
                    .HasColumnName("OPERATION_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.OrginEmpId)
                    .HasColumnName("ORGIN_EMP_ID")
                    .HasMaxLength(80);

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtAmt)
                    .HasColumnName("OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtMultiply)
                    .HasColumnName("OT_MULTIPLY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtPer)
                    .HasColumnName("OT_PER")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.OtPeron)
                    .HasColumnName("OT_PERON")
                    .HasMaxLength(1);

                entity.Property(e => e.OtRateFlg)
                    .HasColumnName("OT_RATE_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.OtRateFormula)
                    .HasColumnName("OT_RATE_FORMULA")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtRatePerHr)
                    .HasColumnName("OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtStat)
                    .HasColumnName("OT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtTfmt)
                    .HasColumnName("OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.OthOt)
                    .HasColumnName("OTH_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OthOtAmt)
                    .HasColumnName("OTH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OthOtRatePerHr)
                    .HasColumnName("OTH_OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OthOtTfmt)
                    .HasColumnName("OTH_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.OutTimeTfmt)
                    .HasColumnName("OUT_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.PaidDatetime)
                    .HasColumnName("PAID_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidFlg)
                    .HasColumnName("PAID_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PerDayBasic)
                    .HasColumnName("PER_DAY_BASIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PeriodNo)
                    .HasColumnName("PERIOD_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Prefix)
                    .HasColumnName("PREFIX")
                    .HasMaxLength(46);

                entity.Property(e => e.ProductTypeName)
                    .HasColumnName("PRODUCT_TYPE_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.RemarksManualAtten)
                    .HasColumnName("REMARKS_MANUAL_ATTEN")
                    .HasMaxLength(500);

                entity.Property(e => e.ReportingTo)
                    .HasColumnName("REPORTING_TO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.RoundMin)
                    .HasColumnName("ROUND_MIN")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RuleJobtitleNo)
                    .HasColumnName("RULE_JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalReportSlNo)
                    .HasColumnName("SAL_REPORT_SL_NO")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.ScaleNo)
                    .HasColumnName("SCALE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SchDatetime)
                    .HasColumnName("SCH_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchInTime)
                    .HasColumnName("SCH_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchOffHr)
                    .HasColumnName("SCH_OFF_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SchOutTime)
                    .HasColumnName("SCH_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchShift)
                    .HasColumnName("SCH_SHIFT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Section)
                    .HasColumnName("SECTION")
                    .HasMaxLength(100);

                entity.Property(e => e.SectionNo)
                    .HasColumnName("SECTION_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ShiftAmt)
                    .HasColumnName("SHIFT_AMT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ShiftId)
                    .HasColumnName("SHIFT_ID")
                    .HasMaxLength(169);

                entity.Property(e => e.ShiftName)
                    .HasColumnName("SHIFT_NAME")
                    .HasMaxLength(15);

                entity.Property(e => e.ShiftWorkHour)
                    .HasColumnName("SHIFT_WORK_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasMaxLength(53);

                entity.Property(e => e.TotOt)
                    .HasColumnName("TOT_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotOtAmt)
                    .HasColumnName("TOT_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotOtTfmt)
                    .HasColumnName("TOT_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.UnadjustedOthOt)
                    .HasColumnName("UNADJUSTED_OTH_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UnadjustedTotOt)
                    .HasColumnName("UNADJUSTED_TOT_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UnadjustedTotOtTfmt)
                    .HasColumnName("UNADJUSTED_TOT_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.VisibleInReport)
                    .HasColumnName("VISIBLE_IN_REPORT")
                    .HasColumnType("decimal(1, 0)");
            });

            modelBuilder.Entity<HrAttnProcessTo300916>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTN_PROCESS_TO_300916");

                entity.Property(e => e.ActWorkHour)
                    .HasColumnName("ACT_WORK_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ActWorkHourTfmt)
                    .HasColumnName("ACT_WORK_HOUR_TFMT")
                    .HasMaxLength(8);

                entity.Property(e => e.ActWorkIncludeMov)
                    .HasColumnName("ACT_WORK_INCLUDE_MOV")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ActWorkIncludeMovTfmt)
                    .HasColumnName("ACT_WORK_INCLUDE_MOV_TFMT")
                    .HasMaxLength(8);

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtnShift)
                    .HasColumnName("ATN_SHIFT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnSchedulePol)
                    .HasColumnName("ATTN_SCHEDULE_POL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Basic)
                    .HasColumnName("BASIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BreakTimeEnd)
                    .HasColumnName("BREAK_TIME_END")
                    .HasColumnType("datetime");

                entity.Property(e => e.BreakTimeEnd2)
                    .HasColumnName("BREAK_TIME_END2")
                    .HasColumnType("datetime");

                entity.Property(e => e.BreakTimeStart)
                    .HasColumnName("BREAK_TIME_START")
                    .HasColumnType("datetime");

                entity.Property(e => e.BreakTimeStart2)
                    .HasColumnName("BREAK_TIME_START2")
                    .HasColumnType("datetime");

                entity.Property(e => e.BuName)
                    .HasColumnName("BU_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNameParent)
                    .HasColumnName("BU_NAME_PARENT")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BuNoAll)
                    .HasColumnName("BU_NO_ALL")
                    .HasMaxLength(200);

                entity.Property(e => e.CalDt)
                    .HasColumnName("CAL_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CalcCountHr)
                    .HasColumnName("CALC_COUNT_HR")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.CashOtAmt)
                    .HasColumnName("CASH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CashOthOtAmt)
                    .HasColumnName("CASH_OTH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeOtAmt)
                    .HasColumnName("CHEQUE_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeOthOtAmt)
                    .HasColumnName("CHEQUE_OTH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DdStatus)
                    .HasColumnName("DD_STATUS")
                    .HasMaxLength(1);

                entity.Property(e => e.DdStatusAlternet)
                    .HasColumnName("DD_STATUS_ALTERNET")
                    .HasMaxLength(1);

                entity.Property(e => e.DeductMin)
                    .HasColumnName("DEDUCT_MIN")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DefaultOperationNo)
                    .HasColumnName("DEFAULT_OPERATION_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DefaultProductType)
                    .HasColumnName("DEFAULT_PRODUCT_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Department)
                    .HasColumnName("DEPARTMENT")
                    .HasMaxLength(100);

                entity.Property(e => e.DepartmentNo)
                    .HasColumnName("DEPARTMENT_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyExitStat)
                    .HasColumnName("EARLY_EXIT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyOt)
                    .HasColumnName("EARLY_OT")
                    .HasMaxLength(4000);

                entity.Property(e => e.EarlyOtFlag)
                    .HasColumnName("EARLY_OT_FLAG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyOtStartTime)
                    .HasColumnName("EARLY_OT_START_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.EarlyOtTfmt)
                    .HasColumnName("EARLY_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EarlyexitReason)
                    .HasColumnName("EARLYEXIT_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.EmpId)
                    .HasColumnName("EMP_ID")
                    .HasMaxLength(80);

                entity.Property(e => e.EmpName)
                    .HasColumnName("EMP_NAME")
                    .HasMaxLength(92);

                entity.Property(e => e.EmpNameId)
                    .HasColumnName("EMP_NAME_ID")
                    .HasMaxLength(175);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpType)
                    .HasColumnName("EMP_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpTypeName)
                    .HasColumnName("EMP_TYPE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ExtraOtMultiply)
                    .HasColumnName("EXTRA_OT_MULTIPLY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.GenInTime)
                    .HasColumnName("GEN_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenInTimeTfmt)
                    .HasColumnName("GEN_IN_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.GenOutTime)
                    .HasColumnName("GEN_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenOutTimeTfmt)
                    .HasColumnName("GEN_OUT_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.Gender)
                    .HasColumnName("GENDER")
                    .HasMaxLength(1);

                entity.Property(e => e.Gross)
                    .HasColumnName("GROSS")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrType)
                    .HasColumnName("HR_TYPE")
                    .HasMaxLength(100);

                entity.Property(e => e.HrTypeNo)
                    .HasColumnName("HR_TYPE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.InTimeTfmt)
                    .HasColumnName("IN_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.IsOtFromFirst)
                    .HasColumnName("IS_OT_FROM_FIRST")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.JobType)
                    .HasColumnName("JOB_TYPE")
                    .HasMaxLength(100);

                entity.Property(e => e.JobTypeNo)
                    .HasColumnName("JOB_TYPE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoblocName)
                    .HasColumnName("JOBLOC_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.JoblocNo)
                    .HasColumnName("JOBLOC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Jobtitle)
                    .HasColumnName("JOBTITLE")
                    .HasMaxLength(50);

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoinDatetime)
                    .HasColumnName("JOIN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryReason)
                    .HasColumnName("LATEENTRY_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.LineName)
                    .HasColumnName("LINE_NAME")
                    .HasMaxLength(50);

                entity.Property(e => e.LineNo)
                    .HasColumnName("LINE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaxExtOtSal)
                    .HasColumnName("MAX_EXT_OT_SAL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaxOtSal)
                    .HasColumnName("MAX_OT_SAL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Maxexotdr)
                    .HasColumnName("MAXEXOTDR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Maxotdr)
                    .HasColumnName("MAXOTDR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MovHr)
                    .HasColumnName("MOV_HR")
                    .HasMaxLength(4000);

                entity.Property(e => e.MovHrTfmt)
                    .HasColumnName("MOV_HR_TFMT")
                    .HasMaxLength(4000);

                entity.Property(e => e.OfficeHour)
                    .HasColumnName("OFFICE_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OperationName)
                    .HasColumnName("OPERATION_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.OrginEmpId)
                    .HasColumnName("ORGIN_EMP_ID")
                    .HasMaxLength(80);

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtAmt)
                    .HasColumnName("OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtMultiply)
                    .HasColumnName("OT_MULTIPLY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtPer)
                    .HasColumnName("OT_PER")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.OtPeron)
                    .HasColumnName("OT_PERON")
                    .HasMaxLength(1);

                entity.Property(e => e.OtRateFlg)
                    .HasColumnName("OT_RATE_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.OtRateFormula)
                    .HasColumnName("OT_RATE_FORMULA")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtRatePerHr)
                    .HasColumnName("OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtStat)
                    .HasColumnName("OT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtTfmt)
                    .HasColumnName("OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.OthOt)
                    .HasColumnName("OTH_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OthOtAmt)
                    .HasColumnName("OTH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OthOtRatePerHr)
                    .HasColumnName("OTH_OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OthOtTfmt)
                    .HasColumnName("OTH_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.OutTimeTfmt)
                    .HasColumnName("OUT_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.PaidDatetime)
                    .HasColumnName("PAID_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidFlg)
                    .HasColumnName("PAID_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PerDayBasic)
                    .HasColumnName("PER_DAY_BASIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PeriodNo)
                    .HasColumnName("PERIOD_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Prefix)
                    .HasColumnName("PREFIX")
                    .HasMaxLength(46);

                entity.Property(e => e.ProductTypeName)
                    .HasColumnName("PRODUCT_TYPE_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.RemarksManualAtten)
                    .HasColumnName("REMARKS_MANUAL_ATTEN")
                    .HasMaxLength(500);

                entity.Property(e => e.ReportingTo)
                    .HasColumnName("REPORTING_TO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.RoundMin)
                    .HasColumnName("ROUND_MIN")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RuleJobtitleNo)
                    .HasColumnName("RULE_JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalReportSlNo)
                    .HasColumnName("SAL_REPORT_SL_NO")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.ScaleNo)
                    .HasColumnName("SCALE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SchDatetime)
                    .HasColumnName("SCH_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchInTime)
                    .HasColumnName("SCH_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchOffHr)
                    .HasColumnName("SCH_OFF_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SchOutTime)
                    .HasColumnName("SCH_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchShift)
                    .HasColumnName("SCH_SHIFT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Section)
                    .HasColumnName("SECTION")
                    .HasMaxLength(100);

                entity.Property(e => e.SectionNo)
                    .HasColumnName("SECTION_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ShiftAmt)
                    .HasColumnName("SHIFT_AMT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ShiftId)
                    .HasColumnName("SHIFT_ID")
                    .HasMaxLength(169);

                entity.Property(e => e.ShiftName)
                    .HasColumnName("SHIFT_NAME")
                    .HasMaxLength(15);

                entity.Property(e => e.ShiftWorkHour)
                    .HasColumnName("SHIFT_WORK_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasMaxLength(53);

                entity.Property(e => e.TotOt)
                    .HasColumnName("TOT_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotOtAmt)
                    .HasColumnName("TOT_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotOtTfmt)
                    .HasColumnName("TOT_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.UnadjustedOthOt)
                    .HasColumnName("UNADJUSTED_OTH_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UnadjustedTotOt)
                    .HasColumnName("UNADJUSTED_TOT_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UnadjustedTotOtTfmt)
                    .HasColumnName("UNADJUSTED_TOT_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.VisibleInReport)
                    .HasColumnName("VISIBLE_IN_REPORT")
                    .HasColumnType("decimal(1, 0)");
            });

            modelBuilder.Entity<HrAttnbonusadj>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTNBONUSADJ");

                entity.Property(e => e.AdjAmount)
                    .HasColumnName("ADJ_AMOUNT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AdjDatetime)
                    .HasColumnName("ADJ_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.AdjsalaryNo)
                    .HasColumnName("ADJSALARY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ApproveBy)
                    .HasColumnName("APPROVE_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ApproveDatetime)
                    .HasColumnName("APPROVE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.HrAttnbonusadjNo)
                    .HasColumnName("HR_ATTNBONUSADJ_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalaryNo)
                    .HasColumnName("SALARY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAttnbonusdtl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTNBONUSDTL");

                entity.Property(e => e.Amount)
                    .HasColumnName("AMOUNT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DependBilldtlNo)
                    .HasColumnName("DEPEND_BILLDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.HrAttnbonusdtlNo)
                    .HasColumnName("HR_ATTNBONUSDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalaryNo)
                    .HasColumnName("SALARY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalstartDatetime)
                    .HasColumnName("SALSTART_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAttnopbal>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTNOPBAL");

                entity.Property(e => e.AttnopbalNo)
                    .HasColumnName("ATTNOPBAL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.OpBal)
                    .HasColumnName("OP_BAL")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.OpDatetime)
                    .HasColumnName("OP_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAttnpol>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTNPOL");

                entity.Property(e => e.AttnpolNo)
                    .HasColumnName("ATTNPOL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ProvisionMonth)
                    .HasColumnName("PROVISION_MONTH")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.RuleDatetime)
                    .HasColumnName("RULE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrAttnpoldtl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTNPOLDTL");

                entity.Property(e => e.AttnpolNo)
                    .HasColumnName("ATTNPOL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnpoldtlNo)
                    .HasColumnName("ATTNPOLDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EarnAmt)
                    .HasColumnName("EARN_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.EarnPer)
                    .HasColumnName("EARN_PER")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.EndAttnPer)
                    .HasColumnName("END_ATTN_PER")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.LateEntryMin)
                    .HasColumnName("LATE_ENTRY_MIN")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.MaxAmt)
                    .HasColumnName("MAX_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Peron)
                    .HasColumnName("PERON")
                    .HasMaxLength(1);

                entity.Property(e => e.PieceEarnAmt)
                    .HasColumnName("PIECE_EARN_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.StartAttnPer)
                    .HasColumnName("START_ATTN_PER")
                    .HasColumnType("decimal(5, 2)");
            });

            modelBuilder.Entity<HrAttnpoldtladdi>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ATTNPOLDTLADDI");

                entity.Property(e => e.AttnpolNo)
                    .HasColumnName("ATTNPOL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnpoldtlNo)
                    .HasColumnName("ATTNPOLDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Attnpoldtladdi)
                    .HasColumnName("ATTNPOLDTLADDI")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EarnAmt)
                    .HasColumnName("EARN_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.EarnPer)
                    .HasColumnName("EARN_PER")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Gender)
                    .HasColumnName("GENDER")
                    .HasMaxLength(1)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.MaxAmt)
                    .HasColumnName("MAX_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Peron)
                    .HasColumnName("PERON")
                    .HasMaxLength(1);

                entity.Property(e => e.ServiceMonthFrom)
                    .HasColumnName("SERVICE_MONTH_FROM")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ServiceMonthTo)
                    .HasColumnName("SERVICE_MONTH_TO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrBankinfo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_BANKINFO");

                entity.Property(e => e.BankName)
                    .HasColumnName("BANK_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.BankNo)
                    .HasColumnName("BANK_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrBanksalaryExc>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_BANKSALARY_EXC");

                entity.Property(e => e.BanksalaryExcId)
                    .HasColumnName("BANKSALARY_EXC_ID")
                    .HasMaxLength(10);

                entity.Property(e => e.BanksalaryExcNo)
                    .HasColumnName("BANKSALARY_EXC_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.MaxBankAmt)
                    .HasColumnName("MAX_BANK_AMT")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrBill>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_BILL");

                entity.Property(e => e.ApproveDatetime)
                    .HasColumnName("APPROVE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ApproveFlg)
                    .HasColumnName("APPROVE_FLG")
                    .HasMaxLength(1);

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AstCrAccNo)
                    .HasColumnName("AST_CR_ACC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttachFileName)
                    .HasColumnName("ATTACH_FILE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.AttachFileNo)
                    .HasColumnName("ATTACH_FILE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BillDatetime)
                    .HasColumnName("BILL_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.BillId)
                    .HasColumnName("BILL_ID")
                    .HasMaxLength(50);

                entity.Property(e => e.BillNo)
                    .HasColumnName("BILL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BilltypedtlNo)
                    .HasColumnName("BILLTYPEDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CheckDatetime)
                    .HasColumnName("CHECK_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.CheckedBy)
                    .HasColumnName("CHECKED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CheckedFlg)
                    .HasColumnName("CHECKED_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.FileExt)
                    .HasColumnName("FILE_EXT")
                    .HasMaxLength(10);

                entity.Property(e => e.FileSize)
                    .HasColumnName("FILE_SIZE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.LblCrAccNo)
                    .HasColumnName("LBL_CR_ACC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PaidBy)
                    .HasColumnName("PAID_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Pay)
                    .HasColumnName("PAY")
                    .HasMaxLength(1);

                entity.Property(e => e.PayDatetime)
                    .HasColumnName("PAY_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PayVNo)
                    .HasColumnName("PAY_V_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PreparedBy)
                    .HasColumnName("PREPARED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ReceivedBy)
                    .HasColumnName("RECEIVED_BY")
                    .HasMaxLength(100);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.VNo)
                    .HasColumnName("V_NO")
                    .HasColumnType("decimal(30, 0)");
            });

            modelBuilder.Entity<HrBillEmpAcc>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_BILL_EMP_ACC");

                entity.Property(e => e.AccNo)
                    .HasColumnName("ACC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BillaccNo)
                    .HasColumnName("BILLACC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BilltypedtlNo)
                    .HasColumnName("BILLTYPEDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrBillProvEmpAcc>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_BILL_PROV_EMP_ACC");

                entity.Property(e => e.AccNo)
                    .HasColumnName("ACC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BilltypedtlNo)
                    .HasColumnName("BILLTYPEDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ProaccNo)
                    .HasColumnName("PROACC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrBilldtl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_BILLDTL");

                entity.Property(e => e.Amount)
                    .HasColumnName("AMOUNT")
                    .HasColumnType("decimal(14, 2)");

                entity.Property(e => e.AppAmount)
                    .HasColumnName("APP_AMOUNT")
                    .HasColumnType("decimal(14, 2)");

                entity.Property(e => e.BillNo)
                    .HasColumnName("BILL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BilldtlNo)
                    .HasColumnName("BILLDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CheckedAmount)
                    .HasColumnName("CHECKED_AMOUNT")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CostNo)
                    .HasColumnName("COST_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DedAmount)
                    .HasColumnName("DED_AMOUNT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DedElementNo)
                    .HasColumnName("DED_ELEMENT_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DrAccNo)
                    .HasColumnName("DR_ACC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ExpenseDatetime)
                    .HasColumnName("EXPENSE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.MaxAmt)
                    .HasColumnName("MAX_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Per)
                    .HasColumnName("PER")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Peron)
                    .HasColumnName("PERON")
                    .HasMaxLength(1);

                entity.Property(e => e.Purpose)
                    .HasColumnName("PURPOSE")
                    .HasMaxLength(300);

                entity.Property(e => e.Remarks)
                    .HasColumnName("REMARKS")
                    .HasMaxLength(250);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrBu>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_BU");

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.BaNo)
                    .HasColumnName("BA_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BuActivityTypeNo)
                    .HasColumnName("BU_ACTIVITY_TYPE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BuAddress)
                    .HasColumnName("BU_ADDRESS")
                    .HasMaxLength(300);

                entity.Property(e => e.BuAddress2)
                    .HasColumnName("BU_ADDRESS_2")
                    .HasMaxLength(299);

                entity.Property(e => e.BuAlias)
                    .HasColumnName("BU_ALIAS")
                    .HasMaxLength(30);

                entity.Property(e => e.BuCategory)
                    .HasColumnName("BU_CATEGORY")
                    .HasMaxLength(50);

                entity.Property(e => e.BuCategoryFlag)
                    .HasColumnName("BU_CATEGORY_FLAG")
                    .HasMaxLength(1);

                entity.Property(e => e.BuCode)
                    .HasColumnName("BU_CODE")
                    .HasMaxLength(30);

                entity.Property(e => e.BuGrade)
                    .HasColumnName("BU_GRADE")
                    .HasMaxLength(3);

                entity.Property(e => e.BuLocationType)
                    .HasColumnName("BU_LOCATION_TYPE")
                    .HasMaxLength(50);

                entity.Property(e => e.BuName)
                    .HasColumnName("BU_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNameLevel)
                    .HasColumnName("BU_NAME_LEVEL")
                    .HasMaxLength(3000);

                entity.Property(e => e.BuNameTree)
                    .HasColumnName("BU_NAME_TREE")
                    .HasMaxLength(500);

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BuNoParent)
                    .HasColumnName("BU_NO_PARENT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BuNoTree)
                    .HasColumnName("BU_NO_TREE")
                    .HasMaxLength(200);

                entity.Property(e => e.BuOpeningDatetime)
                    .HasColumnName("BU_OPENING_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.BuPhone)
                    .HasColumnName("BU_PHONE")
                    .HasMaxLength(150);

                entity.Property(e => e.BuTypeNo)
                    .HasColumnName("BU_TYPE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CashAccNo)
                    .HasColumnName("CASH_ACC_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeAccNo)
                    .HasColumnName("CHEQUE_ACC_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ContractBy)
                    .HasColumnName("CONTRACT_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CreditFlag)
                    .HasColumnName("CREDIT_FLAG")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.DefaltFlag)
                    .HasColumnName("DEFALT_FLAG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(500);

                entity.Property(e => e.EMail)
                    .HasColumnName("E_MAIL")
                    .HasMaxLength(100);

                entity.Property(e => e.Fax)
                    .HasColumnName("FAX")
                    .HasMaxLength(100);

                entity.Property(e => e.IsLeaf)
                    .HasColumnName("IS_LEAF")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.LcAccNo)
                    .HasColumnName("LC_ACC_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Loc)
                    .HasColumnName("LOC")
                    .HasMaxLength(100);

                entity.Property(e => e.LookupdtlNo)
                    .HasColumnName("LOOKUPDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.NlsBuName)
                    .HasColumnName("NLS_BU_NAME")
                    .HasMaxLength(250);

                entity.Property(e => e.OperationBy)
                    .HasColumnName("OPERATION_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ProductionActivityNo)
                    .HasColumnName("PRODUCTION_ACTIVITY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.RefCompanyNo)
                    .HasColumnName("REF_COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ReportSlNo)
                    .HasColumnName("REPORT_SL_NO")
                    .HasColumnType("decimal(5, 0)");

                entity.Property(e => e.RootBuNo)
                    .HasColumnName("ROOT_BU_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.TreeLevel)
                    .HasColumnName("TREE_LEVEL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Web)
                    .HasColumnName("WEB")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<HrBuGroup>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_BU_GROUP");

                entity.Property(e => e.BuGroupNo)
                    .HasColumnName("BU_GROUP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BuType)
                    .HasColumnName("BU_TYPE")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrBudtl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_BUDTL");

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BudtlNo)
                    .HasColumnName("BUDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(500);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EndDatetime)
                    .HasColumnName("END_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.InChargeType)
                    .HasColumnName("IN_CHARGE_TYPE")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.StartDatetime)
                    .HasColumnName("START_datetime")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrBugoal>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_BUGOAL");

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BugoalId)
                    .HasColumnName("BUGOAL_ID")
                    .HasMaxLength(50);

                entity.Property(e => e.BugoalNo)
                    .HasColumnName("BUGOAL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.GoalFromDt)
                    .HasColumnName("GOAL_FROM_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.GoalToDt)
                    .HasColumnName("GOAL_TO_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.ImpactAchive1)
                    .HasColumnName("IMPACT_ACHIVE1")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ImpactAchive2)
                    .HasColumnName("IMPACT_ACHIVE2")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.InteractWith)
                    .HasColumnName("INTERACT_WITH")
                    .HasMaxLength(150);

                entity.Property(e => e.MeasureUnit)
                    .HasColumnName("MEASURE_UNIT")
                    .HasMaxLength(100);

                entity.Property(e => e.Outcome)
                    .HasColumnName("OUTCOME")
                    .HasMaxLength(250);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.Target)
                    .HasColumnName("TARGET")
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<HrBugoaldtl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_BUGOALDTL");

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.BugoalNo)
                    .HasColumnName("BUGOAL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BugoaldtlNo)
                    .HasColumnName("BUGOALDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EffectiveFromDatetime)
                    .HasColumnName("EFFECTIVE_FROM_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.EffectiveToDatetime)
                    .HasColumnName("EFFECTIVE_TO_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ObjectiveSerial)
                    .HasColumnName("OBJECTIVE_SERIAL")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.Objectives)
                    .HasColumnName("OBJECTIVES")
                    .HasMaxLength(1000);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrButasks>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_BUTASKS");

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ButaskNo)
                    .HasColumnName("BUTASK_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Butaskid)
                    .HasColumnName("BUTASKID")
                    .HasMaxLength(50);

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(500);

                entity.Property(e => e.EffectiveFromDatetime)
                    .HasColumnName("EFFECTIVE_FROM_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.EffectiveToDatetime)
                    .HasColumnName("EFFECTIVE_TO_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LevelNo)
                    .HasColumnName("LEVEL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.NatureNo)
                    .HasColumnName("NATURE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.TaskNo)
                    .HasColumnName("TASK_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.TypeNo)
                    .HasColumnName("TYPE_NO")
                    .HasColumnType("decimal(30, 0)");
            });

            modelBuilder.Entity<HrCarddata>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_CARDDATA");

                entity.Property(e => e.AttnDatetime)
                    .HasColumnName("ATTN_datetime")
                    .HasMaxLength(200);

                entity.Property(e => e.CardNo)
                    .HasColumnName("CARD_NO")
                    .HasMaxLength(100);

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(50);

                entity.Property(e => e.QueryFailed)
                    .HasColumnName("QUERY_FAILED")
                    .HasMaxLength(500);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrCarddataExt>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_CARDDATA_EXT");

                entity.Property(e => e.CardData)
                    .HasColumnName("CARD_DATA")
                    .HasMaxLength(1000);

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrCarddatasize>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_CARDDATASIZE");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.FieldName)
                    .HasColumnName("FIELD_NAME")
                    .HasMaxLength(20);

                entity.Property(e => e.LengthEnd)
                    .HasColumnName("LENGTH_END")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.LengthStart)
                    .HasColumnName("LENGTH_START")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.SlNo)
                    .HasColumnName("SL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrCompensationCalcElement>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_COMPENSATION_CALC_ELEMENT");

                entity.Property(e => e.CalcType)
                    .HasColumnName("CALC_TYPE")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompCalcNo)
                    .HasColumnName("COMP_CALC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompruleNo)
                    .HasColumnName("COMPRULE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.MaxAmt)
                    .HasColumnName("MAX_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Per)
                    .HasColumnName("PER")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Peron)
                    .HasColumnName("PERON")
                    .HasMaxLength(1);

                entity.Property(e => e.RuleDatetime)
                    .HasColumnName("RULE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrCompensationRule>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_COMPENSATION_RULE");

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompareWith)
                    .HasColumnName("COMPARE_WITH")
                    .HasMaxLength(100);

                entity.Property(e => e.CompruleNo)
                    .HasColumnName("COMPRULE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EffectType)
                    .HasColumnName("EFFECT_TYPE")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.MaxAmt)
                    .HasColumnName("MAX_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.MinPieceEarning)
                    .HasColumnName("MIN_PIECE_EARNING")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Per)
                    .HasColumnName("PER")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Peron)
                    .HasColumnName("PERON")
                    .HasMaxLength(1);

                entity.Property(e => e.ProbationMonth)
                    .HasColumnName("PROBATION_MONTH")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.RuleDatetime)
                    .HasColumnName("RULE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrConfig>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_CONFIG");

                entity.Property(e => e.AttnBonus)
                    .HasColumnName("ATTN_BONUS")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttndCardEnterDelay)
                    .HasColumnName("ATTND_CARD_ENTER_DELAY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.AutoIdYearCountFlag)
                    .HasColumnName("AUTO_ID_YEAR_COUNT_FLAG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.Basic)
                    .HasColumnName("BASIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BasicSalaryFormat)
                    .HasColumnName("BASIC_SALARY_FORMAT")
                    .HasMaxLength(20);

                entity.Property(e => e.BuDisplay)
                    .HasColumnName("BU_DISPLAY")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.BuTitleBy)
                    .HasColumnName("BU_TITLE_BY")
                    .HasMaxLength(30);

                entity.Property(e => e.CalcCountHrAsOtSkip)
                    .HasColumnName("CALC_COUNT_HR_AS_OT_SKIP")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CardNoGenType)
                    .HasColumnName("CARD_NO_GEN_TYPE")
                    .HasMaxLength(1);

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyWiseEmpIdSeq)
                    .HasColumnName("COMPANY_WISE_EMP_ID_SEQ")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.CompanyWiseIdLength)
                    .HasColumnName("COMPANY_WISE_ID_LENGTH")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.Conv)
                    .HasColumnName("CONV")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DefaultSalarySheet)
                    .HasColumnName("DEFAULT_SALARY_SHEET")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DisplaySalaryPeriod)
                    .HasColumnName("DISPLAY_SALARY_PERIOD")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DrTr)
                    .HasColumnName("DR_TR")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EmpIdIsCharCheck)
                    .HasColumnName("EMP_ID_IS_CHAR_CHECK")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EmpPhotoReadBy)
                    .HasColumnName("EMP_PHOTO_READ_BY")
                    .HasMaxLength(30);

                entity.Property(e => e.ErrorImage)
                    .HasColumnName("ERROR_IMAGE")
                    .HasColumnType("image");

                entity.Property(e => e.ExcludeAttendanceInProcess)
                    .HasColumnName("EXCLUDE_ATTENDANCE_IN_PROCESS")
                    .HasColumnType("decimal(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.FoodAllow)
                    .HasColumnName("FOOD_ALLOW")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.GenInTimeFlag)
                    .HasColumnName("GEN_IN_TIME_FLAG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.GroupFlag)
                    .HasColumnName("GROUP_FLAG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HideIncrementInEmpForm)
                    .HasColumnName("HIDE_INCREMENT_IN_EMP_FORM")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.HidePayrollInEmpForm)
                    .HasColumnName("HIDE_PAYROLL_IN_EMP_FORM")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.HideSalaryInEmpForm)
                    .HasColumnName("HIDE_SALARY_IN_EMP_FORM")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.HideTransferInEmpForm)
                    .HasColumnName("HIDE_TRANSFER_IN_EMP_FORM")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.HouseRent)
                    .HasColumnName("HOUSE_RENT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrConfigNo)
                    .HasColumnName("HR_CONFIG_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrEmpIdNameStyle)
                    .HasColumnName("HR_EMP_ID_NAME_STYLE")
                    .HasMaxLength(1);

                entity.Property(e => e.HrEmpIdSuffixLen)
                    .HasColumnName("HR_EMP_ID_SUFFIX_LEN")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.HrLeaveHour)
                    .HasColumnName("HR_LEAVE_HOUR")
                    .HasColumnType("decimal(4, 2)");

                entity.Property(e => e.HrNofWeekend)
                    .HasColumnName("HR_NOF_WEEKEND")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.HrOfficeHour)
                    .HasColumnName("HR_OFFICE_HOUR")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.HrSysGenEmpId)
                    .HasColumnName("HR_SYS_GEN_EMP_ID")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.HrSysGenEmpIdBuPfix)
                    .HasColumnName("HR_SYS_GEN_EMP_ID_BU_PFIX")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.HrSysGenEmpIdPfix)
                    .HasColumnName("HR_SYS_GEN_EMP_ID_PFIX")
                    .HasMaxLength(1);

                entity.Property(e => e.HrWeekStartDay)
                    .HasColumnName("HR_WEEK_START_DAY")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.HrWeekend1)
                    .HasColumnName("HR_WEEKEND1")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.HrWeekend2)
                    .HasColumnName("HR_WEEKEND2")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.IdCardImagePath)
                    .HasColumnName("ID_CARD_IMAGE_PATH")
                    .HasMaxLength(200);

                entity.Property(e => e.IdCardImagePathBack)
                    .HasColumnName("ID_CARD_IMAGE_PATH_BACK")
                    .HasMaxLength(200);

                entity.Property(e => e.IsFromPreviousMon)
                    .HasColumnName("IS_FROM_PREVIOUS_MON")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.JobLocationFlg)
                    .HasColumnName("JOB_LOCATION_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LeaveApplicationToFlg)
                    .HasColumnName("LEAVE_APPLICATION_TO_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LeaveRound)
                    .HasColumnName("LEAVE_ROUND")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaxServiceYear)
                    .HasColumnName("MAX_SERVICE_YEAR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MedAllow)
                    .HasColumnName("MED_ALLOW")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MovementAsOutTime)
                    .HasColumnName("MOVEMENT_AS_OUT_TIME")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.NoImage)
                    .HasColumnName("NO_IMAGE")
                    .HasColumnType("image");

                entity.Property(e => e.NoOfDayLessProcessForOt)
                    .HasColumnName("NO_OF_DAY_LESS_PROCESS_FOR_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OgNo)
                    .HasColumnName("OG_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.OtForOnlyEmpPayrollFlg)
                    .HasColumnName("OT_FOR_ONLY_EMP_PAYROLL_FLG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtRate)
                    .HasColumnName("OT_RATE")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtRatePerHrRound)
                    .HasColumnName("OT_RATE_PER_HR_ROUND")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtherAllow)
                    .HasColumnName("OTHER_ALLOW")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PaymentVtypeNo)
                    .HasColumnName("PAYMENT_VTYPE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PieceRateSalary)
                    .HasColumnName("PIECE_RATE_SALARY")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PresentAtFutureDatetime)
                    .HasColumnName("PRESENT_AT_FUTURE_datetime")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.ProcessingDayEnd)
                    .HasColumnName("PROCESSING_DAY_END")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ProcessingDayStart)
                    .HasColumnName("PROCESSING_DAY_START")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ProvisionVtypeNo)
                    .HasColumnName("PROVISION_VTYPE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PrvsIncrmntElement1)
                    .HasColumnName("PRVS_INCRMNT_ELEMENT1")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PrvsIncrmntElement2)
                    .HasColumnName("PRVS_INCRMNT_ELEMENT2")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ReportDescDisplayFlag)
                    .HasColumnName("REPORT_DESC_DISPLAY_FLAG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.RoundAmt)
                    .HasColumnName("ROUND_AMT")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.SalDivFacProcessStartDatetime)
                    .HasColumnName("SAL_DIV_FAC_PROCESS_START_datetime")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SalReportTitle)
                    .HasColumnName("SAL_REPORT_TITLE")
                    .HasMaxLength(300);

                entity.Property(e => e.SalaryElementShowBy)
                    .HasColumnName("SALARY_ELEMENT_SHOW_BY")
                    .HasMaxLength(200);

                entity.Property(e => e.SalaryHour)
                    .HasColumnName("SALARY_HOUR")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.SalaryJobtitleShowBy)
                    .HasColumnName("SALARY_JOBTITLE_SHOW_BY")
                    .HasMaxLength(20);

                entity.Property(e => e.ServiceEndAge)
                    .HasColumnName("SERVICE_END_AGE")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCandidatetimeImgExt)
                    .HasColumnName("SS_CANDIdatetime_IMG_EXT")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.SsCandidatetimeImgFilePath)
                    .HasColumnName("SS_CANDIdatetime_IMG_FILE_PATH")
                    .HasMaxLength(1000);

                entity.Property(e => e.SsCandidatetimeImgMaxSizeKb)
                    .HasColumnName("SS_CANDIdatetime_IMG_MAX_SIZE_KB")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsEmpImgType)
                    .HasColumnName("SS_EMP_IMG_TYPE")
                    .HasMaxLength(4);

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsPathEmpImg)
                    .HasColumnName("SS_PATH_EMP_IMG")
                    .HasMaxLength(300);

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsUserDisplayFmt)
                    .HasColumnName("SS_USER_DISPLAY_FMT")
                    .HasMaxLength(10);

                entity.Property(e => e.TrDr)
                    .HasColumnName("TR_DR")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.UseAttendanceProcess)
                    .HasColumnName("USE_ATTENDANCE_PROCESS")
                    .HasColumnType("decimal(18, 0)")
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<HrDaypol>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_DAYPOL");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DayName)
                    .HasColumnName("DAY_NAME")
                    .HasMaxLength(10);

                entity.Property(e => e.DayPolNo)
                    .HasColumnName("DAY_POL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(100);

                entity.Property(e => e.JobgroupNo)
                    .HasColumnName("JOBGROUP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoblocNo)
                    .HasColumnName("JOBLOC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.MasterPolNo)
                    .HasColumnName("MASTER_POL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.OffDay)
                    .HasColumnName("OFF_DAY")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PolType)
                    .HasColumnName("POL_TYPE")
                    .HasMaxLength(1);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrDaypolMaster>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_DAYPOL_MASTER");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(250);

                entity.Property(e => e.MasterPolNo)
                    .HasColumnName("MASTER_POL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PolName)
                    .HasColumnName("POL_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrDaypoldtl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_DAYPOLDTL");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DayPolNo)
                    .HasColumnName("DAY_POL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ShiftNo)
                    .HasColumnName("SHIFT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrDependBill>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_DEPEND_BILL");

                entity.Property(e => e.BillId)
                    .HasColumnName("BILL_ID")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DependBillNo)
                    .HasColumnName("DEPEND_BILL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ProcessBy)
                    .HasColumnName("PROCESS_BY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ProcessDatetime)
                    .HasColumnName("PROCESS_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProcessMonth)
                    .HasColumnName("PROCESS_MONTH")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrDependBillMonthdtl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_DEPEND_BILL_MONTHDTL");

                entity.Property(e => e.Amount)
                    .HasColumnName("AMOUNT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DependBillMonthdtlNo)
                    .HasColumnName("DEPEND_BILL_MONTHDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DependBilldtlNo)
                    .HasColumnName("DEPEND_BILLDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ProcessEndDatetime)
                    .HasColumnName("PROCESS_END_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SalarydtlNo)
                    .HasColumnName("SALARYDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalstartDatetime)
                    .HasColumnName("SALSTART_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrDependBilldtl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_DEPEND_BILLDTL");

                entity.Property(e => e.BillId)
                    .HasColumnName("BILL_ID")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Consucative)
                    .HasColumnName("CONSUCATIVE")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DeductAmt)
                    .HasColumnName("DEDUCT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DependBillNo)
                    .HasColumnName("DEPEND_BILL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DependBilldtlNo)
                    .HasColumnName("DEPEND_BILLDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DependElementNo)
                    .HasColumnName("DEPEND_ELEMENT_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.NetPayAmt)
                    .HasColumnName("NET_PAY_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.NoOfOccurance)
                    .HasColumnName("NO_OF_OCCURANCE")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.NoOfSalOccur)
                    .HasColumnName("NO_OF_SAL_OCCUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PaidFlag)
                    .HasColumnName("PAID_FLAG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PayBy)
                    .HasColumnName("PAY_BY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PayDatetime)
                    .HasColumnName("PAY_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PayElementNo)
                    .HasColumnName("PAY_ELEMENT_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PayableAmt)
                    .HasColumnName("PAYABLE_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ProcessDurationMonth)
                    .HasColumnName("PROCESS_DURATION_MONTH")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ProcessEndDatetime)
                    .HasColumnName("PROCESS_END_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProvisionMonth)
                    .HasColumnName("PROVISION_MONTH")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrDependElementpol>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_DEPEND_ELEMENTPOL");

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.AmtDay)
                    .HasColumnName("AMT_DAY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.AmtDayOn)
                    .HasColumnName("AMT_DAY_ON")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Conjucative)
                    .HasColumnName("CONJUCATIVE")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.DeductElementNo)
                    .HasColumnName("DEDUCT_ELEMENT_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DependNo)
                    .HasColumnName("DEPEND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EffectiveDatetime)
                    .HasColumnName("EFFECTIVE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.FixedAmt)
                    .HasColumnName("FIXED_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.MaxAmt)
                    .HasColumnName("MAX_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.MultiplyBy)
                    .HasColumnName("MULTIPLY_BY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.NoOfOccurance)
                    .HasColumnName("NO_OF_OCCURANCE")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.Per)
                    .HasColumnName("PER")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.Peron)
                    .HasColumnName("PERON")
                    .HasMaxLength(1);

                entity.Property(e => e.ProcessDependOn)
                    .HasColumnName("PROCESS_DEPEND_ON")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ProcessDurationMonth)
                    .HasColumnName("PROCESS_DURATION_MONTH")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.ProvisionMonth)
                    .HasColumnName("PROVISION_MONTH")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.SameAsElementAmt)
                    .HasColumnName("SAME_AS_ELEMENT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrDurationpol>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_DURATIONPOL");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(100);

                entity.Property(e => e.DurationCount)
                    .HasColumnName("DURATION_COUNT")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.DurationPolNo)
                    .HasColumnName("DURATION_POL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JobgroupNo)
                    .HasColumnName("JOBGROUP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoblocNo)
                    .HasColumnName("JOBLOC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.MasterPolNo)
                    .HasColumnName("MASTER_POL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.OffDay)
                    .HasColumnName("OFF_DAY")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PolType)
                    .HasColumnName("POL_TYPE")
                    .HasMaxLength(1);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrDurationpolMaster>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_DURATIONPOL_MASTER");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(250);

                entity.Property(e => e.MasterPolNo)
                    .HasColumnName("MASTER_POL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PolName)
                    .HasColumnName("POL_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrDurationpoldtl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_DURATIONPOLDTL");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DurationPolNo)
                    .HasColumnName("DURATION_POL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ShiftNo)
                    .HasColumnName("SHIFT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrDynamicRep>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_DYNAMIC_REP");

                entity.Property(e => e.Col1)
                    .HasColumnName("COL1")
                    .HasMaxLength(200);

                entity.Property(e => e.Col10)
                    .HasColumnName("COL10")
                    .HasMaxLength(200);

                entity.Property(e => e.Col2)
                    .HasColumnName("COL2")
                    .HasMaxLength(200);

                entity.Property(e => e.Col3)
                    .HasColumnName("COL3")
                    .HasMaxLength(200);

                entity.Property(e => e.Col4)
                    .HasColumnName("COL4")
                    .HasMaxLength(200);

                entity.Property(e => e.Col5)
                    .HasColumnName("COL5")
                    .HasMaxLength(200);

                entity.Property(e => e.Col6)
                    .HasColumnName("COL6")
                    .HasMaxLength(200);

                entity.Property(e => e.Col7)
                    .HasColumnName("COL7")
                    .HasMaxLength(200);

                entity.Property(e => e.Col8)
                    .HasColumnName("COL8")
                    .HasMaxLength(200);

                entity.Property(e => e.Col9)
                    .HasColumnName("COL9")
                    .HasMaxLength(200);

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrDynamicRepCol>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_DYNAMIC_REP_COL");

                entity.Property(e => e.Col1)
                    .HasColumnName("COL1")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Col1Name)
                    .HasColumnName("COL1_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.Col1Value)
                    .HasColumnName("COL1_VALUE")
                    .HasMaxLength(200);

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEditorSource>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EDITOR_SOURCE");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SelectedUsers)
                    .HasColumnName("SELECTED_USERS")
                    .HasMaxLength(4000);

                entity.Property(e => e.ShareType)
                    .HasColumnName("SHARE_TYPE")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('M')");

                entity.Property(e => e.SourceId)
                    .HasColumnName("SOURCE_ID")
                    .HasMaxLength(50);

                entity.Property(e => e.SourceName)
                    .HasColumnName("SOURCE_NAME")
                    .HasMaxLength(30);

                entity.Property(e => e.SourceNo)
                    .HasColumnName("SOURCE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SourceType)
                    .HasColumnName("SOURCE_TYPE")
                    .HasMaxLength(30)
                    .HasDefaultValueSql("('V')");

                entity.Property(e => e.SqlTxt).HasColumnName("SQL_TXT");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEditorTemplate>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EDITOR_TEMPLATE");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(500);

                entity.Property(e => e.EditorTemplateId)
                    .HasColumnName("EDITOR_TEMPLATE_ID")
                    .HasMaxLength(50);

                entity.Property(e => e.EditorTemplateNo)
                    .HasColumnName("EDITOR_TEMPLATE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SelectedUsers)
                    .HasColumnName("SELECTED_USERS")
                    .HasMaxLength(4000);

                entity.Property(e => e.ShareType)
                    .HasColumnName("SHARE_TYPE")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength()
                    .HasDefaultValueSql("('M')");

                entity.Property(e => e.SourceNo)
                    .HasColumnName("SOURCE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.TemplateBody)
                    .HasColumnName("TEMPLATE_BODY")
                    .HasMaxLength(200);

                entity.Property(e => e.TemplateName)
                    .HasColumnName("TEMPLATE_NAME")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<HrEduarea>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EDUAREA");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EduareaName)
                    .HasColumnName("EDUAREA_NAME")
                    .HasMaxLength(150);

                entity.Property(e => e.EduareaNo)
                    .HasColumnName("EDUAREA_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEduboard>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EDUBOARD");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EduboardName)
                    .HasColumnName("EDUBOARD_NAME")
                    .HasMaxLength(150);

                entity.Property(e => e.EduboardNo)
                    .HasColumnName("EDUBOARD_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEduinstitute>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EDUINSTITUTE");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EduinstName)
                    .HasColumnName("EDUINST_NAME")
                    .HasMaxLength(150);

                entity.Property(e => e.EduinstNo)
                    .HasColumnName("EDUINST_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEdulevel>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EDULEVEL");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EdulevelName)
                    .HasColumnName("EDULEVEL_NAME")
                    .HasMaxLength(150);

                entity.Property(e => e.EdulevelNo)
                    .HasColumnName("EDULEVEL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEdumajor>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EDUMAJOR");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EdumajorName)
                    .HasColumnName("EDUMAJOR_NAME")
                    .HasMaxLength(150);

                entity.Property(e => e.EdumajorNo)
                    .HasColumnName("EDUMAJOR_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEdupoint>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EDUPOINT");

                entity.Property(e => e.CgpaFrom)
                    .HasColumnName("CGPA_FROM")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.CgpaTo)
                    .HasColumnName("CGPA_TO")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Grade)
                    .HasColumnName("GRADE")
                    .HasMaxLength(30);

                entity.Property(e => e.ResultPoint)
                    .HasColumnName("RESULT_POINT")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.ResultType)
                    .HasColumnName("RESULT_TYPE")
                    .HasMaxLength(1);

                entity.Property(e => e.SlNo)
                    .HasColumnName("SL_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrElement>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ELEMENT");

                entity.Property(e => e.AccNo)
                    .HasColumnName("ACC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Active)
                    .HasColumnName("ACTIVE")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.AmountType)
                    .HasColumnName("AMOUNT_TYPE")
                    .HasMaxLength(1);

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(300);

                entity.Property(e => e.Earning)
                    .HasColumnName("EARNING")
                    .HasMaxLength(2);

                entity.Property(e => e.EffectOnSal)
                    .HasColumnName("EFFECT_ON_SAL")
                    .HasMaxLength(1);

                entity.Property(e => e.EffectType)
                    .HasColumnName("EFFECT_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ElementId)
                    .HasColumnName("ELEMENT_ID")
                    .HasMaxLength(50);

                entity.Property(e => e.ElementName)
                    .HasColumnName("ELEMENT_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ElementNameBangla)
                    .HasColumnName("ELEMENT_NAME_BANGLA")
                    .HasMaxLength(100);

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ExtraPageInSalBill)
                    .HasColumnName("EXTRA_PAGE_IN_SAL_BILL")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.Gross)
                    .HasColumnName("GROSS")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.GroupNo)
                    .HasColumnName("GROUP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HideFromRep)
                    .HasColumnName("HIDE_FROM_REP")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsBasic)
                    .HasColumnName("IS_BASIC")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.IsCompensation)
                    .HasColumnName("IS_COMPENSATION")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.IsTaxable)
                    .HasColumnName("IS_TAXABLE")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.OtherType)
                    .HasColumnName("OTHER_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Recurring)
                    .HasColumnName("RECURRING")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SkipCalculation)
                    .HasColumnName("SKIP_CALCULATION")
                    .HasColumnType("decimal(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SlNo)
                    .HasColumnName("SL_NO")
                    .HasColumnType("decimal(4, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrElementReportGroup>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_ELEMENT_REPORT_GROUP");

                entity.Property(e => e.CanChange)
                    .HasColumnName("CAN_CHANGE")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(500);

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.GroupNo)
                    .HasColumnName("GROUP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP");

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.AppNo)
                    .HasColumnName("APP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ApplicationUserActiveStatus)
                    .HasColumnName("APPLICATION_USER_ACTIVE_STATUS")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.AttachFileName)
                    .HasColumnName("ATTACH_FILE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.AttachFileNo)
                    .HasColumnName("ATTACH_FILE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnSchPolDatetime)
                    .HasColumnName("ATTN_SCH_POL_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.AttnSchedulePol)
                    .HasColumnName("ATTN_SCHEDULE_POL")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.AutoRefreshPendingJob)
                    .HasColumnName("AUTO_REFRESH_PENDING_JOB")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.BloodGrp)
                    .HasColumnName("BLOOD_GRP")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BpDiastolic)
                    .HasColumnName("BP_DIASTOLIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BpSystolic)
                    .HasColumnName("BP_SYSTOLIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CardNo)
                    .HasColumnName("CARD_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.CatNo)
                    .HasColumnName("CAT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ChildBoy)
                    .HasColumnName("CHILD_BOY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChildGirl)
                    .HasColumnName("CHILD_GIRL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChildrenDecimal)
                    .HasColumnName("CHILDREN_decimal")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ContractDatetime)
                    .HasColumnName("CONTRACT_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ContractEmail)
                    .HasColumnName("CONTRACT_EMAIL")
                    .HasMaxLength(150);

                entity.Property(e => e.ContractExpireDatetime)
                    .HasColumnName("CONTRACT_EXPIRE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ContractMobile)
                    .HasColumnName("CONTRACT_MOBILE")
                    .HasMaxLength(150);

                entity.Property(e => e.ContractPersonName)
                    .HasColumnName("CONTRACT_PERSON_NAME")
                    .HasMaxLength(350);

                entity.Property(e => e.ContractPhone)
                    .HasColumnName("CONTRACT_PHONE")
                    .HasMaxLength(150);

                entity.Property(e => e.ContractRelation)
                    .HasColumnName("CONTRACT_RELATION")
                    .HasMaxLength(150);

                entity.Property(e => e.CurNo)
                    .HasColumnName("CUR_NO")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.CurrentBasic)
                    .HasColumnName("CURRENT_BASIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DefaultJobgroupNo)
                    .HasColumnName("DEFAULT_JOBGROUP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DefaultJoblocNo)
                    .HasColumnName("DEFAULT_JOBLOC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DefaultOperationNo)
                    .HasColumnName("DEFAULT_OPERATION_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DefaultProductType)
                    .HasColumnName("DEFAULT_PRODUCT_TYPE")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DefaultShiftNo)
                    .HasColumnName("DEFAULT_SHIFT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.DrivinglcNo)
                    .HasColumnName("DRIVINGLC_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.EmailOfficial)
                    .HasColumnName("EMAIL_OFFICIAL")
                    .HasMaxLength(100);

                entity.Property(e => e.EmailPersonal)
                    .HasColumnName("EMAIL_PERSONAL")
                    .HasMaxLength(100);

                entity.Property(e => e.EmpId)
                    .HasColumnName("EMP_ID")
                    .HasMaxLength(50);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpType)
                    .HasColumnName("EMP_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmployeeGrade)
                    .HasColumnName("EMPLOYEE_GRADE")
                    .HasMaxLength(200);

                entity.Property(e => e.ExtraCurAct)
                    .HasColumnName("EXTRA_CUR_ACT")
                    .HasMaxLength(200);

                entity.Property(e => e.FamilyEarningMember)
                    .HasColumnName("FAMILY_EARNING_MEMBER")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.FamilyMembers)
                    .HasColumnName("FAMILY_MEMBERS")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.FatherName)
                    .HasColumnName("FATHER_NAME")
                    .HasMaxLength(90);

                entity.Property(e => e.FaxNo)
                    .HasColumnName("FAX_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.FileExt)
                    .HasColumnName("FILE_EXT")
                    .HasMaxLength(10);

                entity.Property(e => e.FileNo)
                    .HasColumnName("FILE_NO")
                    .HasMaxLength(30);

                entity.Property(e => e.FileSize)
                    .HasColumnName("FILE_SIZE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Fname)
                    .HasColumnName("FNAME")
                    .HasMaxLength(50);

                entity.Property(e => e.Gender)
                    .HasColumnName("GENDER")
                    .HasMaxLength(1);

                entity.Property(e => e.GrpInsNom)
                    .HasColumnName("GRP_INS_NOM")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.HeightFeet)
                    .HasColumnName("HEIGHT_FEET")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HeightInches)
                    .HasColumnName("HEIGHT_INCHES")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HoldSalary)
                    .HasColumnName("HOLD_SALARY")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.HrType)
                    .HasColumnName("HR_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.HusbandName)
                    .HasColumnName("HUSBAND_NAME")
                    .HasMaxLength(90);

                entity.Property(e => e.IdentificationMark)
                    .HasColumnName("IDENTIFICATION_MARK")
                    .HasMaxLength(100);

                entity.Property(e => e.InsecAftergrace)
                    .HasColumnName("INSEC_AFTERGRACE")
                    .HasColumnType("decimal(4, 0)");

                entity.Property(e => e.InsecBeforegrace)
                    .HasColumnName("INSEC_BEFOREGRACE")
                    .HasColumnType("decimal(4, 0)");

                entity.Property(e => e.InsuranceNo)
                    .HasColumnName("INSURANCE_NO")
                    .HasMaxLength(100);

                entity.Property(e => e.Interest)
                    .HasColumnName("INTEREST")
                    .HasMaxLength(200);

                entity.Property(e => e.IntroducerInComapany)
                    .HasColumnName("INTRODUCER_IN_COMAPANY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.JobConfirmDatetime)
                    .HasColumnName("JOB_CONFIRM_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.JobType)
                    .HasColumnName("JOB_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JobtitleDatetime)
                    .HasColumnName("JOBTITLE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoinDatetime)
                    .HasColumnName("JOIN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastIncrementDatetime)
                    .HasColumnName("LAST_INCREMENT_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastPromotionDatetime)
                    .HasColumnName("LAST_PROMOTION_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastTransferDatetime)
                    .HasColumnName("LAST_TRANSFER_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastUpdatetimeD)
                    .HasColumnName("LAST_UPdatetimeD")
                    .HasColumnType("datetime");

                entity.Property(e => e.LineNo)
                    .HasColumnName("LINE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Lname)
                    .HasColumnName("LNAME")
                    .HasMaxLength(20);

                entity.Property(e => e.MStatus)
                    .HasColumnName("M_STATUS")
                    .HasMaxLength(1);

                entity.Property(e => e.MeritPosition)
                    .HasColumnName("MERIT_POSITION")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.MotherName)
                    .HasColumnName("MOTHER_NAME")
                    .HasMaxLength(90);

                entity.Property(e => e.Nationality)
                    .HasColumnName("NATIONALITY")
                    .HasMaxLength(50);

                entity.Property(e => e.NatureOfWork)
                    .HasColumnName("NATURE_OF_WORK")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.NewLastName)
                    .HasColumnName("NEW_LAST_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.NextIncrDatetime)
                    .HasColumnName("NEXT_INCR_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.NextTransferDatetime)
                    .HasColumnName("NEXT_TRANSFER_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.NidIssuingDatetime)
                    .HasColumnName("NID_ISSUING_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.NidIssuingPlace)
                    .HasColumnName("NID_ISSUING_PLACE")
                    .HasMaxLength(250);

                entity.Property(e => e.NlsAddress1)
                    .HasColumnName("NLS_ADDRESS1")
                    .HasMaxLength(250);

                entity.Property(e => e.NlsAddress2)
                    .HasColumnName("NLS_ADDRESS2")
                    .HasMaxLength(250);

                entity.Property(e => e.NlsFatherName)
                    .HasColumnName("NLS_FATHER_NAME")
                    .HasMaxLength(250);

                entity.Property(e => e.NlsFname)
                    .HasColumnName("NLS_FNAME")
                    .HasMaxLength(200);

                entity.Property(e => e.NlsHusbandName)
                    .HasColumnName("NLS_HUSBAND_NAME")
                    .HasMaxLength(250);

                entity.Property(e => e.NlsLname)
                    .HasColumnName("NLS_LNAME")
                    .HasMaxLength(250);

                entity.Property(e => e.NlsMotherName)
                    .HasColumnName("NLS_MOTHER_NAME")
                    .HasMaxLength(250);

                entity.Property(e => e.NlsShortName)
                    .HasColumnName("NLS_SHORT_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.NlsSurName)
                    .HasColumnName("NLS_SUR_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.Note)
                    .HasColumnName("NOTE")
                    .HasMaxLength(500);

                entity.Property(e => e.OldEmpNo)
                    .HasColumnName("OLD_EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PabxExt)
                    .HasColumnName("PABX_EXT")
                    .HasColumnType("decimal(5, 0)");

                entity.Property(e => e.PassportNo)
                    .HasColumnName("PASSPORT_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.PeAddr1)
                    .HasColumnName("PE_ADDR1")
                    .HasMaxLength(100);

                entity.Property(e => e.PeAddr2)
                    .HasColumnName("PE_ADDR2")
                    .HasMaxLength(100);

                entity.Property(e => e.PeAddr3)
                    .HasColumnName("PE_ADDR3")
                    .HasMaxLength(500);

                entity.Property(e => e.PeAddrCountry)
                    .HasColumnName("PE_ADDR_COUNTRY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PeAddrDist)
                    .HasColumnName("PE_ADDR_DIST")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PeAddrPost)
                    .HasColumnName("PE_ADDR_POST")
                    .HasMaxLength(20);

                entity.Property(e => e.PeCareOfAddr)
                    .HasColumnName("PE_CARE_OF_ADDR")
                    .HasMaxLength(500);

                entity.Property(e => e.PeDivisionNo)
                    .HasColumnName("PE_DIVISION_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PePostOfficeNo)
                    .HasColumnName("PE_POST_OFFICE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PeThanaNo)
                    .HasColumnName("PE_THANA_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PeUnionNo)
                    .HasColumnName("PE_UNION_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PeVillageNo)
                    .HasColumnName("PE_VILLAGE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PfApprove)
                    .HasColumnName("PF_APPROVE")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PfConfirmDatetime)
                    .HasColumnName("PF_CONFIRM_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PfMemberFlg)
                    .HasColumnName("PF_MEMBER_FLG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PfNo)
                    .HasColumnName("PF_NO")
                    .HasMaxLength(100);

                entity.Property(e => e.PhoneHome)
                    .HasColumnName("PHONE_HOME")
                    .HasMaxLength(30);

                entity.Property(e => e.PhoneMobile)
                    .HasColumnName("PHONE_MOBILE")
                    .HasMaxLength(30);

                entity.Property(e => e.PhoneOffice)
                    .HasColumnName("PHONE_OFFICE")
                    .HasMaxLength(30);

                entity.Property(e => e.PieceRateEmp)
                    .HasColumnName("PIECE_RATE_EMP")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PlaceOfBirth)
                    .HasColumnName("PLACE_OF_BIRTH")
                    .HasMaxLength(100);

                entity.Property(e => e.PrAddr1)
                    .HasColumnName("PR_ADDR1")
                    .HasMaxLength(100);

                entity.Property(e => e.PrAddr2)
                    .HasColumnName("PR_ADDR2")
                    .HasMaxLength(100);

                entity.Property(e => e.PrAddr3)
                    .HasColumnName("PR_ADDR3")
                    .HasMaxLength(500);

                entity.Property(e => e.PrAddrCountry)
                    .HasColumnName("PR_ADDR_COUNTRY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PrAddrDist)
                    .HasColumnName("PR_ADDR_DIST")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PrAddrPost)
                    .HasColumnName("PR_ADDR_POST")
                    .HasMaxLength(20);

                entity.Property(e => e.PrCareOfAddr)
                    .HasColumnName("PR_CARE_OF_ADDR")
                    .HasMaxLength(500);

                entity.Property(e => e.PrDivisionNo)
                    .HasColumnName("PR_DIVISION_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PrNlsAddress1)
                    .HasColumnName("PR_NLS_ADDRESS1")
                    .HasMaxLength(250);

                entity.Property(e => e.PrNlsAddress2)
                    .HasColumnName("PR_NLS_ADDRESS2")
                    .HasMaxLength(250);

                entity.Property(e => e.PrPostOfficeNo)
                    .HasColumnName("PR_POST_OFFICE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PrThanaNo)
                    .HasColumnName("PR_THANA_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PrUnionNo)
                    .HasColumnName("PR_UNION_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PrVillageNo)
                    .HasColumnName("PR_VILLAGE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PreferedAddr)
                    .HasColumnName("PREFERED_ADDR")
                    .HasMaxLength(2);

                entity.Property(e => e.PreferredPhone)
                    .HasColumnName("PREFERRED_PHONE")
                    .HasMaxLength(1);

                entity.Property(e => e.PrimaryCompanyNo)
                    .HasColumnName("PRIMARY_COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ProMembership)
                    .HasColumnName("PRO_MEMBERSHIP")
                    .HasMaxLength(200);

                entity.Property(e => e.ProbableTransferDuration)
                    .HasColumnName("PROBABLE_TRANSFER_DURATION")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.ProvMonth)
                    .HasColumnName("PROV_MONTH")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.Pwd)
                    .HasColumnName("PWD")
                    .HasMaxLength(40);

                entity.Property(e => e.Ref1Dept)
                    .HasColumnName("REF1_DEPT")
                    .HasMaxLength(200);

                entity.Property(e => e.Ref1Desig)
                    .HasColumnName("REF1_DESIG")
                    .HasMaxLength(200);

                entity.Property(e => e.Ref1Mobile)
                    .HasColumnName("REF1_MOBILE")
                    .HasMaxLength(100);

                entity.Property(e => e.Ref1Name)
                    .HasColumnName("REF1_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.Ref1Org)
                    .HasColumnName("REF1_ORG")
                    .HasMaxLength(200);

                entity.Property(e => e.Ref1Rel)
                    .HasColumnName("REF1_REL")
                    .HasMaxLength(200);

                entity.Property(e => e.Ref2Dept)
                    .HasColumnName("REF2_DEPT")
                    .HasMaxLength(200);

                entity.Property(e => e.Ref2Desig)
                    .HasColumnName("REF2_DESIG")
                    .HasMaxLength(200);

                entity.Property(e => e.Ref2Mobile)
                    .HasColumnName("REF2_MOBILE")
                    .HasMaxLength(100);

                entity.Property(e => e.Ref2Name)
                    .HasColumnName("REF2_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.Ref2Org)
                    .HasColumnName("REF2_ORG")
                    .HasMaxLength(200);

                entity.Property(e => e.Ref2Rel)
                    .HasColumnName("REF2_REL")
                    .HasMaxLength(200);

                entity.Property(e => e.RelativeInCompany)
                    .HasColumnName("RELATIVE_IN_COMPANY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RelativeRelation)
                    .HasColumnName("RELATIVE_RELATION")
                    .HasMaxLength(100);

                entity.Property(e => e.Religion)
                    .HasColumnName("RELIGION")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ReportingTo)
                    .HasColumnName("REPORTING_TO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ReqdetlNo)
                    .HasColumnName("REQDETL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SaLastLogin)
                    .HasColumnName("SA_LAST_LOGIN")
                    .HasColumnType("datetime");

                entity.Property(e => e.SaPrivType)
                    .HasColumnName("SA_PRIV_TYPE")
                    .HasMaxLength(1);

                entity.Property(e => e.SaPwdModifiedon)
                    .HasColumnName("SA_PWD_MODIFIEDON")
                    .HasColumnType("datetime");

                entity.Property(e => e.SaRoleNo)
                    .HasColumnName("SA_ROLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalFromAccountNo)
                    .HasColumnName("SAL_FROM_ACCOUNT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalaryBankAccNo)
                    .HasColumnName("SALARY_BANK_ACC_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.SalaryBankNo)
                    .HasColumnName("SALARY_BANK_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalaryReportSlNo)
                    .HasColumnName("SALARY_REPORT_SL_NO")
                    .HasColumnType("decimal(5, 0)");

                entity.Property(e => e.SalaryType)
                    .HasColumnName("SALARY_TYPE")
                    .HasMaxLength(1)
                    .HasDefaultValueSql("('D')");

                entity.Property(e => e.Salutation)
                    .HasColumnName("SALUTATION")
                    .HasMaxLength(20);

                entity.Property(e => e.ScaleNo)
                    .HasColumnName("SCALE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SchOffDayNo)
                    .HasColumnName("SCH_OFF_DAY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ServiceDuration)
                    .HasColumnName("SERVICE_DURATION")
                    .HasColumnType("decimal(5, 0)");

                entity.Property(e => e.ServiceDurationType)
                    .HasColumnName("SERVICE_DURATION_TYPE")
                    .HasMaxLength(1);

                entity.Property(e => e.ServiceEndDatetime)
                    .HasColumnName("SERVICE_END_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ServiceExtentionMonth)
                    .HasColumnName("SERVICE_EXTENTION_MONTH")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsNo)
                    .HasColumnName("SS_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsOsIp)
                    .HasColumnName("SS_OS_IP")
                    .HasMaxLength(20);

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.StepNo)
                    .HasColumnName("STEP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SurName)
                    .HasColumnName("SUR_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.TinNo)
                    .HasColumnName("TIN_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.TypeOfAgeProve)
                    .HasColumnName("TYPE_OF_AGE_PROVE")
                    .HasMaxLength(500);

                entity.Property(e => e.UserName)
                    .HasColumnName("USER_NAME")
                    .HasMaxLength(30);

                entity.Property(e => e.WeightKg)
                    .HasColumnName("WEIGHT_KG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.WfApprove)
                    .HasColumnName("WF_APPROVE")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.WfConfirmDatetime)
                    .HasColumnName("WF_CONFIRM_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.WfMemberFlg)
                    .HasColumnName("WF_MEMBER_FLG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.WfNo)
                    .HasColumnName("WF_NO")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmpAdvanceSearch>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_ADVANCE_SEARCH");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ConditionDetails)
                    .HasColumnName("CONDITION_DETAILS")
                    .HasMaxLength(4000);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SearchName)
                    .HasColumnName("SEARCH_NAME")
                    .HasMaxLength(500);

                entity.Property(e => e.SlNo)
                    .HasColumnName("SL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SummaryDetails)
                    .HasColumnName("SUMMARY_DETAILS")
                    .HasMaxLength(4000);

                entity.Property(e => e.TemplateType)
                    .HasColumnName("TEMPLATE_TYPE")
                    .HasColumnType("decimal(1, 0)");
            });

            modelBuilder.Entity<HrEmpAlternateTaxsal>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_ALTERNATE_TAXSAL");

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.AlternateTaxsalId)
                    .HasColumnName("ALTERNATE_TAXSAL_ID")
                    .HasMaxLength(30);

                entity.Property(e => e.AlternateTaxsalNo)
                    .HasColumnName("ALTERNATE_TAXSAL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(300);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.MasterBaFlag)
                    .HasColumnName("MASTER_BA_FLAG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmpAttndtlTab>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_ATTNDTL_TAB");

                entity.Property(e => e.ActOthOt)
                    .HasColumnName("ACT_OTH_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ActOthOtTfmt)
                    .HasColumnName("ACT_OTH_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.ActWorkHour)
                    .HasColumnName("ACT_WORK_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.AtnShift)
                    .HasColumnName("ATN_SHIFT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnSchedulePol)
                    .HasColumnName("ATTN_SCHEDULE_POL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BuName)
                    .HasColumnName("BU_NAME")
                    .HasMaxLength(4000);

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BuNoAll)
                    .HasColumnName("BU_NO_ALL")
                    .HasMaxLength(200);

                entity.Property(e => e.CalDt)
                    .HasColumnName("CAL_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DdStatus)
                    .HasColumnName("DD_STATUS")
                    .HasMaxLength(1);

                entity.Property(e => e.Department)
                    .HasColumnName("DEPARTMENT")
                    .HasMaxLength(4000);

                entity.Property(e => e.DepartmentNo)
                    .HasColumnName("DEPARTMENT_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyExitStat)
                    .HasColumnName("EARLY_EXIT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EarlyexitReason)
                    .HasColumnName("EARLYEXIT_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.EmpId)
                    .HasColumnName("EMP_ID")
                    .HasMaxLength(50);

                entity.Property(e => e.EmpName)
                    .HasColumnName("EMP_NAME")
                    .HasMaxLength(92);

                entity.Property(e => e.EmpNameId)
                    .HasColumnName("EMP_NAME_ID")
                    .HasMaxLength(145);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpType)
                    .HasColumnName("EMP_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpTypeName)
                    .HasColumnName("EMP_TYPE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ExtraOtMultiply)
                    .HasColumnName("EXTRA_OT_MULTIPLY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.GenInTime)
                    .HasColumnName("GEN_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenInTimeTmft)
                    .HasColumnName("GEN_IN_TIME_TMFT")
                    .HasMaxLength(75);

                entity.Property(e => e.GenOutTime)
                    .HasColumnName("GEN_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenOutTimeTfmt)
                    .HasColumnName("GEN_OUT_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.Gender)
                    .HasColumnName("GENDER")
                    .HasMaxLength(1);

                entity.Property(e => e.HrType)
                    .HasColumnName("HR_TYPE")
                    .HasMaxLength(100);

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.InTimeTfmt)
                    .HasColumnName("IN_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.JobType)
                    .HasColumnName("JOB_TYPE")
                    .HasMaxLength(100);

                entity.Property(e => e.Jobtitle)
                    .HasColumnName("JOBTITLE")
                    .HasMaxLength(50);

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoinDatetime)
                    .HasColumnName("JOIN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryReason)
                    .HasColumnName("LATEENTRY_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasMaxLength(4000);

                entity.Property(e => e.OtMultiply)
                    .HasColumnName("OT_MULTIPLY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtStat)
                    .HasColumnName("OT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtTfmt)
                    .HasColumnName("OT_TFMT")
                    .HasMaxLength(4000);

                entity.Property(e => e.OthOt)
                    .HasColumnName("OTH_OT")
                    .HasMaxLength(4000);

                entity.Property(e => e.OthOtTfmt)
                    .HasColumnName("OTH_OT_TFMT")
                    .HasMaxLength(4000);

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.OutTimeTfmt)
                    .HasColumnName("OUT_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.PeriodNo)
                    .HasColumnName("PERIOD_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Prefix)
                    .HasColumnName("PREFIX")
                    .HasMaxLength(4000);

                entity.Property(e => e.RemarksManualAtten)
                    .HasColumnName("REMARKS_MANUAL_ATTEN")
                    .HasMaxLength(500);

                entity.Property(e => e.ReportingTo)
                    .HasColumnName("REPORTING_TO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalReportSlNo)
                    .HasColumnName("SAL_REPORT_SL_NO")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.SchDatetime)
                    .HasColumnName("SCH_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchInTime)
                    .HasColumnName("SCH_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchOutTime)
                    .HasColumnName("SCH_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchShift)
                    .HasColumnName("SCH_SHIFT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Section)
                    .HasColumnName("SECTION")
                    .HasMaxLength(100);

                entity.Property(e => e.SectionNo)
                    .HasColumnName("SECTION_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ShiftId)
                    .HasColumnName("SHIFT_ID")
                    .HasMaxLength(169);

                entity.Property(e => e.ShiftWorkHour)
                    .HasColumnName("SHIFT_WORK_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasMaxLength(4000);

                entity.Property(e => e.TotOt)
                    .HasColumnName("TOT_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotOtTfmt)
                    .HasColumnName("TOT_OT_TFMT")
                    .HasMaxLength(50);

                entity.Property(e => e.UnadjustedOthOt)
                    .HasColumnName("UNADJUSTED_OTH_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UnadjustedTotOt)
                    .HasColumnName("UNADJUSTED_TOT_OT")
                    .HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<HrEmpAttndtlTmp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_ATTNDTL_TMP");

                entity.Property(e => e.ActWorkHour)
                    .HasColumnName("ACT_WORK_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ActWorkHourTfmt)
                    .HasColumnName("ACT_WORK_HOUR_TFMT")
                    .HasMaxLength(8);

                entity.Property(e => e.AtnShift)
                    .HasColumnName("ATN_SHIFT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BuName)
                    .HasColumnName("BU_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CalDt)
                    .HasColumnName("CAL_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DdStatus)
                    .HasColumnName("DD_STATUS")
                    .HasMaxLength(1);

                entity.Property(e => e.Department)
                    .HasColumnName("DEPARTMENT")
                    .HasMaxLength(100);

                entity.Property(e => e.DepartmentNo)
                    .HasColumnName("DEPARTMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EarlyExitStat)
                    .HasColumnName("EARLY_EXIT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EmpNameId)
                    .HasColumnName("EMP_NAME_ID")
                    .HasMaxLength(145);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.HrType)
                    .HasColumnName("HR_TYPE")
                    .HasMaxLength(50);

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.JobType)
                    .HasColumnName("JOB_TYPE")
                    .HasMaxLength(50);

                entity.Property(e => e.Jobtitle)
                    .HasColumnName("JOBTITLE")
                    .HasMaxLength(50);

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.MovHr)
                    .HasColumnName("MOV_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.Prefix)
                    .HasColumnName("PREFIX")
                    .HasMaxLength(4000);

                entity.Property(e => e.SalReportSlNo)
                    .HasColumnName("SAL_REPORT_SL_NO")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.SchDatetime)
                    .HasColumnName("SCH_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchInTime)
                    .HasColumnName("SCH_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchOutTime)
                    .HasColumnName("SCH_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchShift)
                    .HasColumnName("SCH_SHIFT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Section)
                    .HasColumnName("SECTION")
                    .HasMaxLength(100);

                entity.Property(e => e.SectionNo)
                    .HasColumnName("SECTION_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<HrEmpAttndtlTmpRep>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_ATTNDTL_TMP_REP");

                entity.Property(e => e.AtnShift)
                    .HasColumnName("ATN_SHIFT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BuName)
                    .HasColumnName("BU_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CalDt)
                    .HasColumnName("CAL_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ContFlg)
                    .HasColumnName("CONT_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.DdStatus)
                    .HasColumnName("DD_STATUS")
                    .HasMaxLength(1);

                entity.Property(e => e.Department)
                    .HasColumnName("DEPARTMENT")
                    .HasMaxLength(100);

                entity.Property(e => e.DepartmentNo)
                    .HasColumnName("DEPARTMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Early)
                    .HasColumnName("EARLY")
                    .HasMaxLength(10);

                entity.Property(e => e.EarlyExitStat)
                    .HasColumnName("EARLY_EXIT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EmpId)
                    .HasColumnName("EMP_ID")
                    .HasMaxLength(100);

                entity.Property(e => e.EmpName)
                    .HasColumnName("EMP_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.EmpNameId)
                    .HasColumnName("EMP_NAME_ID")
                    .HasMaxLength(145);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.GenInTime)
                    .HasColumnName("GEN_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenOt)
                    .HasColumnName("GEN_OT")
                    .HasMaxLength(20);

                entity.Property(e => e.GenOtC)
                    .HasColumnName("GEN_OT_C")
                    .HasMaxLength(5);

                entity.Property(e => e.GenOutTime)
                    .HasColumnName("GEN_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenOutTimeC)
                    .HasColumnName("GEN_OUT_TIME_C")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenOutTimeTfmt)
                    .HasColumnName("GEN_OUT_TIME_TFMT")
                    .HasMaxLength(20);

                entity.Property(e => e.HrType)
                    .HasColumnName("HR_TYPE")
                    .HasMaxLength(50);

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasMaxLength(50);

                entity.Property(e => e.InTimeTfmt)
                    .HasColumnName("IN_TIME_TFMT")
                    .HasMaxLength(20);

                entity.Property(e => e.JobType)
                    .HasColumnName("JOB_TYPE")
                    .HasMaxLength(50);

                entity.Property(e => e.Jobtitle)
                    .HasColumnName("JOBTITLE")
                    .HasMaxLength(50);

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Late)
                    .HasColumnName("LATE")
                    .HasMaxLength(10);

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LineName)
                    .HasColumnName("LINE_NAME")
                    .HasMaxLength(150);

                entity.Property(e => e.MovementHour)
                    .HasColumnName("MOVEMENT_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasMaxLength(500);

                entity.Property(e => e.OtTfmt)
                    .HasColumnName("OT_TFMT")
                    .HasMaxLength(20);

                entity.Property(e => e.OthOt)
                    .HasColumnName("OTH_OT")
                    .HasMaxLength(20);

                entity.Property(e => e.OthOtTfmt)
                    .HasColumnName("OTH_OT_TFMT")
                    .HasMaxLength(20);

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasMaxLength(50);

                entity.Property(e => e.OutTimeTfmt)
                    .HasColumnName("OUT_TIME_TFMT")
                    .HasMaxLength(20);

                entity.Property(e => e.Prefix)
                    .HasColumnName("PREFIX")
                    .HasMaxLength(30);

                entity.Property(e => e.Remarks)
                    .HasColumnName("REMARKS")
                    .HasMaxLength(100);

                entity.Property(e => e.SalReportSlNo)
                    .HasColumnName("SAL_REPORT_SL_NO")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.ScaleNo)
                    .HasColumnName("SCALE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SchDatetime)
                    .HasColumnName("SCH_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchInTime)
                    .HasColumnName("SCH_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchOutTime)
                    .HasColumnName("SCH_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchShift)
                    .HasColumnName("SCH_SHIFT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Section)
                    .HasColumnName("SECTION")
                    .HasMaxLength(100);

                entity.Property(e => e.SectionNo)
                    .HasColumnName("SECTION_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Shift)
                    .HasColumnName("SHIFT")
                    .HasMaxLength(100);

                entity.Property(e => e.SlNo)
                    .HasColumnName("SL_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasMaxLength(50);

                entity.Property(e => e.TotOt)
                    .HasColumnName("TOT_OT")
                    .HasMaxLength(20);

                entity.Property(e => e.TotOtTfmt)
                    .HasColumnName("TOT_OT_TFMT")
                    .HasMaxLength(20);

                entity.Property(e => e.Wo)
                    .HasColumnName("WO")
                    .HasMaxLength(10);

                entity.Property(e => e.WorkHour)
                    .HasColumnName("WORK_HOUR")
                    .HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<HrEmpAttndtlVTmp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_ATTNDTL_V_TMP");

                entity.Property(e => e.ActWorkHour)
                    .HasColumnName("ACT_WORK_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ActWorkHourTfmt)
                    .HasColumnName("ACT_WORK_HOUR_TFMT")
                    .HasMaxLength(8);

                entity.Property(e => e.ActWorkIncludeMov)
                    .HasColumnName("ACT_WORK_INCLUDE_MOV")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ActWorkIncludeMovTfmt)
                    .HasColumnName("ACT_WORK_INCLUDE_MOV_TFMT")
                    .HasMaxLength(8);

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtnShift)
                    .HasColumnName("ATN_SHIFT")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AtndNo)
                    .HasColumnName("ATND_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnSchedulePol)
                    .HasColumnName("ATTN_SCHEDULE_POL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Basic)
                    .HasColumnName("BASIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BreakTimeEnd)
                    .HasColumnName("BREAK_TIME_END")
                    .HasColumnType("datetime");

                entity.Property(e => e.BreakTimeStart)
                    .HasColumnName("BREAK_TIME_START")
                    .HasColumnType("datetime");

                entity.Property(e => e.BuName)
                    .HasColumnName("BU_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNameParent)
                    .HasColumnName("BU_NAME_PARENT")
                    .HasMaxLength(100);

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BuNoAll)
                    .HasColumnName("BU_NO_ALL")
                    .HasMaxLength(200);

                entity.Property(e => e.CalDt)
                    .HasColumnName("CAL_DT")
                    .HasColumnType("datetime");

                entity.Property(e => e.CalcCountHr)
                    .HasColumnName("CALC_COUNT_HR")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.CashOtAmt)
                    .HasColumnName("CASH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CashOthOtAmt)
                    .HasColumnName("CASH_OTH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeOtAmt)
                    .HasColumnName("CHEQUE_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeOthOtAmt)
                    .HasColumnName("CHEQUE_OTH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DdStatus)
                    .HasColumnName("DD_STATUS")
                    .HasMaxLength(1);

                entity.Property(e => e.DeductMin)
                    .HasColumnName("DEDUCT_MIN")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Department)
                    .HasColumnName("DEPARTMENT")
                    .HasMaxLength(100);

                entity.Property(e => e.DepartmentNo)
                    .HasColumnName("DEPARTMENT_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyExitStat)
                    .HasColumnName("EARLY_EXIT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyOt)
                    .HasColumnName("EARLY_OT")
                    .HasMaxLength(4000);

                entity.Property(e => e.EarlyOtFlag)
                    .HasColumnName("EARLY_OT_FLAG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EarlyOtStartTime)
                    .HasColumnName("EARLY_OT_START_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.EarlyOtTfmt)
                    .HasColumnName("EARLY_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.EarlyexitApproved)
                    .HasColumnName("EARLYEXIT_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EarlyexitReason)
                    .HasColumnName("EARLYEXIT_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.EmpId)
                    .HasColumnName("EMP_ID")
                    .HasMaxLength(80);

                entity.Property(e => e.EmpName)
                    .HasColumnName("EMP_NAME")
                    .HasMaxLength(92);

                entity.Property(e => e.EmpNameId)
                    .HasColumnName("EMP_NAME_ID")
                    .HasMaxLength(175);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpType)
                    .HasColumnName("EMP_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpTypeName)
                    .HasColumnName("EMP_TYPE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.ExtraOtMultiply)
                    .HasColumnName("EXTRA_OT_MULTIPLY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.GenInTime)
                    .HasColumnName("GEN_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenInTimeTfmt)
                    .HasColumnName("GEN_IN_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.GenOutTime)
                    .HasColumnName("GEN_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GenOutTimeTfmt)
                    .HasColumnName("GEN_OUT_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.Gender)
                    .HasColumnName("GENDER")
                    .HasMaxLength(1);

                entity.Property(e => e.Gross)
                    .HasColumnName("GROSS")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.HrType)
                    .HasColumnName("HR_TYPE")
                    .HasMaxLength(100);

                entity.Property(e => e.HrTypeNo)
                    .HasColumnName("HR_TYPE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.InTime)
                    .HasColumnName("IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.InTimeTfmt)
                    .HasColumnName("IN_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.IsOtFromFirst)
                    .HasColumnName("IS_OT_FROM_FIRST")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.JobType)
                    .HasColumnName("JOB_TYPE")
                    .HasMaxLength(100);

                entity.Property(e => e.JobTypeNo)
                    .HasColumnName("JOB_TYPE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoblocName)
                    .HasColumnName("JOBLOC_NAME")
                    .HasMaxLength(200);

                entity.Property(e => e.JoblocNo)
                    .HasColumnName("JOBLOC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Jobtitle)
                    .HasColumnName("JOBTITLE")
                    .HasMaxLength(50);

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoinDatetime)
                    .HasColumnName("JOIN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LateStat)
                    .HasColumnName("LATE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryApproved)
                    .HasColumnName("LATEENTRY_APPROVED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.LateentryReason)
                    .HasColumnName("LATEENTRY_REASON")
                    .HasMaxLength(250);

                entity.Property(e => e.LineName)
                    .HasColumnName("LINE_NAME")
                    .HasMaxLength(50);

                entity.Property(e => e.LineNo)
                    .HasColumnName("LINE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaxExtOtSal)
                    .HasColumnName("MAX_EXT_OT_SAL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaxOtSal)
                    .HasColumnName("MAX_OT_SAL")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Maxexotdr)
                    .HasColumnName("MAXEXOTDR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Maxotdr)
                    .HasColumnName("MAXOTDR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MovHr)
                    .HasColumnName("MOV_HR")
                    .HasMaxLength(4000);

                entity.Property(e => e.MovHrTfmt)
                    .HasColumnName("MOV_HR_TFMT")
                    .HasMaxLength(4000);

                entity.Property(e => e.OfficeHour)
                    .HasColumnName("OFFICE_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OrginEmpId)
                    .HasColumnName("ORGIN_EMP_ID")
                    .HasMaxLength(80);

                entity.Property(e => e.Ot)
                    .HasColumnName("OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtAmt)
                    .HasColumnName("OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtMultiply)
                    .HasColumnName("OT_MULTIPLY")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtPer)
                    .HasColumnName("OT_PER")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.OtPeron)
                    .HasColumnName("OT_PERON")
                    .HasMaxLength(1);

                entity.Property(e => e.OtRateFlg)
                    .HasColumnName("OT_RATE_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.OtRateFormula)
                    .HasColumnName("OT_RATE_FORMULA")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtRatePerHr)
                    .HasColumnName("OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtStat)
                    .HasColumnName("OT_STAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OtTfmt)
                    .HasColumnName("OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.OthOt)
                    .HasColumnName("OTH_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OthOtAmt)
                    .HasColumnName("OTH_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OthOtRatePerHr)
                    .HasColumnName("OTH_OT_RATE_PER_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.OthOtTfmt)
                    .HasColumnName("OTH_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.OutTime)
                    .HasColumnName("OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.OutTimeTfmt)
                    .HasColumnName("OUT_TIME_TFMT")
                    .HasMaxLength(75);

                entity.Property(e => e.PaidDatetime)
                    .HasColumnName("PAID_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaidFlg)
                    .HasColumnName("PAID_FLG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PerDayBasic)
                    .HasColumnName("PER_DAY_BASIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PeriodNo)
                    .HasColumnName("PERIOD_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Prefix)
                    .HasColumnName("PREFIX")
                    .HasMaxLength(46);

                entity.Property(e => e.RemarksManualAtten)
                    .HasColumnName("REMARKS_MANUAL_ATTEN")
                    .HasMaxLength(500);

                entity.Property(e => e.ReportingTo)
                    .HasColumnName("REPORTING_TO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.RoundMin)
                    .HasColumnName("ROUND_MIN")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RuleJobtitleNo)
                    .HasColumnName("RULE_JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalReportSlNo)
                    .HasColumnName("SAL_REPORT_SL_NO")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.ScaleNo)
                    .HasColumnName("SCALE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SchDatetime)
                    .HasColumnName("SCH_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchInTime)
                    .HasColumnName("SCH_IN_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchOffHr)
                    .HasColumnName("SCH_OFF_HR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SchOutTime)
                    .HasColumnName("SCH_OUT_TIME")
                    .HasColumnType("datetime");

                entity.Property(e => e.SchShift)
                    .HasColumnName("SCH_SHIFT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Section)
                    .HasColumnName("SECTION")
                    .HasMaxLength(100);

                entity.Property(e => e.SectionNo)
                    .HasColumnName("SECTION_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ShiftId)
                    .HasColumnName("SHIFT_ID")
                    .HasMaxLength(169);

                entity.Property(e => e.ShiftName)
                    .HasColumnName("SHIFT_NAME")
                    .HasMaxLength(15);

                entity.Property(e => e.ShiftWorkHour)
                    .HasColumnName("SHIFT_WORK_HOUR")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasMaxLength(53);

                entity.Property(e => e.TotOt)
                    .HasColumnName("TOT_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotOtAmt)
                    .HasColumnName("TOT_OT_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotOtTfmt)
                    .HasColumnName("TOT_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.UnadjustedOthOt)
                    .HasColumnName("UNADJUSTED_OTH_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UnadjustedTotOt)
                    .HasColumnName("UNADJUSTED_TOT_OT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UnadjustedTotOtTfmt)
                    .HasColumnName("UNADJUSTED_TOT_OT_TFMT")
                    .HasMaxLength(5);

                entity.Property(e => e.VisibleInReport)
                    .HasColumnName("VISIBLE_IN_REPORT")
                    .HasColumnType("decimal(1, 0)");
            });

            modelBuilder.Entity<HrEmpAutoinactive>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_AUTOINACTIVE");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpAutoInactiveId)
                    .HasColumnName("EMP_AUTO_INACTIVE_ID")
                    .HasMaxLength(50);

                entity.Property(e => e.EmpAutoInactiveNo)
                    .HasColumnName("EMP_AUTO_INACTIVE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.InactiveByEmp)
                    .HasColumnName("INACTIVE_BY_EMP")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.InactiveDatetime)
                    .HasColumnName("INACTIVE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmpDoc>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_DOC");

                entity.Property(e => e.AppNo)
                    .HasColumnName("APP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.AttachFileName)
                    .HasColumnName("ATTACH_FILE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.AttachFileNo)
                    .HasColumnName("ATTACH_FILE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DocBody)
                    .HasColumnName("DOC_BODY")
                    .HasMaxLength(200);

                entity.Property(e => e.DocFooter)
                    .HasColumnName("DOC_FOOTER")
                    .HasMaxLength(500);

                entity.Property(e => e.DocHeader)
                    .HasColumnName("DOC_HEADER")
                    .HasMaxLength(500);

                entity.Property(e => e.DocNo)
                    .HasColumnName("DOC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DoctypeNo)
                    .HasColumnName("DOCTYPE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.FileExt)
                    .HasColumnName("FILE_EXT")
                    .HasMaxLength(10);

                entity.Property(e => e.FileSize)
                    .HasColumnName("FILE_SIZE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoinDatetime)
                    .HasColumnName("JOIN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastApplicationDatetime)
                    .HasColumnName("LAST_APPLICATION_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastInterviewDatetime)
                    .HasColumnName("LAST_INTERVIEW_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrintDatetime)
                    .HasColumnName("PRINT_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RefNo)
                    .HasColumnName("REF_NO")
                    .HasMaxLength(20);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.Test)
                    .HasColumnName("TEST")
                    .HasMaxLength(4000);

                entity.Property(e => e.Test2)
                    .HasColumnName("TEST2")
                    .HasMaxLength(4000);

                entity.Property(e => e.TraineeAmount)
                    .HasColumnName("TRAINEE_AMOUNT")
                    .HasColumnType("decimal(12, 2)");
            });

            modelBuilder.Entity<HrEmpDoctype>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_DOCTYPE");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DocName)
                    .HasColumnName("DOC_NAME")
                    .HasMaxLength(50);

                entity.Property(e => e.DoctypeNo)
                    .HasColumnName("DOCTYPE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpType)
                    .HasColumnName("EMP_TYPE")
                    .HasMaxLength(20);

                entity.Property(e => e.HrDepartmentAlias)
                    .HasColumnName("HR_DEPARTMENT_ALIAS")
                    .HasMaxLength(20);

                entity.Property(e => e.RepAlias)
                    .HasColumnName("REP_ALIAS")
                    .HasMaxLength(20);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SubmenuNo)
                    .HasColumnName("SUBMENU_NO")
                    .HasColumnType("decimal(30, 0)");
            });

            modelBuilder.Entity<HrEmpJd>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_JD");

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(500);

                entity.Property(e => e.EffectiveFromDatetime)
                    .HasColumnName("EFFECTIVE_FROM_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmpJdId)
                    .HasColumnName("EMP_JD_ID")
                    .HasMaxLength(50);

                entity.Property(e => e.EmpJdNo)
                    .HasColumnName("EMP_JD_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SlNo)
                    .HasColumnName("SL_NO")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.TaskNo)
                    .HasColumnName("TASK_NO")
                    .HasColumnType("decimal(30, 0)");
            });

            modelBuilder.Entity<HrEmpJdDtl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_JD_DTL");

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(500);

                entity.Property(e => e.EffectiveFromDatetime)
                    .HasColumnName("EFFECTIVE_FROM_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmpJdDtlNo)
                    .HasColumnName("EMP_JD_DTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpJdMstNo)
                    .HasColumnName("EMP_JD_MST_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SlNo)
                    .HasColumnName("SL_NO")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.TaskNo)
                    .HasColumnName("TASK_NO")
                    .HasColumnType("decimal(30, 0)");
            });

            modelBuilder.Entity<HrEmpLog>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_LOG");

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.AppNo)
                    .HasColumnName("APP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ApplicationUserActiveStatus)
                    .HasColumnName("APPLICATION_USER_ACTIVE_STATUS")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.AttachFileName)
                    .HasColumnName("ATTACH_FILE_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.AttachFileNo)
                    .HasColumnName("ATTACH_FILE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AttnSchPolDatetime)
                    .HasColumnName("ATTN_SCH_POL_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.AttnSchedulePol)
                    .HasColumnName("ATTN_SCHEDULE_POL")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.AutoRefreshPendingJob)
                    .HasColumnName("AUTO_REFRESH_PENDING_JOB")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.BloodGrp)
                    .HasColumnName("BLOOD_GRP")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CardNo)
                    .HasColumnName("CARD_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.CatNo)
                    .HasColumnName("CAT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ChildrenDecimal)
                    .HasColumnName("CHILDREN_decimal")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ContractDatetime)
                    .HasColumnName("CONTRACT_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ContractEmail)
                    .HasColumnName("CONTRACT_EMAIL")
                    .HasMaxLength(150);

                entity.Property(e => e.ContractExpireDatetime)
                    .HasColumnName("CONTRACT_EXPIRE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ContractMobile)
                    .HasColumnName("CONTRACT_MOBILE")
                    .HasMaxLength(150);

                entity.Property(e => e.ContractPersonName)
                    .HasColumnName("CONTRACT_PERSON_NAME")
                    .HasMaxLength(350);

                entity.Property(e => e.ContractPhone)
                    .HasColumnName("CONTRACT_PHONE")
                    .HasMaxLength(150);

                entity.Property(e => e.ContractRelation)
                    .HasColumnName("CONTRACT_RELATION")
                    .HasMaxLength(150);

                entity.Property(e => e.CurNo)
                    .HasColumnName("CUR_NO")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.CurrentBasic)
                    .HasColumnName("CURRENT_BASIC")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.DefaultJobgroupNo)
                    .HasColumnName("DEFAULT_JOBGROUP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DefaultJoblocNo)
                    .HasColumnName("DEFAULT_JOBLOC_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DefaultShiftNo)
                    .HasColumnName("DEFAULT_SHIFT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.DrivinglcNo)
                    .HasColumnName("DRIVINGLC_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.EmailOfficial)
                    .HasColumnName("EMAIL_OFFICIAL")
                    .HasMaxLength(100);

                entity.Property(e => e.EmailPersonal)
                    .HasColumnName("EMAIL_PERSONAL")
                    .HasMaxLength(100);

                entity.Property(e => e.EmpId)
                    .HasColumnName("EMP_ID")
                    .HasMaxLength(50);

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpType)
                    .HasColumnName("EMP_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmployeeGrade)
                    .HasColumnName("EMPLOYEE_GRADE")
                    .HasMaxLength(200);

                entity.Property(e => e.ExtraCurAct)
                    .HasColumnName("EXTRA_CUR_ACT")
                    .HasMaxLength(200);

                entity.Property(e => e.FatherName)
                    .HasColumnName("FATHER_NAME")
                    .HasMaxLength(90);

                entity.Property(e => e.FaxNo)
                    .HasColumnName("FAX_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.FileExt)
                    .HasColumnName("FILE_EXT")
                    .HasMaxLength(10);

                entity.Property(e => e.FileNo)
                    .HasColumnName("FILE_NO")
                    .HasMaxLength(30);

                entity.Property(e => e.FileSize)
                    .HasColumnName("FILE_SIZE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Fname)
                    .HasColumnName("FNAME")
                    .HasMaxLength(50);

                entity.Property(e => e.Gender)
                    .HasColumnName("GENDER")
                    .HasMaxLength(1);

                entity.Property(e => e.GrpInsNom)
                    .HasColumnName("GRP_INS_NOM")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.HoldSalary)
                    .HasColumnName("HOLD_SALARY")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.HrType)
                    .HasColumnName("HR_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.HusbandName)
                    .HasColumnName("HUSBAND_NAME")
                    .HasMaxLength(90);

                entity.Property(e => e.InsecAftergrace)
                    .HasColumnName("INSEC_AFTERGRACE")
                    .HasColumnType("decimal(4, 0)");

                entity.Property(e => e.InsecBeforegrace)
                    .HasColumnName("INSEC_BEFOREGRACE")
                    .HasColumnType("decimal(4, 0)");

                entity.Property(e => e.Interest)
                    .HasColumnName("INTEREST")
                    .HasMaxLength(200);

                entity.Property(e => e.JobConfirmDatetime)
                    .HasColumnName("JOB_CONFIRM_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.JobType)
                    .HasColumnName("JOB_TYPE")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JobtitleDatetime)
                    .HasColumnName("JOBTITLE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JoinDatetime)
                    .HasColumnName("JOIN_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastIncrementDatetime)
                    .HasColumnName("LAST_INCREMENT_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastPromotionDatetime)
                    .HasColumnName("LAST_PROMOTION_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastTransferDatetime)
                    .HasColumnName("LAST_TRANSFER_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastUpdatetimeD)
                    .HasColumnName("LAST_UPdatetimeD")
                    .HasColumnType("datetime");

                entity.Property(e => e.LineNo)
                    .HasColumnName("LINE_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Lname)
                    .HasColumnName("LNAME")
                    .HasMaxLength(20);

                entity.Property(e => e.MStatus)
                    .HasColumnName("M_STATUS")
                    .HasMaxLength(1);

                entity.Property(e => e.MeritPosition)
                    .HasColumnName("MERIT_POSITION")
                    .HasColumnType("decimal(30, 2)");

                entity.Property(e => e.ModifiedBySession)
                    .HasColumnName("MODIFIED_BY_SESSION")
                    .HasMaxLength(100);

                entity.Property(e => e.MotherName)
                    .HasColumnName("MOTHER_NAME")
                    .HasMaxLength(90);

                entity.Property(e => e.Nationality)
                    .HasColumnName("NATIONALITY")
                    .HasMaxLength(50);

                entity.Property(e => e.NewLastName)
                    .HasColumnName("NEW_LAST_NAME")
                    .HasMaxLength(100);

                entity.Property(e => e.NextIncrDatetime)
                    .HasColumnName("NEXT_INCR_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.NextTransferDatetime)
                    .HasColumnName("NEXT_TRANSFER_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.NlsAddress1)
                    .HasColumnName("NLS_ADDRESS1")
                    .HasMaxLength(250);

                entity.Property(e => e.NlsAddress2)
                    .HasColumnName("NLS_ADDRESS2")
                    .HasMaxLength(250);

                entity.Property(e => e.NlsFname)
                    .HasColumnName("NLS_FNAME")
                    .HasMaxLength(200);

                entity.Property(e => e.NlsLname)
                    .HasColumnName("NLS_LNAME")
                    .HasMaxLength(250);

                entity.Property(e => e.OldEmpNo)
                    .HasColumnName("OLD_EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PabxExt)
                    .HasColumnName("PABX_EXT")
                    .HasColumnType("decimal(5, 0)");

                entity.Property(e => e.PassportNo)
                    .HasColumnName("PASSPORT_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.PeAddr1)
                    .HasColumnName("PE_ADDR1")
                    .HasMaxLength(100);

                entity.Property(e => e.PeAddr2)
                    .HasColumnName("PE_ADDR2")
                    .HasMaxLength(100);

                entity.Property(e => e.PeAddr3)
                    .HasColumnName("PE_ADDR3")
                    .HasMaxLength(500);

                entity.Property(e => e.PeAddrCountry)
                    .HasColumnName("PE_ADDR_COUNTRY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PeAddrDist)
                    .HasColumnName("PE_ADDR_DIST")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PeAddrPost)
                    .HasColumnName("PE_ADDR_POST")
                    .HasMaxLength(20);

                entity.Property(e => e.PeCareOfAddr)
                    .HasColumnName("PE_CARE_OF_ADDR")
                    .HasMaxLength(500);

                entity.Property(e => e.PfApprove)
                    .HasColumnName("PF_APPROVE")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PfConfirmDatetime)
                    .HasColumnName("PF_CONFIRM_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PfMemberFlg)
                    .HasColumnName("PF_MEMBER_FLG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PfNo)
                    .HasColumnName("PF_NO")
                    .HasMaxLength(100);

                entity.Property(e => e.PhoneHome)
                    .HasColumnName("PHONE_HOME")
                    .HasMaxLength(30);

                entity.Property(e => e.PhoneMobile)
                    .HasColumnName("PHONE_MOBILE")
                    .HasMaxLength(30);

                entity.Property(e => e.PhoneOffice)
                    .HasColumnName("PHONE_OFFICE")
                    .HasMaxLength(30);

                entity.Property(e => e.PlaceOfBirth)
                    .HasColumnName("PLACE_OF_BIRTH")
                    .HasMaxLength(100);

                entity.Property(e => e.PrAddr1)
                    .HasColumnName("PR_ADDR1")
                    .HasMaxLength(100);

                entity.Property(e => e.PrAddr2)
                    .HasColumnName("PR_ADDR2")
                    .HasMaxLength(100);

                entity.Property(e => e.PrAddr3)
                    .HasColumnName("PR_ADDR3")
                    .HasMaxLength(500);

                entity.Property(e => e.PrAddrCountry)
                    .HasColumnName("PR_ADDR_COUNTRY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PrAddrDist)
                    .HasColumnName("PR_ADDR_DIST")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PrAddrPost)
                    .HasColumnName("PR_ADDR_POST")
                    .HasMaxLength(20);

                entity.Property(e => e.PrCareOfAddr)
                    .HasColumnName("PR_CARE_OF_ADDR")
                    .HasMaxLength(500);

                entity.Property(e => e.PreferedAddr)
                    .HasColumnName("PREFERED_ADDR")
                    .HasMaxLength(2);

                entity.Property(e => e.PreferredPhone)
                    .HasColumnName("PREFERRED_PHONE")
                    .HasMaxLength(1);

                entity.Property(e => e.ProMembership)
                    .HasColumnName("PRO_MEMBERSHIP")
                    .HasMaxLength(200);

                entity.Property(e => e.ProbableTransferDuration)
                    .HasColumnName("PROBABLE_TRANSFER_DURATION")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.ProvMonth)
                    .HasColumnName("PROV_MONTH")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.Pwd)
                    .HasColumnName("PWD")
                    .HasMaxLength(40);

                entity.Property(e => e.Religion)
                    .HasColumnName("RELIGION")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ReportingTo)
                    .HasColumnName("REPORTING_TO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SaLastLogin)
                    .HasColumnName("SA_LAST_LOGIN")
                    .HasColumnType("datetime");

                entity.Property(e => e.SaPrivType)
                    .HasColumnName("SA_PRIV_TYPE")
                    .HasMaxLength(1);

                entity.Property(e => e.SaPwdModifiedon)
                    .HasColumnName("SA_PWD_MODIFIEDON")
                    .HasColumnType("datetime");

                entity.Property(e => e.SaRoleNo)
                    .HasColumnName("SA_ROLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalaryBankAccNo)
                    .HasColumnName("SALARY_BANK_ACC_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.SalaryBankNo)
                    .HasColumnName("SALARY_BANK_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SalaryReportSlNo)
                    .HasColumnName("SALARY_REPORT_SL_NO")
                    .HasColumnType("decimal(5, 0)");

                entity.Property(e => e.SalaryType)
                    .HasColumnName("SALARY_TYPE")
                    .HasMaxLength(1);

                entity.Property(e => e.Salutation)
                    .HasColumnName("SALUTATION")
                    .HasMaxLength(20);

                entity.Property(e => e.ScaleNo)
                    .HasColumnName("SCALE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ServiceDuration)
                    .HasColumnName("SERVICE_DURATION")
                    .HasColumnType("decimal(5, 0)");

                entity.Property(e => e.ServiceDurationType)
                    .HasColumnName("SERVICE_DURATION_TYPE")
                    .HasMaxLength(1);

                entity.Property(e => e.ServiceEndDatetime)
                    .HasColumnName("SERVICE_END_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ServiceExtentionMonth)
                    .HasColumnName("SERVICE_EXTENTION_MONTH")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsNo)
                    .HasColumnName("SS_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsOsIp)
                    .HasColumnName("SS_OS_IP")
                    .HasMaxLength(20);

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.StepNo)
                    .HasColumnName("STEP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.TinNo)
                    .HasColumnName("TIN_NO")
                    .HasMaxLength(25);

                entity.Property(e => e.TypeOfAgeProve)
                    .HasColumnName("TYPE_OF_AGE_PROVE")
                    .HasMaxLength(500);

                entity.Property(e => e.UserName)
                    .HasColumnName("USER_NAME")
                    .HasMaxLength(30);

                entity.Property(e => e.WfApprove)
                    .HasColumnName("WF_APPROVE")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.WfConfirmDatetime)
                    .HasColumnName("WF_CONFIRM_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.WfMemberFlg)
                    .HasColumnName("WF_MEMBER_FLG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.WfNo)
                    .HasColumnName("WF_NO")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmpSalArear>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_SAL_AREAR");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ArearAmt)
                    .HasColumnName("AREAR_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ArearDatetime)
                    .HasColumnName("AREAR_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ArearElement)
                    .HasColumnName("AREAR_ELEMENT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ArearEndDatetime)
                    .HasColumnName("AREAR_END_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ArearId)
                    .HasColumnName("AREAR_ID")
                    .HasMaxLength(50);

                entity.Property(e => e.ArearNo)
                    .HasColumnName("AREAR_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ArearStartDatetime)
                    .HasColumnName("AREAR_START_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(300);

                entity.Property(e => e.EffectWithsal)
                    .HasColumnName("EFFECT_WITHSAL")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmppayrollNo)
                    .HasColumnName("EMPPAYROLL_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.IsAutoCalculate)
                    .HasColumnName("IS_AUTO_CALCULATE")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.IsInstallment)
                    .HasColumnName("IS_INSTALLMENT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.NoOfInstallment)
                    .HasColumnName("NO_OF_INSTALLMENT")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.PaidFlag)
                    .HasColumnName("PAID_FLAG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PaidInSeperateElement)
                    .HasColumnName("PAID_IN_SEPERATE_ELEMENT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.PaymentInterval)
                    .HasColumnName("PAYMENT_INTERVAL")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.PerInstallmentAmt)
                    .HasColumnName("PER_INSTALLMENT_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmpSalAreardtl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_SAL_AREARDTL");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("APPROVED_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ArearNo)
                    .HasColumnName("AREAR_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.AreardtlNo)
                    .HasColumnName("AREARDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Descr)
                    .HasColumnName("DESCR")
                    .HasMaxLength(300);

                entity.Property(e => e.InstallmentAmt)
                    .HasColumnName("INSTALLMENT_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.InstallmentElement)
                    .HasColumnName("INSTALLMENT_ELEMENT")
                    .HasColumnType("decimal(3, 0)");

                entity.Property(e => e.PaidAmt)
                    .HasColumnName("PAID_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.PaidFlag)
                    .HasColumnName("PAID_FLAG")
                    .HasMaxLength(1);

                entity.Property(e => e.PaymentDatetime)
                    .HasColumnName("PAYMENT_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmpSearchEdumajor>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_SEARCH_EDUMAJOR");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EdumajorNo)
                    .HasColumnName("EDUMAJOR_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SessionEmpNo)
                    .HasColumnName("SESSION_EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmpSearchTemp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMP_SEARCH_TEMP");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SessionEmpNo)
                    .HasColumnName("SESSION_EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SummaryDetails)
                    .HasColumnName("SUMMARY_DETAILS")
                    .HasMaxLength(4000);
            });

            modelBuilder.Entity<HrEmpcardreg>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPCARDREG");

                entity.Property(e => e.AccesslevelNo)
                    .HasColumnName("ACCESSLEVEL_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ActivationDatetime)
                    .HasColumnName("ACTIVATION_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ActiveStat)
                    .HasColumnName("ACTIVE_STAT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.Activeuser)
                    .HasColumnName("ACTIVEUSER")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Alarmuser)
                    .HasColumnName("ALARMUSER")
                    .HasMaxLength(10);

                entity.Property(e => e.Antipassbackind)
                    .HasColumnName("ANTIPASSBACKIND")
                    .HasMaxLength(10);

                entity.Property(e => e.CardNo)
                    .HasColumnName("CARD_NO")
                    .HasMaxLength(100);

                entity.Property(e => e.CardRegBy)
                    .HasColumnName("CARD_REG_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CardRegDatetime)
                    .HasColumnName("CARD_REG_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.CardRegNo)
                    .HasColumnName("CARD_REG_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Cardtypeid)
                    .HasColumnName("CARDTYPEID")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Expirydatetime)
                    .HasColumnName("EXPIRYdatetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RegCompleteBy)
                    .HasColumnName("REG_COMPLETE_BY")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.RegCompleteDatetime)
                    .HasColumnName("REG_COMPLETE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RegCompleteFlag)
                    .HasColumnName("REG_COMPLETE_FLAG")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.ServerSubmitDatetime)
                    .HasColumnName("SERVER_SUBMIT_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ServerSubmitFlag)
                    .HasColumnName("SERVER_SUBMIT_FLAG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmpevakra>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPEVAKRA");

                entity.Property(e => e.AssignforNo)
                    .HasColumnName("ASSIGNFOR_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpevakraNo)
                    .HasColumnName("EMPEVAKRA_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EvaFor)
                    .HasColumnName("EVA_FOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SlNo)
                    .HasColumnName("SL_NO")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.TaskNo)
                    .HasColumnName("TASK_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Weightage)
                    .HasColumnName("WEIGHTAGE")
                    .HasColumnType("decimal(5, 0)");
            });

            modelBuilder.Entity<HrEmpfamaly>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPFAMALY");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CurrentStatus)
                    .HasColumnName("CURRENT_STATUS")
                    .HasMaxLength(400);

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(500);

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Fname)
                    .HasColumnName("FNAME")
                    .HasMaxLength(350);

                entity.Property(e => e.Relation)
                    .HasColumnName("RELATION")
                    .HasMaxLength(150);

                entity.Property(e => e.SlNo)
                    .HasColumnName("SL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmpfixedelement>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPFIXEDELEMENT");

                entity.Property(e => e.Active)
                    .HasColumnName("ACTIVE")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.Amt)
                    .HasColumnName("AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpfixedelementNo)
                    .HasColumnName("EMPFIXEDELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.MaxAmt)
                    .HasColumnName("MAX_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Per)
                    .HasColumnName("PER")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.PerOn)
                    .HasColumnName("PER_ON")
                    .HasMaxLength(2);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmpinfo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPINFO");

                entity.Property(e => e.ActiveStatus)
                    .HasColumnName("ACTIVE_STATUS")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ConditionFlag)
                    .HasColumnName("CONDITION_FLAG")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(500);

                entity.Property(e => e.InfoName)
                    .HasColumnName("INFO_NAME")
                    .HasMaxLength(500);

                entity.Property(e => e.InfoNo)
                    .HasColumnName("INFO_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.NextDays)
                    .HasColumnName("NEXT_DAYS")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Priority)
                    .HasColumnName("PRIORITY")
                    .HasMaxLength(100);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmpjdreports>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPJDREPORTS");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpjdNo)
                    .HasColumnName("EMPJD_NO")
                    .HasMaxLength(50);

                entity.Property(e => e.EmpjdreportsNo)
                    .HasColumnName("EMPJDREPORTS_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.FieldVisit)
                    .HasColumnName("FIELD_VISIT")
                    .HasMaxLength(100);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.Type)
                    .HasColumnName("TYPE")
                    .HasMaxLength(5);
            });

            modelBuilder.Entity<HrEmpjdtaskreports>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPJDTASKREPORTS");

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpJdDtlNo)
                    .HasColumnName("EMP_JD_DTL_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpjdtaskreportsNo)
                    .HasColumnName("EMPJDTASKREPORTS_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.Type)
                    .HasColumnName("TYPE")
                    .HasMaxLength(5);
            });

            modelBuilder.Entity<HrEmplastschedule>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPLASTSCHEDULE");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.DurationPolNo)
                    .HasColumnName("DURATION_POL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.LastRuleDuration)
                    .HasColumnName("LAST_RULE_DURATION")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.LastScheduleDuration)
                    .HasColumnName("LAST_SCHEDULE_DURATION")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.LastScheduleNo)
                    .HasColumnName("LAST_SCHEDULE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmpp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPP");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.McardNo)
                    .HasColumnName("MCARD_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmppayroll>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPPAYROLL");

                entity.Property(e => e.BuNo)
                    .HasColumnName("BU_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CalBasicFromGrossFormula)
                    .HasColumnName("CAL_BASIC_FROM_GROSS_FORMULA")
                    .HasMaxLength(50);

                entity.Property(e => e.CashAmt)
                    .HasColumnName("CASH_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CashPayment)
                    .HasColumnName("CASH_PAYMENT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeAmt)
                    .HasColumnName("CHEQUE_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequePayment)
                    .HasColumnName("CHEQUE_PAYMENT")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.CurrNo)
                    .HasColumnName("CURR_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EffectiveDatetime)
                    .HasColumnName("EFFECTIVE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmppayrollNo)
                    .HasColumnName("EMPPAYROLL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ExchangeRate)
                    .HasColumnName("EXCHANGE_RATE")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.GradeNo)
                    .HasColumnName("GRADE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Gross)
                    .HasColumnName("GROSS")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.IncrementNo)
                    .HasColumnName("INCREMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.JobtitleNo)
                    .HasColumnName("JOBTITLE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.PolNo)
                    .HasColumnName("POL_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.StepNo)
                    .HasColumnName("STEP_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TransferNo)
                    .HasColumnName("TRANSFER_NO")
                    .HasColumnType("decimal(30, 0)");
            });

            modelBuilder.Entity<HrEmppayrolldtl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPPAYROLLDTL");

                entity.Property(e => e.Active)
                    .HasColumnName("ACTIVE")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.Amt)
                    .HasColumnName("AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CashAmt)
                    .HasColumnName("CASH_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeAmt)
                    .HasColumnName("CHEQUE_AMT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EffectiveDatetime)
                    .HasColumnName("EFFECTIVE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmppayrollNo)
                    .HasColumnName("EMPPAYROLL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmppayrolldtlNo)
                    .HasColumnName("EMPPAYROLLDTL_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.FlagForArear)
                    .HasColumnName("FLAG_FOR_AREAR")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.IsGross)
                    .HasColumnName("IS_GROSS")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.JtpayrollNo)
                    .HasColumnName("JTPAYROLL_NO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MaxAmt)
                    .HasColumnName("MAX_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Per)
                    .HasColumnName("PER")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Percents)
                    .HasColumnName("PERCENTS")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Peron)
                    .HasColumnName("PERON")
                    .HasMaxLength(1);

                entity.Property(e => e.ProvisionMonth)
                    .HasColumnName("PROVISION_MONTH")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmppfrule>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPPFRULE");

                entity.Property(e => e.Amt)
                    .HasColumnName("AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CashRatio)
                    .HasColumnName("CASH_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ChequeRatio)
                    .HasColumnName("CHEQUE_RATIO")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ElementNo)
                    .HasColumnName("ELEMENT_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.MaxAmt)
                    .HasColumnName("MAX_AMT")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.OwnPer)
                    .HasColumnName("OWN_PER")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Peron)
                    .HasColumnName("PERON")
                    .HasMaxLength(2);

                entity.Property(e => e.PfType)
                    .HasColumnName("PF_TYPE")
                    .HasMaxLength(1);

                entity.Property(e => e.PfruleNo)
                    .HasColumnName("PFRULE_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.ProbationMonth)
                    .HasColumnName("PROBATION_MONTH")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.RuleDatetime)
                    .HasColumnName("RULE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmprosterhistory>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPROSTERHISTORY");

                entity.Property(e => e.AppliedDatetime)
                    .HasColumnName("APPLIED_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.AttnSchedulePol)
                    .HasColumnName("ATTN_SCHEDULE_POL")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.EmpNo)
                    .HasColumnName("EMP_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.HistoryNo)
                    .HasColumnName("HISTORY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.InactiveDatetime)
                    .HasColumnName("INACTIVE_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<HrEmptraining>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("HR_EMPTRAINING");

                entity.Property(e => e.ActiveStatus)
                    .HasColumnName("ACTIVE_STATUS")
                    .HasColumnType("decimal(1, 0)");

                entity.Property(e => e.CompanyNo)
                    .HasColumnName("COMPANY_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(1000);

                entity.Property(e => e.DurationCount)
                    .HasColumnName("DURATION_COUNT")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.DurationType)
                    .HasColumnName("DURATION_TYPE")
                    .HasMaxLength(10);

                entity.Property(e => e.PreRequisite)
                    .HasColumnName("PRE_REQUISITE")
                    .HasMaxLength(1000);

                entity.Property(e => e.Specialty)
                    .HasColumnName("SPECIALTY")
                    .HasMaxLength(500);

                entity.Property(e => e.SsCreatedOn)
                    .HasColumnName("SS_CREATED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsCreator)
                    .HasColumnName("SS_CREATOR")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsIsDeleted)
                    .HasColumnName("SS_IS_DELETED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsIsUploaded)
                    .HasColumnName("SS_IS_UPLOADED")
                    .HasColumnType("decimal(1, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SsModifiedOn)
                    .HasColumnName("SS_MODIFIED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.SsModifier)
                    .HasColumnName("SS_MODIFIER")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsOgNo)
                    .HasColumnName("SS_OG_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.SsUploadedOn)
                    .HasColumnName("SS_UPLOADED_ON")
                    .HasMaxLength(50);

                entity.Property(e => e.Step)
                    .HasColumnName("STEP")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.Title)
                    .HasColumnName("TITLE")
                    .HasMaxLength(100);

                entity.Property(e => e.TrainingId)
                    .HasColumnName("TRAINING_ID")
                    .HasMaxLength(50);

                entity.Property(e => e.TrainingName)
                    .HasColumnName("TRAINING_NAME")
                    .HasMaxLength(500);

                entity.Property(e => e.TrainingNo)
                    .HasColumnName("TRAINING_NO")
                    .HasColumnType("decimal(30, 0)");

                entity.Property(e => e.TrainingObjective)
                    .HasColumnName("TRAINING_OBJECTIVE")
                    .HasMaxLength(500);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
