﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrEmptraining
    {
        public decimal? TrainingNo { get; set; }
        public string TrainingId { get; set; }
        public string Title { get; set; }
        public decimal? Step { get; set; }
        public string TrainingName { get; set; }
        public string Description { get; set; }
        public decimal? DurationCount { get; set; }
        public string DurationType { get; set; }
        public string Specialty { get; set; }
        public string PreRequisite { get; set; }
        public string TrainingObjective { get; set; }
        public decimal? ActiveStatus { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
