﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrDependBill
    {
        public decimal? DependBillNo { get; set; }
        public decimal? BillId { get; set; }
        public DateTime? ProcessMonth { get; set; }
        public decimal? ProcessBy { get; set; }
        public DateTime? ProcessDatetime { get; set; }
        public decimal? CompanyNo { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
