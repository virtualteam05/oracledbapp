﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrDependBilldtl
    {
        public decimal? DependBilldtlNo { get; set; }
        public decimal? DependBillNo { get; set; }
        public decimal? BillId { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? PayElementNo { get; set; }
        public decimal? DependElementNo { get; set; }
        public decimal? ProcessDurationMonth { get; set; }
        public decimal? ProvisionMonth { get; set; }
        public decimal? NoOfOccurance { get; set; }
        public decimal? Consucative { get; set; }
        public decimal? NoOfSalOccur { get; set; }
        public decimal? PayableAmt { get; set; }
        public decimal? DeductAmt { get; set; }
        public decimal? NetPayAmt { get; set; }
        public decimal? PaidFlag { get; set; }
        public decimal? PayBy { get; set; }
        public DateTime? PayDatetime { get; set; }
        public decimal? CompanyNo { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public DateTime? ProcessEndDatetime { get; set; }
    }
}
