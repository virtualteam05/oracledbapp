﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrEmpAttndtlTmpRep
    {
        public decimal? SlNo { get; set; }
        public DateTime? CalDt { get; set; }
        public string DdStatus { get; set; }
        public decimal? EmpNo { get; set; }
        public string Shift { get; set; }
        public string EmpName { get; set; }
        public string EmpId { get; set; }
        public DateTime? SchDatetime { get; set; }
        public decimal? SchShift { get; set; }
        public DateTime? SchInTime { get; set; }
        public DateTime? SchOutTime { get; set; }
        public decimal? AtndNo { get; set; }
        public decimal? AtnShift { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public decimal? LateStat { get; set; }
        public decimal? LateentryApproved { get; set; }
        public decimal? EarlyExitStat { get; set; }
        public decimal? EarlyexitApproved { get; set; }
        public string Status { get; set; }
        public string Prefix { get; set; }
        public decimal? BuNo { get; set; }
        public string BuName { get; set; }
        public string EmpNameId { get; set; }
        public decimal? JobtitleNo { get; set; }
        public string Jobtitle { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? SalReportSlNo { get; set; }
        public string HrType { get; set; }
        public string JobType { get; set; }
        public decimal? DepartmentNo { get; set; }
        public string Department { get; set; }
        public decimal? SectionNo { get; set; }
        public string Section { get; set; }
        public string Ot { get; set; }
        public string Wo { get; set; }
        public string Late { get; set; }
        public decimal? ContFlg { get; set; }
        public string Early { get; set; }
        public string Remarks { get; set; }
        public DateTime? GenInTime { get; set; }
        public string InTimeTfmt { get; set; }
        public string GenOt { get; set; }
        public string OutTimeTfmt { get; set; }
        public DateTime? GenOutTime { get; set; }
        public string GenOutTimeTfmt { get; set; }
        public string OtTfmt { get; set; }
        public string OthOt { get; set; }
        public string OthOtTfmt { get; set; }
        public decimal? WorkHour { get; set; }
        public decimal? MovementHour { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public decimal? ScaleNo { get; set; }
        public string TotOt { get; set; }
        public string TotOtTfmt { get; set; }
        public DateTime? GenOutTimeC { get; set; }
        public string GenOtC { get; set; }
        public string LineName { get; set; }
    }
}
