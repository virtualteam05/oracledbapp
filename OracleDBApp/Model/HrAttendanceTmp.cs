﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrAttendanceTmp
    {
        public decimal? AtndNo { get; set; }
        public decimal? EmpNo { get; set; }
        public DateTime? AttndDatetime { get; set; }
        public DateTime? ShiftOutTime { get; set; }
        public DateTime? OutTime { get; set; }
        public DateTime? GenOutTime { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
        public string SsCreatedOn { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsCreator { get; set; }
        public decimal? SsModifier { get; set; }
        public decimal? SsOgNo { get; set; }
    }
}
