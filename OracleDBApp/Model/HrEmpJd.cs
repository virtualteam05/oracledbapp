﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrEmpJd
    {
        public decimal? EmpJdNo { get; set; }
        public string EmpJdId { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? BuNo { get; set; }
        public decimal? TaskNo { get; set; }
        public decimal? SlNo { get; set; }
        public string Descr { get; set; }
        public DateTime? EffectiveFromDatetime { get; set; }
        public decimal? ActiveStat { get; set; }
        public decimal? CompanyNo { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
