﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrEmpfixedelement
    {
        public decimal? EmpfixedelementNo { get; set; }
        public decimal? EmpNo { get; set; }
        public decimal? ElementNo { get; set; }
        public decimal? Amt { get; set; }
        public decimal? Per { get; set; }
        public string PerOn { get; set; }
        public decimal? MaxAmt { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? Active { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
