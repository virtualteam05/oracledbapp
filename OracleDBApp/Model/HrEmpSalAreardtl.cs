﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrEmpSalAreardtl
    {
        public decimal? AreardtlNo { get; set; }
        public decimal? ArearNo { get; set; }
        public decimal? InstallmentElement { get; set; }
        public decimal? InstallmentAmt { get; set; }
        public DateTime? PaymentDatetime { get; set; }
        public decimal? PaidAmt { get; set; }
        public string Descr { get; set; }
        public string PaidFlag { get; set; }
        public decimal? SsCreator { get; set; }
        public string SsCreatedOn { get; set; }
        public decimal? SsModifier { get; set; }
        public string SsModifiedOn { get; set; }
        public decimal? ApprovedBy { get; set; }
        public decimal? SsOgNo { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
        public decimal? SsIsDeleted { get; set; }
        public decimal? SsIsUploaded { get; set; }
    }
}
