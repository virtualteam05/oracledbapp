﻿using System;
using System.Collections.Generic;

namespace OracleDBApp.Model
{
    public partial class HrAppinterviewExt
    {
        public string RollNo { get; set; }
        public decimal? GetMark { get; set; }
        public decimal? CompanyNo { get; set; }
        public string SsUploadedOn { get; set; }
    }
}
