﻿using Dapper;
using Microsoft.Extensions.Configuration;

using Oracle.ManagedDataAccess.Client;
using OracleDBApp.Common.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace OracleDBApp.DAL
{
    public class DataAccessManager : IDataAccessManager
    {
        private IConfiguration configuration;
        private string conString;
        private OracleConnection conn;
        public DataAccessManager(IConfiguration _configuration)
        {
            configuration = _configuration;
            conString = configuration["ConnectionString"].ToString();
            conn = new OracleConnection(conString);
        }
        public IDbConnection GetConnection()
        {
            //var connectionString = configuration.GetSection("OracleConfiguration").Value;
            var conn = new OracleConnection(conString);
            return conn;
        }
        //private OracleConnection GetOracleDBConnection()
        //{
        //    string conString = configuration["ConnectionString"].ToString();
        //}

        public Task<int> Delete<T>(T obj)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<T>> ExecuteFunction<T>(string functionName,object parameters)
        {
            IEnumerable<T> result = null;

            //object result = null;
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
         
            if (conn.State == ConnectionState.Open)
            {
                result= await SqlMapper.QueryAsync<T>(conn, functionName, param: parameters, commandType: CommandType.StoredProcedure);
            }
            return result;
        }

        public async Task<IEnumerable<T>> ExecuteProcedure<T>(string procedureName, object parameters)
        {
            IEnumerable<T> result = null;

            //object result = null;
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            if (conn.State == ConnectionState.Open)
            {
                result = await SqlMapper.QueryAsync<T>(conn, procedureName, param: parameters, commandType: CommandType.StoredProcedure);
            }
            return result;
        }

        public async Task<object> GetAll<T>()
        
        
        {
            object result = null;
            string sql = "Select *from " + typeof(T).Name;
            //object result = null;
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            if (conn.State == ConnectionState.Open)
            {
                result = await SqlMapper.QueryAsync(conn,sql, commandType: CommandType.Text);
            }
            return result;
        }

        public async Task<T> GetByID<T>(object id)
        {
            await SqlMapper.QueryAsync(conn, string.Empty, CommandType.Text);
            return default;
        }

        public async Task Insert<T>(DataTable dt)
        {
            //using (var connection = new OracleConnection(conString))
            //{
            //    using (var bulkCopy = new OracleBulkCopy(conString))
            //    {
            //        bulkCopy.DestinationTableName = typeof(T).Name;
            //        await bulkCopy.WriteToServerAsync(dt);
            //    }
            //}
            await SqlMapper.QueryAsync(conn, string.Empty, CommandType.Text);
            
        }


        public async Task<int> Update<T>(T obj)
        {
            await SqlMapper.QueryAsync(conn, string.Empty, CommandType.Text);
            return 0;
        }


    }
}
