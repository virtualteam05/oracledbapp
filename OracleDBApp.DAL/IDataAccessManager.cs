﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace OracleDBApp.DAL
{
   public interface IDataAccessManager
    {
        Task Insert<T>(DataTable obj);
        Task<int> Update<T>(T obj);
        Task<int> Delete<T>(T obj);
        Task<T> GetByID<T>(object id);
        Task<object> GetAll<T>();
        Task<IEnumerable<T>> ExecuteProcedure<T>(string procedureName,object inputParams);
        Task<IEnumerable<T>> ExecuteFunction<T>( string functionName,object parameters);
    }
}
